//
//  LoginInvolvePresenter.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 19/04/21.
//

import UIKit

protocol LoginInvolveDelegate : NSObjectProtocol {
    func onSuccessLogin(mResponse : LoginInvolveResponse)
    func onValidateEmail(mResponse : LoginInvolveResponse)
}

class LoginInvolvePresenter: BaseInvolvePresenter{
    var mViewController : UIViewController!
    var mDelegate : LoginInvolveDelegate!
    var mSuccessEmail : Bool = false
    
    init(mViewController : UIViewController, mDelegate : LoginInvolveDelegate) {
        self.mViewController = mViewController
        self.mDelegate = mDelegate
    }
    
    func validateEmail(mEmail: String){
        AlertDialog.showOverlay()
        let mLoginRequest = LoginInvolveRequest(mEmail: mEmail, mEmailSuccesss: false)
        BaseRetrofitManager<LoginInvolveResponse>.init(requestUrl: ApiDefinitions.WS_LOGIN, delegate: self).requestPost(requestModel: mLoginRequest)
    }
    
    func login(mEmail: String, mPassword : String, mEmailSuccess : Bool){
        mSuccessEmail = mEmailSuccess
        AlertDialog.showOverlay()
        let mLoginRequest = LoginInvolveRequest(mEmail: mEmail, mPassword: mPassword, mEmailSuccesss: mSuccessEmail)
        BaseRetrofitManager<LoginInvolveResponse>.init(requestUrl: ApiDefinitions.WS_LOGIN, delegate: self).requestLoginGetToken(requestModel: mLoginRequest)
    }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        onSuccessValidateLogin(mResponse: response as! LoginInvolveResponse)
    }
    
    func onSuccessValidateLogin(mResponse : LoginInvolveResponse){
        AlertDialog.hideOverlay()
        print("Response: \(mResponse.toJSONString(prettyPrint: true) ?? "")")
        if(mResponse.mBodyModel != nil){
            mDelegate.onValidateEmail(mResponse: mResponse)
        }else{
            mDelegate.onSuccessLogin(mResponse: mResponse)
        }
    }
}
