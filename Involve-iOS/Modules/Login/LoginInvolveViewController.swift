//
//  LoginInvolveViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

class LoginInvolveViewController: BaseViewController, UITextFieldDelegate, LoginInvolveDelegate {
    @IBOutlet weak var mEmailTextField: TextFieldUtils!
    @IBOutlet weak var mPassWordLoginContainerView: UIView!
    @IBOutlet weak var mPasswordTextField: TextFieldUtils!
    @IBOutlet weak var mBackButton: UIButton!
    @IBOutlet weak var mMailLabel: UILabel!
    
    var mShow : Bool = false
    let validatorManager = ValidatorText()
    var mShowPassword : Bool = false
    var mLoginInvolvePresenter : LoginInvolvePresenter!
    var mSuccessEmail = false
    var mPersistenceData : PersistenceData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mEmailTextField.rightView?.isHidden = false
        addTargets()
        addValidator()
        initializePersistence()
    }
    
    func initializePersistence(){
        mPersistenceData = PersistenceData()
    }
    
    override func getPresenter() -> BasePresenter? {
        mLoginInvolvePresenter = LoginInvolvePresenter(mViewController: self, mDelegate: self)
        return mLoginInvolvePresenter
    }
    
    func addValidator(){
        mEmailTextField.delegate = validatorManager
        
        validatorManager.addFieldValidate(
            mFields: [ValidatorField.textField(
                mEmailTextField,
                .required,
                .email,
                "Al parecer el formato del correo electrónico no es el correcto, verifiquelo por favor.",
                self
            )])
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        mSuccessEmail = false
        mPasswordTextField.text = ""
        showPasswordView(mIsHide: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func createAccountAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "CreateAccount", mViewController: CreateAccountViewController.self, mNameViewController: "CreateAccountViewController", mDelegate: nil)
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "ForgotPassword", mViewController: ForgotPasswordViewController.self, mNameViewController: "ForgotPasswordViewController", mDelegate: nil)
    }
    
    @IBAction func mShowPasswordButton(_ sender: Any) {
        if(!mShowPassword){
            mPasswordTextField.isSecureTextEntry = false
            mShowPassword = true
        }else{
            mPasswordTextField.isSecureTextEntry = true
        }
    }
    
    @IBAction func mNextAction(_ sender: Any) {
        if(!mSuccessEmail){
            if (validatorManager.validate()){
                mLoginInvolvePresenter.validateEmail(mEmail: mEmailTextField.text!.trim())
            }
        }else{
            if(mPasswordTextField.text?.count == 0){
                AlertDialog.show(title: "Aviso", body: "El campo no puede estar vacío", view: self)
            }else{
                mLoginInvolvePresenter.login(mEmail:  mEmailTextField.text!.trim(), mPassword: (mPasswordTextField.text?.trim())!, mEmailSuccess: mSuccessEmail)
            }
        }
    }
    
    func showPasswordView(mIsHide : Bool){
        mPassWordLoginContainerView.isHidden = mIsHide
    }
    
    func launchHome(){
        presentWithNavigationBar(mNameStoryBoard: "Home", mViewController: HomeViewController.self, mNameViewController: "HomeViewController")
    }
    
    func onValidateEmail(mResponse : LoginInvolveResponse) {
        if(mResponse.mBodyModel == nil){
            AlertDialog.show(title: "Involve", body: (mResponse.mBodyModel?.mMessage)!, view: self)
            mSuccessEmail = false
        }else{
            mSuccessEmail = true
            mMailLabel.text = mEmailTextField.text
            showPasswordView(mIsHide: false)
        }
    }
    
    func onSuccessLogin(mResponse: LoginInvolveResponse) {
        if(mResponse.mUser != nil){
            mPersistenceData.saveData(mData: "on", meKey: "loginSuccess")
            selectFlow(mPorcent: mResponse.mPorcentageProfilel!)
        }else{
            if(mResponse.mBodyModel != nil){
                if(mResponse.mStatus == 412){
                    restorePasswordDialog(mMessage: "La cuenta esta bloqueada")
                }else{
                    AlertDialog.show(title: "Involve", body: "mResponse.mMessage!", view: self)
                }
            }
        }
    }
    
    func selectFlow(mPorcent : Int){
        switch mPorcent {
        case 0:
            //launchHome()

            presentWithNavigationBar(mNameStoryBoard: "Register",mViewController: RegisterViewController.self, mNameViewController: "RegisterViewController")
            break
        
        case 20:
            presentWithNavigationBarHideBack(mNameStoryBoard: "RegisterGenre",mViewController: RegisterGenreViewController.self, mNameViewController: "RegisterGenreViewController")
            break
        
        case 60:
            presentWithNavigationBarHideBack(mNameStoryBoard: "RegisterProfile",mViewController: RegisterProfileViewController.self, mNameViewController: "RegisterProfileViewController")
            break
        
        case 80:
            presentWithNavigationBarHideBack(mNameStoryBoard: "RegisterPosition",mViewController: RegisterPositionViewController.self, mNameViewController: "RegisterPositionViewController")
            break
        
        case 100:
            presentWithNavigationBarHideBack(mNameStoryBoard: "RegisterSalary",mViewController: RegisterSalaryViewController.self, mNameViewController: "RegisterSalaryViewController")
            break
        case 16:
            launchHome()
            break
        default:
            break
        }
    }
    
    func restorePasswordDialog(mMessage : String){
        let alert = UIAlertController(title: "Involve", message: mMessage, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Restablecer", style: .default, handler: { action in
                    self.pushViewController(mNameStoryBoard: "RestorePassword", mViewController: RestorePasswordViewController.self, mNameViewController: "RestorePasswordViewController", mDelegate: nil)
                })
        let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: { action in
                })
                alert.addAction(cancel)
                alert.addAction(ok)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
    }
    
}
