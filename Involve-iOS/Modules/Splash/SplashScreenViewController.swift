//
//  SplashScreenViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//
import GradientProgress
import UIKit

class SplashScreenViewController: BaseViewController {
    @IBOutlet weak var mProgressBarView: GradientProgressBar!
    var mCounter = 0
    var mTimer = Timer()
    var mPorcentIncrement = 0.0
    var mPersistenceData : PersistenceData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        initializePersistence()
        delayLaunchIntroductionView()
    }
    
    func initializePersistence(){
        mPersistenceData = PersistenceData()
    }
    
    func setProgressBar(){
        mProgressBarView.gradientColors = [UIColor(netHex: Colors.color_branding_blue).cgColor, UIColor(netHex: Colors.color_branding_blue).cgColor]
        mProgressBarView.cornerRadius = 6
        mProgressBarView.borderWidth = 1
        mProgressBarView.borderColor = UIColor(netHex: Colors.color_branding_blue)
    }
    
    func delayLaunchIntroductionView(){
        mTimer.invalidate()
        mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelay), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelay() {
        mCounter += 1
        mPorcentIncrement += 0.10
        mProgressBarView.setProgress(Float(mPorcentIncrement), animated: true)
        if(mCounter == 20){
            mTimer.invalidate()
            launchOBInvolveScreen()
        }
    }
    
    func launchOBInvolveScreen(){
        if(mPersistenceData.retrieveData(mKey: "Skip") == "on"){
            /*if(mPersistenceData.retrieveData(mKey: "loginSuccess") == "on"){
                presentWithNavigationBar(mNameStoryBoard: "Home", mViewController: HomeViewController.self, mNameViewController: "HomeViewController")
            }else{*/
                presentWithNavigationBar(mNameStoryBoard: "LoginInvolve",mViewController: LoginInvolveViewController.self, mNameViewController: "LoginInvolveViewController")
            //}
        }else{
            presentViewController(mNameStoryBoard: "OBInvolve", mViewController: OBInvolveViewController.self, mNameViewController: "OBInvolveViewController")
        }
       
    }

}
