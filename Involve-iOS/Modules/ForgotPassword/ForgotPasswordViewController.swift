//
//  ForgotPasswordViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {
    @IBOutlet weak var mTitleForgotLabel: UILabel!
    @IBOutlet weak var mDescriptionLabel: UILabel!
    
    @IBOutlet weak var mEmailTextField: TextFieldUtils!
    let validatorManager = ValidatorText()

    var mCounter = 0
    var mTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
        setTextFormat()
        addValidator()
    }

    func addValidator(){
        mEmailTextField.delegate = validatorManager
        
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mEmailTextField, //textfield
                .required, //validacion del texto mientras.
                .all,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func setTextFormat(){
        mTitleForgotLabel.text = "¿Olvidaste tu\ncontraseña?"
    }
    
    @IBAction func mSendAction(_ sender: Any) {
        delayActionValidaEmail()
    }
    
    func dismissController(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func delayActionValidaEmail(){
        AlertDialog.showOverlay()
        mTimer.invalidate()
        mCounter = 0
        mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelayValidEmail), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayValidEmail(){
        mCounter += 1
     
        if(mCounter == 20){
            mTimer.invalidate()
            AlertDialog.hideOverlay()
            sendEmail()
        }
    }
    
    func sendEmail(){
        presentViewController(mNameStoryBoard: "CheckMail", mViewController: CheckMailViewController.self, mNameViewController: "CheckMailViewController")
        dismissController()
    }
    

}

