//
//  VancancyViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 07/05/21.
//

import UIKit
import SlidingContainerViewController

class VancancyViewController: BaseViewController,SlidingContainerViewControllerDelegate {
    @IBOutlet weak var mFavsSelectorView: UIView!
    @IBOutlet weak var mVacancySelectorView: UIView!
    @IBOutlet weak var mFabsLabel: UILabel!
    @IBOutlet weak var mVacancyLabel: UILabel!
    @IBOutlet weak var mMainContainerView: UIView!
    var slidingContainerViewController : SlidingContainerViewController!
    var mArrayControllers : [UIViewController] = []
    var mArrayLabels : [UILabel] = []
    var mArrayTabSelector :  [UIView] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        populateViews()
        populateControllers()
        setDataSourceTabs()
    }
    
    func populateViews(){
        mArrayLabels.append(mVacancyLabel)
        mArrayLabels.append(mFabsLabel)
        
        mArrayTabSelector.append(mVacancySelectorView)
        mArrayTabSelector.append(mFavsSelectorView)
    }
    
    func setDataSourceTabs(){
        slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: [],mHeight: 0)
        slidingContainerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        slidingContainerViewController.delegate = self
        mMainContainerView.addSubview(slidingContainerViewController.view)
        selectTab(mPosition: 0)
    }
    
    @IBAction func mFavsAction(_ sender: Any) {
        selectTab(mPosition: 1)
        slidingContainerViewController.setCurrentViewControllerAtIndex(1)

    }
   
    @IBAction func mVacancyAction(_ sender: Any) {
        selectTab(mPosition: 0)
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
    }
    
    func populateControllers(){
        let storyboardVacancy : UIStoryboard = UIStoryboard(name: "VacancySubTab", bundle: nil)
        let mVacancy = (storyboardVacancy.instantiateViewController(withIdentifier: "VacancySubTabViewController") as! VacancySubTabViewController)
        
        let storyboardFavs : UIStoryboard = UIStoryboard(name: "Favs", bundle: nil)
        let mFavs = (storyboardFavs.instantiateViewController(withIdentifier: "FavsViewController") as! FavsViewController)
        
        mArrayControllers.append(mVacancy)
        mArrayControllers.append(mFavs)
    }
    
    func selectTab(mPosition : Int){
        for mIndex in 0...mArrayLabels.count - 1 {
            if(mIndex == mPosition){
                mArrayLabels[mIndex].textColor = UIColor(netHex: Colors.color_grey_select)
                mArrayTabSelector[mIndex].backgroundColor = UIColor(netHex: Colors.color_final_strong_green)
            }else{
                mArrayLabels[mIndex].textColor = UIColor(netHex: Colors.color_gray_less_profile)
                mArrayTabSelector[mIndex].backgroundColor = UIColor(netHex: Colors.color_white)
            }
        }
        
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        selectTab(mPosition: atIndex)
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }

}
