//
//  FavsViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class FavsViewController: BaseViewController, VacancyDelegate {
   
    @IBOutlet weak var mFavsTableView: UITableView!
    var mVacancyDataSource : VacancyDataSource!
    var mListVacancy : [VacancyModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createTableView()
    }
    
    func createTableView(){
        mVacancyDataSource = VacancyDataSource(tableView: mFavsTableView, mViewController: self, mSettingsDelegate: self)
        mFavsTableView.dataSource = mVacancyDataSource
        mFavsTableView.delegate = mVacancyDataSource
        mFavsTableView.reloadData()
        mVacancyDataSource?.update(items: mListVacancy)
    }

    func selectOption(mOption: String) {
        
    }
    
    
}
