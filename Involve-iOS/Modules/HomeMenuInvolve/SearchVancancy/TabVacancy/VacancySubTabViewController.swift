//
//  VacancySubTabViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/05/21.
//

import UIKit
import FlexibleTable

class VacancySubTabViewController: BaseViewController, VacancyDelegate, SearchCoincidenceDelegate {
   
    @IBOutlet weak var mHeightLabelResultConstraint: NSLayoutConstraint!
    @IBOutlet weak var mListSearchTableView: UITableView!
    @IBOutlet weak var mSearchTextField: UITextField!
    @IBOutlet weak var mListVacancyTableView: UITableView!
    @IBOutlet weak var mResultsLabel: UILabel!
    @IBOutlet weak var mHeaaderView: UIView!
    @IBOutlet weak var mHeightView: NSLayoutConstraint!
    
    var mVacancyDataSource : VacancyDataSource!
    var mListVacancy : [VacancyModel] = []
    var mListSearch : [SearchCoincidenceModel] = []
    var mSearchDataSource : SearchCoincidenceDataSource!
    var mPresenterVacancySubTab : VacancySubTabPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTarget()
        createTableView()
        populateSearch()
        createTableViewSearch()
        mSearchTextField.addIconRight(mNameImage: "ic_search", mColor: UIColor(netHex: Colors.color_gray_less_profile))
    }
    
    override func getPresenter() -> BasePresenter? {
        mPresenterVacancySubTab = VacancySubTabPresenter()
        mPresenterVacancySubTab.searchVacant()
        return mPresenterVacancySubTab
    }

    override func viewWillAppear(_ animated: Bool) {
        mHeightLabelResultConstraint.constant = CGFloat(0)
        mResultsLabel.layoutSubviews()
    }
    
    func addTarget(){
        mSearchTextField.addTarget(self, action: #selector(listenerInput(_:)), for: UIControl.Event.editingChanged)
    }
    
    @objc func listenerInput(_ mTextField : UITextField){
        if(mTextField.text?.count == 0){
            mHeightView.constant = CGFloat(0)
            mListSearchTableView.layoutSubviews()
        }else{
            mHeightView.constant = CGFloat(mListSearch.count * 60)
            mListSearchTableView.layoutSubviews()
        }
    }
    
    func createTableView(){
        mVacancyDataSource = VacancyDataSource(tableView: mListVacancyTableView, mViewController: self, mSettingsDelegate: self)
        mListVacancyTableView.dataSource = mVacancyDataSource
        mListVacancyTableView.delegate = mVacancyDataSource
        mListVacancyTableView.reloadData()
        mVacancyDataSource?.update(items: mListVacancy)
    }
    
    func createTableViewSearch(){
        mSearchDataSource = SearchCoincidenceDataSource(tableView: mListSearchTableView, mViewController: self, mSearchCoincidenceDelegate: self)
        mListSearchTableView.dataSource = mSearchDataSource
        mListSearchTableView.delegate = mSearchDataSource
        mListSearchTableView.reloadData()
        mSearchDataSource?.update(items: mListSearch)
    }
    
    func selectOption(mOption: String) {
        pushViewController(mNameStoryBoard: "VacancyDetail", mViewController: VacancyDetailViewController.self, mNameViewController: "VacancyDetailViewController",mDelegate: self)
    }
    
    @IBAction func showFilterAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "Filters", mViewController: FiltersViewController.self, mNameViewController: "FiltersViewController",mDelegate: self)
    }

    func selectOptionCoincidence(mItem: SearchCoincidenceModel) {
        mSearchTextField.text = mItem.mTitleCoincidence
        mSearchTextField.resignFirstResponder()
        mSearchTextField.endEditing(true)
        mResultsLabel.text = "Resultados para '" + mSearchTextField.text! + "'"
        
        mHeightLabelResultConstraint.constant = CGFloat(32)
        mResultsLabel.layoutSubviews()
        mHeightView.constant = CGFloat(0)
        mListSearchTableView.layoutSubviews()
    }
    
    func populateSearch(){
        mListSearch.append(SearchCoincidenceModel(mIdSearch: "1",mTitleCoincidence: "UX Designer"))
        mListSearch.append(SearchCoincidenceModel(mIdSearch: "2",mTitleCoincidence: "UX Lead"))
        mListSearch.append(SearchCoincidenceModel(mIdSearch: "3",mTitleCoincidence: "UX Freelance"))
    }
}
