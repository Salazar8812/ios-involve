//
//  VacancySubTabPresenter.swift
//  Involve-iOS
//
//  Created by BE-003 on 16/06/21.
//

import UIKit

class VacancySubTabPresenter: BaseInvolvePresenter {
    
    func searchByFilters(){
        let mRequestFilter = SearchVacantFilterRequest()
        BaseRetrofitManager<SearchVacantFilterResponse>.init(requestUrl: ApiDefinitions.WS_SEARCH_VACANCY_FILTERS, delegate: self).requestPost(requestModel: mRequestFilter)
    }
    
    func searchVacant(){
        let mURL = ApiDefinitions.WS_SEARCH_VACANCY
        
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        switch requestUrl {
        case ApiDefinitions.WS_SEARCH_VACANCY_FILTERS:
            OnSuccessFiltersWS(mResponse: response as! SearchVacantFilterResponse)
            break
            
        case ApiDefinitions.WS_SEARCH_VACANCY:
            break
        default:
            break
        }
    }
    
    override func onErrorConnection() {
        
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        
    }
    
    func OnSuccessFiltersWS(mResponse : SearchVacantFilterResponse){
        
    }

}
