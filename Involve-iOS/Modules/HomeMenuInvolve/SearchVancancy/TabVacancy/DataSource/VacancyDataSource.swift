//
//  VacancyDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

protocol VacancyDelegate : NSObjectProtocol{
    func selectOption(mOption: String)
}

class VacancyDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [VacancyModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemVacancyTableViewCell"
    var mViewController:  UIViewController?
    var mSettingsDelegate : VacancyDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mSettingsDelegate : VacancyDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.separatorStyle = .none
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mSettingsDelegate = mSettingsDelegate
    }
    
    func update(items: [VacancyModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 430
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5//self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemVacancyTableViewCell

        cell.mHeaderTop.roundCorners([.topLeft, .topRight], radius: 8)
        cell.mApplyButton.tag = indexPath.row
        cell.mApplyButton.addTarget(self, action: #selector(optionSelect(_:)), for: .touchUpInside)
        return cell
    }
    
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
        print(mPositionTag)
        self.mSettingsDelegate.selectOption(mOption: "")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    

}
