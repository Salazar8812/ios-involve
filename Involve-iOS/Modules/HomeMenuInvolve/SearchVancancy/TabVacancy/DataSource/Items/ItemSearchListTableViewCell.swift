//
//  ItemSearchListTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class ItemSearchListTableViewCell: UITableViewCell {
    @IBOutlet weak var mTitleSearchLabel: UILabel!
    @IBOutlet weak var mClearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
