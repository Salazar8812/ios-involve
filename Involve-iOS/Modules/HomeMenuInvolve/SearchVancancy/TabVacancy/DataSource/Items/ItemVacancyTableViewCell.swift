//
//  ItemVacancyTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/05/21.
//

import UIKit

class ItemVacancyTableViewCell: UITableViewCell {
    @IBOutlet weak var mTitleVacancyLabel: UILabel!
    @IBOutlet weak var mFavsButton: UIButton!
    @IBOutlet weak var mApplyButton: UIButton!
    @IBOutlet weak var mJorneyLabel: UILabel!
    @IBOutlet weak var mLocationLabel: UILabel!
    @IBOutlet weak var mContractLabel: UILabel!
    @IBOutlet weak var mModalityLabel: UILabel!
    @IBOutlet weak var mSalaryLabel: UILabel!
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mDateLabel: UILabel!
    @IBOutlet weak var mFavsImageView: UIImageView!
    @IBOutlet weak var mHeaderTop: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
