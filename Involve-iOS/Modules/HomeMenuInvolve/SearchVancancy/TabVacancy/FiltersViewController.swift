//
//  FiltersViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit
import ZMSwiftRangeSlider

class FiltersViewController: BaseViewController {
    @IBOutlet weak var mRangeView: RangeSlider!
    @IBOutlet weak var mMinValueLabel: UILabel!
    @IBOutlet weak var mMaxLabel: UILabel!
    var mListFilters : [OptionFiltersModel] = []    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
        configuraRange()
    }
    
    func configuraRange(){
        mRangeView.setMinAndMaxRange(0, maxRange: 100000)
        mRangeView.minValueThumbTintColor = UIColor(netHex: Colors.color_gray_less_profile)
        mRangeView.maxValueThumbTintColor = UIColor(netHex: Colors.color_gray_less_profile)
        mRangeView.thumbSize = 17
        mRangeView.thumbOutlineSize = 0.5
        mRangeView.labelsAreBelow = true
        mRangeView.displayTextFontSize = 0.0
        
        mRangeView.setMinValueDisplayTextGetter { (minValue) -> String? in
            let mVal = minValue * 1000
            self.mMinValueLabel.text = "$ " + String(mVal)
                   return "$\(minValue)"
        }
        
        mRangeView.setMaxValueDisplayTextGetter { (maxValue) -> String? in
            let mVal = maxValue * 1000
            self.mMaxLabel.text = "$ " + String(mVal)
                   return "$\(maxValue)"
        }
    }
 
    
    @IBAction func cleantAction(_ sender: Any) {
        
    }
    
    @IBAction func applyAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
