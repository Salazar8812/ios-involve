//
//  QuestionsViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 03/06/21.
//

import UIKit

class QuestionsViewController: BaseViewController {
    @IBOutlet weak var mSendButton: UIButton!
    @IBOutlet weak var mCanceButton: UIButton!
    @IBOutlet weak var mPlusButton: UIButton!
    @IBOutlet weak var mNoButton: UIButton!
    @IBOutlet weak var mYearsLabel: UILabel!
    @IBOutlet weak var mMinusButton: UIButton!
    @IBOutlet weak var mOkButton: UIButton!
    @IBOutlet weak var mContainerNoView: UIView!
    @IBOutlet weak var mContainerOkView: UIView!
    var mCounter : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTarget()
        setNavigationBarBrown()
    }
    
    func addTarget(){
        mSendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        mCanceButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        mOkButton.addTarget(self, action: #selector(okAction), for: .touchUpInside)
        mNoButton.addTarget(self, action: #selector(noAction), for: .touchUpInside)
        mMinusButton.addTarget(self, action: #selector(minusAction), for: .touchUpInside)
        mPlusButton.addTarget(self, action: #selector(plusAction), for: .touchUpInside)
    }
    
    @objc func sendAction(){
        if(mExtras[0] == "postulate"){
            navigationController?.popViewController(animated: true)
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func cancelAction(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func okAction(){
        changeSelectedGenre(mButton: mOkButton, mView: mContainerOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_final_strong_green))
        changeSelectedGenre(mButton: mNoButton, mView:  mContainerNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @objc func noAction(){
        changeSelectedGenre(mButton: mOkButton, mView: mContainerOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mNoButton, mView:  mContainerNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_final_strong_green))
    }
    
    @objc func minusAction(){
        if(mCounter > 0){
         mCounter -= 1
         mYearsLabel.text = String(mCounter)
        }
    }
    
    @objc func plusAction(){
        mCounter += 1
        mYearsLabel.text = String(mCounter)
    }
    
    func changeSelectedGenre(mButton: UIButton, mView : UIView, mSelect: Bool, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mView.backgroundColor = mColorBackground
        if(!mSelect){
            mView.layer.borderWidth = 1
            mView.layer.borderColor = mColorText.cgColor
        }else{
            mView.layer.borderWidth = 0
        }
    }
}
