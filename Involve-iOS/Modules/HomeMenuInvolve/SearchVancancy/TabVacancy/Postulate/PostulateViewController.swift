//
//  PostulateViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit

class PostulateViewController: BaseViewController {

    @IBOutlet weak var mScroll: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func goQuestionsAction(_ sender: Any) {
        pushViewControllerExtra(mNameStoryBoard: "Questions", mViewController: QuestionsViewController.self, mNameViewController: "QuestionsViewController",mDelegate: self, vc: QuestionsViewController(), mExtras:["postulate"])
    }
    
    @IBAction func backVacancyAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated:  animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
