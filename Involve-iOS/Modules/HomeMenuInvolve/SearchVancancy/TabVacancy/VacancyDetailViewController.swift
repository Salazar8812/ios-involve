//
//  VacancyDetailViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class VacancyDetailViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
    }

    @IBAction func postulateAction(_ sender: Any) {
        presentWithNavigationBarHideBack(mNameStoryBoard: "Postulate", mViewController: PostulateViewController.self, mNameViewController: "PostulateViewController")
    }
}
