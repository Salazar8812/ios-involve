//
//  SettingsViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit

class SettingsViewController: BaseViewController, SettingsDelegate {
    @IBOutlet weak var mOptionTableView: UITableView!
    var mSettingsDataSource : SettingsDataSource!
    var mListOptions : [OptionSettingsModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        populateOptions()
        createTableView()
    }
    
    func populateOptions(){
        mListOptions.append(OptionSettingsModel(mIcon:"ic_lock",mOption: "Seguridad e inicio de sesión"))
        mListOptions.append(OptionSettingsModel(mIcon: "ic_notification",mOption: "Administrar notificaciones"))
        mListOptions.append(OptionSettingsModel(mIcon: "ic_terms_conditions",mOption: "Terminos y condiciones"))
        mListOptions.append(OptionSettingsModel(mIcon: "ic_logout",mOption: "Cerrar Sesión"))
    }
    
    func createTableView(){
        mSettingsDataSource = SettingsDataSource(tableView: mOptionTableView, mViewController: self, mSettingsDelegate: self)
        mOptionTableView.dataSource = mSettingsDataSource
        mOptionTableView.delegate = mSettingsDataSource
        mOptionTableView.reloadData()
        mSettingsDataSource?.update(items: mListOptions)
    }

    func selectOption(mOption: String) {
        print(mOption)
        if(mOption == "Cerrar Sesión"){
            adversitingCloseSession(mMessage: "¿Estas seguro de cerrar tu sesión?")
        }
    }
    
    func adversitingCloseSession(mMessage : String){
        let alert = UIAlertController(title: "Involve", message: mMessage, preferredStyle: .alert)
                let ok = UIAlertAction(title: "Cerrar sesión", style: .default, handler: { action in
                    self.presentWithNavigationBar(mNameStoryBoard: "LoginInvolve",mViewController: LoginInvolveViewController.self, mNameViewController: "LoginInvolveViewController")
                })
        let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: { action in
                })
                alert.addAction(cancel)
                alert.addAction(ok)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
    }
    
}
