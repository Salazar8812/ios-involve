//
//  ItemSettingsOptionUITaTableViewCell.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit

class ItemSettingsOptionUITaTableViewCell: UITableViewCell {

    @IBOutlet weak var mOptionLabel: UILabel!
    @IBOutlet weak var mIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
