//
//  SettingsDataSource.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit

protocol SettingsDelegate : NSObjectProtocol{
    func selectOption(mOption: String)
}

class SettingsDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [OptionSettingsModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemSettingsOptionUITaTableViewCell"
    var mViewController:  UIViewController?
    var mSettingsDelegate : SettingsDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mSettingsDelegate : SettingsDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mSettingsDelegate = mSettingsDelegate
    }
    
    func update(items: [OptionSettingsModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemSettingsOptionUITaTableViewCell

        cell.mOptionLabel.text = item.mOption
        cell.mIconImageView.image = UIImage(named:item.mIcon!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mSettingsDelegate.selectOption(mOption: mSectionsArray[indexPath.row].mOption!)
    }
    

}
