//
//  CourseViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol CourseDelegate: NSObjectProtocol {
    func saveCourse()
}

class CourseViewController: BaseViewController, UIGestureRecognizerDelegate, CalendarDelegate {
   
    @IBOutlet weak var mBackButton: UIButton!
    var mCourseDelegate : CourseDelegate!
    @IBOutlet weak var mHoursDurationTextField: TextFieldUtils!
    
    @IBOutlet weak var mExpeditionDateTextField: TextFieldUtils!
    @IBOutlet weak var mInstitutionTextField: TextFieldUtils!
    @IBOutlet weak var mCourseTextField: TextFieldUtils!
    let validatorManager = ValidatorText()

    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setIcons()
        addTaps()
        addValidators()
    }
    
    func addValidators(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mCourseTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mInstitutionTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mExpeditionDateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mHoursDurationTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func addTaps(){
        let tapDate = UITapGestureRecognizer(target: self, action: #selector(launchDate))
        tapDate.delegate = self
        mExpeditionDateTextField.addGestureRecognizer(tapDate)
    }
    
    @objc func launchDate(){
        launchBlurControllerCalendar(mNameStoryBoard: "Calendar", mViewController: CalendarViewController.self,mNameViewController: "CalendarViewController", mDelegate: self, mField: mExpeditionDateTextField)
    }
    
    func setIcons(){
        mExpeditionDateTextField.addIconLeftCalendar(mNameImage: "ic_calendar", mColor: UIColor(netHex: Colors.color_grey_select))
    }
    
    func setDelegate(){
        mCourseDelegate = (mDelegate as! CourseDelegate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    

    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mCourseDelegate.saveCourse()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getSelectedDate(mDate: String, mField: TextFieldUtils) {
        mField.text = mDate
    }

}
