//
//  PersonalInformationViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol PersonalInformationDelegate : NSObjectProtocol {
    func savePersonal(mProfile : String , mSalary : String)
}

class PersonalInformationViewController: BaseViewController {

    @IBOutlet weak var mUploadFileView: UIView!
    @IBOutlet weak var mUploadCVView: UIView!
    @IBOutlet weak var mBackButton: UIButton!
    var mPersonalInformationDelegate : PersonalInformationDelegate!
    let validatorManager = ValidatorText()
    @IBOutlet weak var mSalaryTextField: TextFieldUtils!
    
    @IBOutlet weak var mNameProfileTextField: TextFieldUtils!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        addStyle()
        setDelegate()
        addValidtors()
        setIconMoney()
    }
    
    func setIconMoney(){
        mSalaryTextField.addTextLeft(mChar: "$", mColor: UIColor(netHex: Colors.color_grey_select))
    }
    
    func addValidtors(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mNameProfileTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mSalaryTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func setDelegate(){
        mPersonalInformationDelegate = (mDelegate as! PersonalInformationDelegate)
    }
    
    func addStyle(){
        mUploadCVView.addDashedBorder()
        mUploadFileView.addDashedBorder()
    }
    
    @IBAction func mUploadCVAction(_ sender: Any) {
    }
   
    
    @IBAction func mUploadFileAction(_ sender: Any) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mPersonalInformationDelegate.savePersonal(mProfile: mNameProfileTextField.text!, mSalary: mSalaryTextField.text!)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }

}
