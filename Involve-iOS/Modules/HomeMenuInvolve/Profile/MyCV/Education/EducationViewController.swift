//
//  EducationViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol EducationDelegate: NSObjectProtocol{
    func saveEducation()
}

class EducationViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var mBackButton: UIButton!
    var mEducationDelegate : EducationDelegate!
    
    @IBOutlet weak var mLevelStudyTextField: TextFieldUtils!
    @IBOutlet weak var mStatusTextField: TextFieldUtils!
    @IBOutlet weak var mBeginYearTextField: TextFieldUtils!
    @IBOutlet weak var mEndYearTextField: TextFieldUtils!
    @IBOutlet weak var mInstitutionTextField: TextFieldUtils!
    @IBOutlet weak var mSkillGetTextField: TextFieldUtils!
    let validatorManager = ValidatorText()

    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setIcons()
        addTaps()
        addValidators()
    }
    
    func addValidators(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mLevelStudyTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mSkillGetTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mInstitutionTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mStatusTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mBeginYearTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mEndYearTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func setIcons(){
        mLevelStudyTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mStatusTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mBeginYearTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mEndYearTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func addTaps(){
        let tapLevel = UITapGestureRecognizer(target: self, action: #selector(launchLevel))
        tapLevel.delegate = self
        mLevelStudyTextField.addGestureRecognizer(tapLevel)
        
        let tapStatus = UITapGestureRecognizer(target: self, action: #selector(launchStatus))
        tapStatus.delegate = self
        mStatusTextField.addGestureRecognizer(tapStatus)
        
        let tapBeginDate = UITapGestureRecognizer(target: self, action: #selector(lauchBeginDate))
        tapBeginDate.delegate = self
        mBeginYearTextField.addGestureRecognizer(tapBeginDate)
        
        let tapEndDate = UITapGestureRecognizer(target: self, action: #selector(launchEndDate))
        tapEndDate.delegate = self
        mEndYearTextField.addGestureRecognizer(tapEndDate)
    }
    
    @objc func launchLevel(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Doctorado"),
                                    OptionSettingsModel(mIcon: "", mOption: "Posgrado"),
                                    OptionSettingsModel(mIcon: "", mOption: "Maestris"),
                                    OptionSettingsModel(mIcon: "", mOption: "Licenciatura"),
                                    OptionSettingsModel(mIcon: "", mOption: "Preparatoria")]
        mGenericList?.mTextField = mLevelStudyTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func launchStatus(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Junior"),
                                    OptionSettingsModel(mIcon: "", mOption: "Senior"),
                                    OptionSettingsModel(mIcon: "", mOption: "Novato")]
        mGenericList?.mTextField = mStatusTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func lauchBeginDate(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "2021"),
                                    OptionSettingsModel(mIcon: "", mOption: "2020"),
                                    OptionSettingsModel(mIcon: "", mOption: "2019")]
        mGenericList?.mTextField = mBeginYearTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func launchEndDate(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "2021"),
                                    OptionSettingsModel(mIcon: "", mOption: "2020"),
                                    OptionSettingsModel(mIcon: "", mOption: "2019")]
        mGenericList?.mTextField = mEndYearTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    func setDelegate(){
        mEducationDelegate = (mDelegate as! EducationDelegate)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mEducationDelegate.saveEducation()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }

}
