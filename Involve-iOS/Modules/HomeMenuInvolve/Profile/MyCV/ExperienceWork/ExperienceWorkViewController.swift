//
//  ExperienceWorkViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol ExperienceWorkDelegate : NSObjectProtocol {
    func saveExperience()
}

class ExperienceWorkViewController: BaseViewController, UIGestureRecognizerDelegate, CalendarDelegate {
    
    
    @IBOutlet weak var mBackButton: UIButton!
    var mExperienceWorkDelegate : ExperienceWorkDelegate!
    @IBOutlet weak var mVacancyTextField: TextFieldUtils!
    @IBOutlet weak var mTypeVacancyTextField: TextFieldUtils!
    @IBOutlet weak var mEnterpriseTextField: TextFieldUtils!
    @IBOutlet weak var mBeginDateTextField: TextFieldUtils!
    @IBOutlet weak var mFunctionDoneTextField: TextFieldUtils!
    @IBOutlet weak var mEndDateTextField: TextFieldUtils!
    
    @IBOutlet weak var mNoButton: UIButton!
    @IBOutlet weak var mNoView: UIView!
    @IBOutlet weak var mOkButton: UIButton!
    @IBOutlet weak var mOkView: UIView!
    let validatorManager = ValidatorText()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setIcons()
        addTaps()
        addValidators()
    }
    
    func addTaps(){
        
        let tapBeginDate = UITapGestureRecognizer(target: self, action: #selector(launchCalendarBegin))
        tapBeginDate.delegate = self
        mBeginDateTextField.addGestureRecognizer(tapBeginDate)
        
        let tapEndDate = UITapGestureRecognizer(target: self, action: #selector(launchCalendarEnd))
        tapEndDate.delegate = self
        mEndDateTextField.addGestureRecognizer(tapEndDate)
        
        let tapJob = UITapGestureRecognizer(target: self, action: #selector(launchTypeJob))
        tapJob.delegate = self
        mTypeVacancyTextField.addGestureRecognizer(tapJob)
    }
    
    @objc func launchCalendarBegin(){
        launchBlurControllerCalendar(mNameStoryBoard: "Calendar", mViewController: CalendarViewController.self,mNameViewController: "CalendarViewController", mDelegate: self, mField: mBeginDateTextField)
    }
    
    @objc func launchCalendarEnd(){
        launchBlurControllerCalendar(mNameStoryBoard: "Calendar", mViewController: CalendarViewController.self,mNameViewController: "CalendarViewController", mDelegate: self, mField: mEndDateTextField)
    }
    
    @objc func launchTypeJob(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Lider de proyecto"),
                                    OptionSettingsModel(mIcon: "", mOption: "Gerente"),
                                    OptionSettingsModel(mIcon: "", mOption: "Desarrollador"),
                                    OptionSettingsModel(mIcon: "", mOption: "Junior"),
                                    OptionSettingsModel(mIcon: "", mOption: "Senior")]
        mGenericList?.mTextField = mTypeVacancyTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    func addValidators(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mVacancyTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mTypeVacancyTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mEnterpriseTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mBeginDateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mEndDateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mFunctionDoneTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    @IBAction func mNoAction(_ sender: Any) {
        changeSelectedGenre(mButton: mNoButton, mView: mNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        
        changeSelectedGenre(mButton: mOkButton, mView: mOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @IBAction func mOkAction(_ sender: Any) {
        changeSelectedGenre(mButton: mNoButton, mView: mNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        
        changeSelectedGenre(mButton: mOkButton, mView: mOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func setIcons(){
        mTypeVacancyTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        
        mBeginDateTextField.addIconLeftCalendar(mNameImage: "ic_calendar", mColor: UIColor(netHex: Colors.color_branding_blue))
        
        mEndDateTextField.addIconLeftCalendar(mNameImage: "ic_calendar", mColor: UIColor(netHex: Colors.color_branding_blue))
        
    }
    
    func setDelegate(){
        mExperienceWorkDelegate = (mDelegate as! ExperienceWorkDelegate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mExperienceWorkDelegate.saveExperience()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeSelectedGenre(mButton: UIButton, mView : UIView, mSelect: Bool, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mView.backgroundColor = mColorBackground
        if(!mSelect){
            mView.layer.borderWidth = 1
            mView.layer.borderColor = mColorText.cgColor
        }else{
            mView.layer.borderWidth = 0
        }
    }
    
    func getSelectedDate(mDate: String, mField: TextFieldUtils) {
        mField.text = mDate
    }

}
