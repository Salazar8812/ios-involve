//
//  TechnicalKnowledgeDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit


protocol TechnicalKnowledgeDelegate : NSObjectProtocol{
    func removeItem()
}

class TechnicalKnowledgeDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    let reuseIdentifier = "IitemTechnicalKnowledgeCollectionViewCell" // also enter this string as the cell identifier in the storyboard
    var mListTechnical : [TechnicalModel] = []
    var mListPhotosCollectionView : UICollectionView!
    var mDelegate : TechnicalKnowledgeDelegate?
    
    init(mListTechnical : [TechnicalModel], mListPhotosCollectionView : UICollectionView, mDelegate : TechnicalKnowledgeDelegate) {
        super.init()
        self.mListTechnical = mListTechnical
        self.mListPhotosCollectionView = mListPhotosCollectionView
        self.mListPhotosCollectionView.delegate = self
        self.mDelegate = mDelegate
        self.mListPhotosCollectionView.dataSource = self
        let nibName = UINib(nibName: "IitemTechnicalKnowledgeCollectionViewCell", bundle:nil)
        mListPhotosCollectionView.register(nibName, forCellWithReuseIdentifier: "IitemTechnicalKnowledgeCollectionViewCell")

    }
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mListTechnical.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
               let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
               let size:CGFloat = (mListPhotosCollectionView.frame.size.width - space) / 2.0
               return CGSize(width: size, height: size)
    }

    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! IitemTechnicalKnowledgeCollectionViewCell
        
        cell.mTitleLabel.text = mListTechnical[indexPath.row].mTitle
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        //mDelegate?.onItemSelect(mNameImage: mListPhotos[indexPath.item])
    }

}
