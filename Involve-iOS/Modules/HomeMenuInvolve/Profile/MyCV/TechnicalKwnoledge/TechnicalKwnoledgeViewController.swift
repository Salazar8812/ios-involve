//
//  TechnicalKwnoledgeViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 11/05/21.
//

import UIKit

protocol TechnicalKwnoledgeVCDelegate : NSObjectProtocol {
    func saveTechnicalKnowlegde(mListTechnical : [TechnicalModel])
}

class TechnicalKwnoledgeViewController: BaseViewController, UIGestureRecognizerDelegate, TechnicalDelegate {
  
    
    @IBOutlet weak var mInputTechnicalTextField: TextFieldUtils!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mPredictionTableView: UITableView!
    @IBOutlet weak var mListTechnicalTableView: UITableView!
    @IBOutlet weak var mLevelTextField: TextFieldUtils!
    var mTechnicalDataSource : TechnicalDataSource?
    var mListTechnical : [TechnicalModel] = []
    @IBOutlet weak var mAddButton: UIButton!
    var mId : Int = 0
    let validatorManager = ValidatorText()
    var mTechnicalKwnoledgeVCDelegate: TechnicalKwnoledgeVCDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        setIconLeft()
        addTab()
        createTableView()
        addTarget()
        addValidator()
        setDelegate()
    }
    
    func setDelegate(){
        mTechnicalKwnoledgeVCDelegate = (mDelegate as! TechnicalKwnoledgeVCDelegate)
    }
    
    func addTarget(){
        mAddButton.addTarget(self, action: #selector(addAction), for: .touchUpInside)
    }
    
    func addValidator(){
        mInputTechnicalTextField.delegate = validatorManager
        mLevelTextField.delegate = validatorManager
        
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mInputTechnicalTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mLevelTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    @objc func addAction(){
        if(validatorManager.validate()){
            mId += 1
            let mItem = mInputTechnicalTextField.text! + "-" + mLevelTextField.text!
            mListTechnical.append(TechnicalModel(mTitle: mItem, mId: String(mId)))
            mTechnicalDataSource?.update(items: mListTechnical)
            mInputTechnicalTextField.text = ""
            mLevelTextField.text = ""
        }
    }
    
    func createTableView(){
        mTechnicalDataSource = TechnicalDataSource(tableView: mListTechnicalTableView, mViewController: self, mTechnicalDelegate: self)
        mListTechnicalTableView.dataSource = mTechnicalDataSource
        mListTechnicalTableView.delegate = mTechnicalDataSource
        mListTechnicalTableView.reloadData()
        mTechnicalDataSource?.update(items: mListTechnical)
    }
    
    func addTab(){
        let tapLevel = UITapGestureRecognizer(target: self, action: #selector(launchLevel))
        tapLevel.delegate = self
        mLevelTextField.addGestureRecognizer(tapLevel)
    }
    
    @objc func launchLevel(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Experto"),
                                    OptionSettingsModel(mIcon: "", mOption: "Profesional"),
                                    OptionSettingsModel(mIcon: "", mOption: "Intermedio"),
                                    OptionSettingsModel(mIcon: "", mOption: "Básico")]
        mGenericList?.mTextField = mLevelTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    func setIconLeft(){
        mLevelTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
    }
    
    @IBAction func saveAction(_ sender: Any) {
        mTechnicalKwnoledgeVCDelegate.saveTechnicalKnowlegde(mListTechnical: mListTechnical)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func removeTechnical(mId: String) {
    }
}
