//
//  ItemTechnicalTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit

class ItemTechnicalTableViewCell: UITableViewCell {
    @IBOutlet weak var mTrashButton: UIButton!
    @IBOutlet weak var mTitleTechnicalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
