//
//  TechnicalDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit
protocol  TechnicalDelegate:  NSObjectProtocol {
    func removeTechnical(mId: String)
}

class TechnicalDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [TechnicalModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemTechnicalTableViewCell"
    var mViewController:  UIViewController?
    var mTechnicalDelegate : TechnicalDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mTechnicalDelegate : TechnicalDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mTechnicalDelegate = mTechnicalDelegate
    }
    
    func update(items: [TechnicalModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemTechnicalTableViewCell
        cell.mTitleTechnicalLabel.text = item.mTitle
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    

}

class TechnicalModel : NSObject{
    var mTitle : String?
    var mId : String?
    
    init(mTitle : String , mId: String) {
        self.mTitle = mTitle
        self.mId = mId
    }
}
