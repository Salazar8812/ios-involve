//
//  PresenceOnlineViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol PresenceOnlineDelegate : NSObjectProtocol {
    func savePresence()
}

class PresenceOnlineViewController: BaseViewController {
    @IBOutlet weak var mBackButton: UIButton!
    var mPresenceOnlineDelegate : PresenceOnlineDelegate!
    
    @IBOutlet weak var mUrlTextField: UITextField!
    @IBOutlet weak var mLinkedInTextField: TextFieldUtils!
    @IBOutlet weak var mBehanceTextField: TextFieldUtils!
    @IBOutlet weak var mGitHubTextField: TextFieldUtils!
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setPaddingLeft()
    }
    
    func setDelegate(){
        mPresenceOnlineDelegate = (mDelegate as! PresenceOnlineDelegate)
    }
    
    func setPaddingLeft(){
        mGitHubTextField.addTextLeft(mChar: " ", mColor: UIColor(netHex: Colors.color_white))
        mBehanceTextField.addTextLeft(mChar: " ", mColor: UIColor(netHex: Colors.color_white))
        mLinkedInTextField.addTextLeft(mChar: " ", mColor: UIColor(netHex: Colors.color_white))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        mPresenceOnlineDelegate.savePresence()
        self.navigationController?.popViewController(animated: true)
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }

}
