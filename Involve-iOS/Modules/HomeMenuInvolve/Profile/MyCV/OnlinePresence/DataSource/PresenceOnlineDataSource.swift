//
//  PresenceOnlineDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 28/04/21.
//

import UIKit

class PresenceOnlineDataSource: NSObject, iCarouselDataSource, iCarouselDelegate {
    var mICarousel: iCarousel
    var mItems : [String] = []
    var mSelectedIndex : Int = 0

    init(carrusel : iCarousel){
        mICarousel = carrusel
    }
    
    func settings(){
        mICarousel.type = iCarouselType.linear
        mICarousel.delegate = self
        mICarousel.dataSource = self
    }
    
    func update(items : [String]){
        mItems = items
        mICarousel.reloadData()
        mICarousel.currentItemIndex = items.count - 1
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        //mPageControl?.numberOfPages = mItems.count
        return mItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view = Bundle.main.loadNibNamed("ItemPresenceOnlineCollectionReusableView", owner: self, options: nil)![0] as! ItemPresenceOnlineCollectionReusableView
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        mSelectedIndex = index
        //mNumberPhoneDataDelegate?.onItemClick(numberSelected: mItems[mSelectedIndex])
    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
       // mPageControl?.currentPage = Int(carousel.currentItemIndex)
    }
    
}
