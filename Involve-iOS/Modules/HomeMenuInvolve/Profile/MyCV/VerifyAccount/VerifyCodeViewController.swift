//
//  VerifyCodeViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 10/05/21.
//

import UIKit

class VerifyCodeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
    }

    @IBAction func verifyAction(_ sender: Any) {
        presentViewController(mNameStoryBoard: "VerifyPhoneDone", mViewController: VerifyPhoneDoneViewController.self, mNameViewController: "VerifyPhoneDoneViewController")
        DispatchQueue.main.async {
            let controllers = self.navigationController?.viewControllers
                     for vc in controllers! {
                       if vc is VerifyAccountViewController {
                        _ = self.navigationController?.popToViewController(vc as! VerifyAccountViewController, animated: true)
                       }
                    }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

