//
//  VerifyEmailViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 10/05/21.
//

import UIKit

class VerifyEmailViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        presentViewController(mNameStoryBoard: "VerifyEmailDone", mViewController: VerifyEmailDoneViewController.self, mNameViewController: "VerifyEmailDoneViewController")
        navigationController?.popViewController(animated: true)

    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
