//
//  PhonesDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 11/05/21.
//

import UIKit

protocol PhoneDelegate: NSObjectProtocol {
    func phoneSelect(mPhone : String)
}

class PhonesDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [String] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemPhonesTableViewCell"
    var mViewController:  UIViewController?
    var mPhoneDelegate : PhoneDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mPhoneDelegate : PhoneDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mPhoneDelegate = mPhoneDelegate
    }
    
    func update(items: [String]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemPhonesTableViewCell

        cell.mPhoneTextField.text = item
        
        return cell
    }
    
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mPhoneDelegate.phoneSelect(mPhone: mSectionsArray[indexPath.row])
    }
    

}
