//
//  VerifyPhoneViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 10/05/21.
//

import UIKit

class VerifyPhoneViewController: BaseViewController, UIGestureRecognizerDelegate , PhoneDelegate{
    @IBOutlet weak var mPhoneCompleteTextField: UITextField!
    @IBOutlet weak var mPhoneTextField: UITextField!
    @IBOutlet weak var mListPhoneTableView: UITableView!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    var mPhoneDataSource : PhonesDataSource!
    var mListPhones : [String] = []
    
    @IBOutlet weak var mHeightConstraintView: NSLayoutConstraint!
    static var mVerifyPhone : VerifyPhoneViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        VerifyPhoneViewController.mVerifyPhone = self
        setNavigationBarBrown()
        mPhoneTextField.addIconRightMin(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_grey_select))
        addTargets()
        populatePhones()
        createTableView()
    }
    

    func calculateHeight(mHeight : Int){
        mHeightConstraintView.constant = CGFloat(mHeight)
    }
    
    func createTableView(){
        mPhoneDataSource = PhonesDataSource(tableView: mListPhoneTableView, mViewController: self, mPhoneDelegate: self)
        mListPhoneTableView.dataSource = mPhoneDataSource
        mListPhoneTableView.delegate = mPhoneDataSource
        mListPhoneTableView.reloadData()
        mPhoneDataSource?.update(items: mListPhones)
    }
    
    func populatePhones(){
        mListPhones.append("52")
        mListPhones.append("23")
        mListPhones.append("12")
        mListPhones.append("08")
        mListPhones.append("45")
        mListPhones.append("11")
        mListPhones.append("03")
        mListPhones.append("78")
        mListPhones.append("02")
    }
    
    func addTargets(){
        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(loadListPhone))
        tapCountry.delegate = self
        mPhoneTextField.addGestureRecognizer(tapCountry)
    }
    
    @objc func loadListPhone(){
        calculateHeight(mHeight: 300)
    }
    
    @IBAction func mVerifyAction(_ sender: Any) {
        pushViewControllerExtra(mNameStoryBoard: "VerifyCode", mViewController: VerifyCodeViewController.self, mNameViewController: "VerifyCodeViewController", mDelegate: self, vc: VerifyPhoneViewController.mVerifyPhone, mExtras: [""])
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func phoneSelect(mPhone: String) {
        calculateHeight(mHeight: 0)
        mPhoneTextField.text = mPhone
    }
    
}
