//
//  VerifyPhonePresenter.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/05/21.
//

import UIKit

protocol  VerifyPhoneDelegate: NSObjectProtocol {
    func verify()
}

class VerifyPhonePresenter: BaseInvolvePresenter {

    var mViewController : UIViewController!
    var mVerifyPhoneDelegate : VerifyPhoneDelegate!
    
    init(mViewController : UIViewController, mVerifyPhoneDelegate : VerifyPhoneDelegate) {
        self.mViewController = mViewController
        self.mVerifyPhoneDelegate = mVerifyPhoneDelegate
    }
    
    func verifyPhone(){
        
    }
    
    func onSucccessPhone(){
        
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        
    }
    
    override func onErrorConnection() {
        
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        
    }
    
}
