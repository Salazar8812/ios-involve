//
//  VerifyAccountViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 10/05/21.
//

import UIKit

class VerifyAccountViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
    }

    @IBAction func mEmailVerifyAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "VerifyEmail", mViewController: VerifyEmailViewController.self, mNameViewController: "VerifyEmailViewController", mDelegate: self)
    }
    
    @IBAction func mPhoneNumberVerifyAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "VerifyPhone", mViewController: VerifyPhoneViewController.self, mNameViewController: "VerifyPhoneViewController", mDelegate: self)
    }
}
