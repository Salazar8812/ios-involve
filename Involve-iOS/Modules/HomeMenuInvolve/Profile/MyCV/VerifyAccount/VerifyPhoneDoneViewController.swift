//
//  VerifyPhoneDoneViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 11/05/21.
//

import UIKit

class VerifyPhoneDoneViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func continueAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
