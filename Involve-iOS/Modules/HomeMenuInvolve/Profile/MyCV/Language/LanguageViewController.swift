//
//  LanguageViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol LanguageDelegate : NSObjectProtocol {
    func saveLanguage(mListTechnical : [TechnicalModel])
}

class LanguageViewController: BaseViewController, UIGestureRecognizerDelegate, TechnicalDelegate {
    @IBOutlet weak var mAddButton: UIButton!
    @IBOutlet weak var mLanguageTableView: UITableView!
    @IBOutlet weak var mBackButton: UIButton!
    var mLanguageDelegate : LanguageDelegate!
    
    @IBOutlet weak var mLevelTextField: TextFieldUtils!
    @IBOutlet weak var mLanguageTextField: TextFieldUtils!
    let validatorManager = ValidatorText()
    var mId : Int = 0
    var mListTechnical : [TechnicalModel] = []
    var mTechnicalDataSource : TechnicalDataSource?

    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setIcons()
        addTaps()
        addValidators()
        createTableView()
    }
    
    func createTableView(){
        mTechnicalDataSource = TechnicalDataSource(tableView: mLanguageTableView, mViewController: self, mTechnicalDelegate: self)
        mLanguageTableView.dataSource = mTechnicalDataSource
        mLanguageTableView.delegate = mTechnicalDataSource
        mLanguageTableView.reloadData()
        mTechnicalDataSource?.update(items: mListTechnical)
    }
    
    func addTaps(){
        
        let tapLanguage = UITapGestureRecognizer(target: self, action: #selector(launchLanguage))
        tapLanguage.delegate = self
        mLanguageTextField.addGestureRecognizer(tapLanguage)
        
        let tapLevel = UITapGestureRecognizer(target: self, action: #selector(launchLevel))
        tapLevel.delegate = self
        mLevelTextField.addGestureRecognizer(tapLevel)
        
        mAddButton.addTarget(self, action: #selector(addAction), for: .touchUpInside)
    }

    @objc func launchLanguage(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Inglés"),
                                    OptionSettingsModel(mIcon: "", mOption: "Chino"),
                                    OptionSettingsModel(mIcon: "", mOption: "Aleman"),
                                    OptionSettingsModel(mIcon: "", mOption: "Frances"),
                                    OptionSettingsModel(mIcon: "", mOption: "Italiano")]
        mGenericList?.mTextField = mLanguageTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func launchLevel(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Experto"),
                                    OptionSettingsModel(mIcon: "", mOption: "Avanzado"),
                                    OptionSettingsModel(mIcon: "", mOption: "Intermedio"),
                                    OptionSettingsModel(mIcon: "", mOption: "Basico")]
        mGenericList?.mTextField = mLevelTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    func addValidators(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mLanguageTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mLevelTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    @objc func addAction(){
        if(validatorManager.validate()){
            mId += 1
            let mItem = mLanguageTextField.text! + "-" + mLevelTextField.text!
            mListTechnical.append(TechnicalModel(mTitle: mItem, mId: String(mId)))
            mTechnicalDataSource?.update(items: mListTechnical)
            mLanguageTextField.text = ""
            mLevelTextField.text = ""
        }
    }
    
    func setDelegate(){
        mLanguageDelegate = (mDelegate as! LanguageDelegate)
    }
    
    func setIcons(){
        mLevelTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_grey_select))
        mLanguageTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_grey_select))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func saveAction(_ sender: Any) {
        mLanguageDelegate.saveLanguage(mListTechnical: mListTechnical)
        self.navigationController?.popViewController(animated: true)
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func removeTechnical(mId: String) {
        
    }
    
}
