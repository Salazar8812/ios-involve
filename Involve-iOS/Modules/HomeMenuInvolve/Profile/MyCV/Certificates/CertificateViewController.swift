//
//  CertificateViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit
protocol CertificateDelegate : NSObjectProtocol {
    func saveCertificate()
}

class CertificateViewController: BaseViewController, UIGestureRecognizerDelegate,CalendarDelegate {

    @IBOutlet weak var mBackButton: UIButton!
    var mCertificateDelegate : CertificateDelegate!
    
    @IBOutlet weak var mUrlTextField: TextFieldUtils!
    @IBOutlet weak var mIdCredentialTextField: TextFieldUtils!
    @IBOutlet weak var mExpiredDateTextField: TextFieldUtils!
    @IBOutlet weak var mExpeditionDateTextField: TextFieldUtils!
    @IBOutlet weak var mInstitutionTextField: TextFieldUtils!
    @IBOutlet weak var mCertificationTextField: TextFieldUtils!
    
    @IBOutlet weak var mNoView: UIView!
    @IBOutlet weak var mOkView: UIView!
    @IBOutlet weak var mNoButton: UIButton!
    @IBOutlet weak var mOKButton: UIButton!
    
    let validatorManager = ValidatorText()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setDelegate()
        setIcons()
        addTaps()
        addValidators()
    }
    
    
    func addValidators(){
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mCertificationTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mInstitutionTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mExpeditionDateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mExpiredDateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mIdCredentialTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mUrlTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func addTaps(){
        let tapDateBegin = UITapGestureRecognizer(target: self, action: #selector(launchBeginCalendar))
        tapDateBegin.delegate = self
        mExpeditionDateTextField.addGestureRecognizer(tapDateBegin)
        
        let tapEndDate = UITapGestureRecognizer(target: self, action: #selector(launchEndCalendar))
        tapEndDate.delegate = self
        mExpiredDateTextField.addGestureRecognizer(tapEndDate)
    }
    
    @objc func launchBeginCalendar(){
        launchBlurControllerCalendar(mNameStoryBoard: "Calendar", mViewController: CalendarViewController.self,mNameViewController: "CalendarViewController", mDelegate: self, mField: mExpeditionDateTextField)
    }
    
    @objc func launchEndCalendar(){
        launchBlurControllerCalendar(mNameStoryBoard: "Calendar", mViewController: CalendarViewController.self,mNameViewController: "CalendarViewController", mDelegate: self, mField: mExpiredDateTextField)
    }
    
    @objc func okAction(){
        changeSelectedGenre(mButton: mOKButton, mView: mOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_final_strong_green))
        
        changeSelectedGenre(mButton: mNoButton, mView: mNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @objc func noAction(){
        changeSelectedGenre(mButton: mOKButton, mView: mOkView, mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        
        changeSelectedGenre(mButton: mNoButton, mView: mNoView, mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_final_strong_green))
    }
    
    func setIcons(){
        mExpeditionDateTextField.addIconLeftCalendar(mNameImage: "ic_calendar", mColor: UIColor(netHex: Colors.color_grey_select))
        mExpiredDateTextField.addIconLeftCalendar(mNameImage: "ic_calendar", mColor: UIColor(netHex: Colors.color_grey_select))
        
    }
    
    func setDelegate(){
        mCertificateDelegate = (mDelegate as! CertificateDelegate)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mCertificateDelegate.saveCertificate()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        mOKButton.addTarget(self, action: #selector(okAction), for: .touchUpInside)
        
        mNoButton.addTarget(self, action: #selector(noAction), for: .touchUpInside)
    }
    
    func getSelectedDate(mDate: String, mField: TextFieldUtils) {
        mField.text = mDate
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeSelectedGenre(mButton: UIButton, mView : UIView, mSelect: Bool, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mView.backgroundColor = mColorBackground
        if(!mSelect){
            mView.layer.borderWidth = 1
            mView.layer.borderColor = mColorText.cgColor
        }else{
            mView.layer.borderWidth = 0
        }
    }
}
