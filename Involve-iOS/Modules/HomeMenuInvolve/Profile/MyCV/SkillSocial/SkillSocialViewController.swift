//
//  SkillSocialViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 11/05/21.
//

import UIKit

protocol SkillSocialVCDelegate : NSObjectProtocol {
    func saveSkillSocial(mListTechnical : [TechnicalModel])
}

class SkillSocialViewController: BaseViewController, TechnicalDelegate {

    @IBOutlet weak var mListSkillSocialTableView: UITableView!
    @IBOutlet weak var mAddButton: UIButton!
    @IBOutlet weak var mInputTextField: TextFieldUtils!
    var mSkillSocialDelegate: SkillSocialVCDelegate!
    var mTechnicalDataSource : TechnicalDataSource?
    var mListTechnical : [TechnicalModel] = []
    let validatorManager = ValidatorText()
    var mId : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        createTableView()
        addTarget()
        addValidator()
    }
    
    func createTableView(){
        mTechnicalDataSource = TechnicalDataSource(tableView: mListSkillSocialTableView, mViewController: self, mTechnicalDelegate: self)
        mListSkillSocialTableView.dataSource = mTechnicalDataSource
        mListSkillSocialTableView.delegate = mTechnicalDataSource
        mListSkillSocialTableView.reloadData()
        mTechnicalDataSource?.update(items: mListTechnical)
    }
    
    func setDelegate(){
        mSkillSocialDelegate = (mDelegate as! SkillSocialVCDelegate)
    }
    
    func addTarget(){
        mAddButton.addTarget(self, action: #selector(addAction), for: .touchUpInside)
    }
    
    func addValidator(){
        mInputTextField.delegate = validatorManager
        
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mInputTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    @objc func addAction(){
        if(validatorManager.validate()){
            mId += 1
            let mItem = mInputTextField.text!
            mListTechnical.append(TechnicalModel(mTitle: mItem, mId: String(mId)))
            mTechnicalDataSource?.update(items: mListTechnical)
            mInputTextField.text = ""
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        mSkillSocialDelegate.saveSkillSocial(mListTechnical: mListTechnical)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func removeTechnical(mId: String) {
        
    }
}
