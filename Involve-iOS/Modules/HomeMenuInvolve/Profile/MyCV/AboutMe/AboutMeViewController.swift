//
//  AboutMeViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 26/04/21.
//

import UIKit

protocol AboutMeDelegate : NSObjectProtocol {
    func saveAbout(mGenre : String , mLocation : String, mState : String, mNationality: String, mName : String, mLastName : String)
}

class AboutMeViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var mBackButton: UIButton!
    @IBOutlet weak var mContainerView: UIView!
    @IBOutlet weak var mEditButton: UIButton!
    @IBOutlet weak var mEditImageView: UIImageView!
    @IBOutlet weak var mProfileImageView: UIImageView!
    
    @IBOutlet weak var mMarriedView: UIView!
    @IBOutlet weak var mSingleView: UIView!
    
    @IBOutlet weak var mWomanView: UIView!
    @IBOutlet weak var mMaleView: UIView!
    @IBOutlet weak var mNoBinaryView: UIView!
    
    @IBOutlet weak var mWomanButton: UIButton!
    @IBOutlet weak var mNoBinaryButton: UIButton!
    
    @IBOutlet weak var mMarriedButton: UIButton!
    @IBOutlet weak var mSingleButton: UIButton!
    @IBOutlet weak var mMaleButton: UIButton!
    var mArraysViews : [UIView] = []
    var mAboutMeDelegate: AboutMeDelegate!
    
    @IBOutlet weak var mNameTextField: TextFieldUtils!
    @IBOutlet weak var mMiddleLastName: TextFieldUtils!
    @IBOutlet weak var mLastName: TextFieldUtils!
    @IBOutlet weak var mCountryTextField: TextFieldUtils!
    @IBOutlet weak var mStateTextField: TextFieldUtils!
    @IBOutlet weak var mCityTextField: TextFieldUtils!
    
    @IBOutlet weak var mNationalityTextField: TextFieldUtils!
    var imagePicker: UIImagePickerController?
    let nombreImagen = "Foto"
    let validatorManager = ValidatorText()
    var mGenre : String = ""
    var mMarried : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        setStyle()
        addViews()
        setDelegate()
        setIcons()
        addTaps()
        addValidtors()
    }
    
    func addValidtors(){
        mCountryTextField.delegate = validatorManager
        mCityTextField.delegate = validatorManager
        mStateTextField.delegate = validatorManager
        mNameTextField.delegate = validatorManager
        mMiddleLastName.delegate = validatorManager
        mLastName.delegate = validatorManager
        
        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mNameTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mMiddleLastName, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mLastName, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mCountryTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mStateTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mCityTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),ValidatorField.textField(
                mNationalityTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func setDelegate(){
        mAboutMeDelegate = (mDelegate as! AboutMeDelegate)
    }
    
    func setIcons(){
        mCountryTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mStateTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mCityTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mNationalityTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func addTargets(){
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        mNoBinaryButton.addTarget(self, action: #selector(SelectnoBinaryAction), for: .touchUpInside)
        mMaleButton.addTarget(self, action: #selector(SelectManAction), for: .touchUpInside)
        mWomanButton.addTarget(self, action: #selector(SelectWomanAction), for: .touchUpInside)
        mEditButton.addTarget(self, action: #selector(capturar), for: .touchUpInside)
    }
    
    func addTaps(){
        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(loadListCountry))
        tapCountry.delegate = self
        mCountryTextField.addGestureRecognizer(tapCountry)
        
        let tapStates = UITapGestureRecognizer(target: self, action: #selector(loadStates))
        tapStates.delegate = self
        mStateTextField.addGestureRecognizer(tapStates)
        
        let tapCity = UITapGestureRecognizer(target: self, action: #selector(loadCities))
        tapCity.delegate = self
        mCityTextField.addGestureRecognizer(tapCity)
        
        let tapNationality = UITapGestureRecognizer(target: self, action: #selector(loadNationality))
        tapNationality.delegate = self
        mNationalityTextField.addGestureRecognizer(tapNationality)
    }
    
    @objc func loadListCountry(textField: UITextField) {
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "México"),
                                    OptionSettingsModel(mIcon: "", mOption: "Argentina"),
                                    OptionSettingsModel(mIcon: "", mOption: "Colombia"),
                                    OptionSettingsModel(mIcon: "", mOption: "Brasil"),
                                    OptionSettingsModel(mIcon: "", mOption: "Uruguay")]
        mGenericList?.mTextField = mCountryTextField
        present(mGenericList!, animated: true, completion: nil)

    }
    
    @objc func loadStates(textField: UITextField){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "AguasCalientes"),
                                    OptionSettingsModel(mIcon: "", mOption: "Veracruz"),
                                    OptionSettingsModel(mIcon: "", mOption: "Tabasco"),
                                    OptionSettingsModel(mIcon: "", mOption: "Matamoros"),
                                    OptionSettingsModel(mIcon: "", mOption: "Yucatan")]
        mGenericList?.mTextField = mStateTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func loadCities(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Xalapa"),
                                    OptionSettingsModel(mIcon: "", mOption: "Cuernavaca"),
                                    OptionSettingsModel(mIcon: "", mOption: "Colima"),
                                    OptionSettingsModel(mIcon: "", mOption: "Tuxtepec"),
                                    OptionSettingsModel(mIcon: "", mOption: "Acapulco")]
        mGenericList?.mTextField = mCityTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    
    @objc func loadNationality(){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Mexicana"),
                                    OptionSettingsModel(mIcon: "", mOption: "Americana")]
        mGenericList?.mTextField = mNationalityTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @objc func SelectnoBinaryAction(){
        mGenre = "Binario"
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: true, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        changeSelectedGenre(mButton: mMaleButton, mView:  mArraysViews[1], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @objc func SelectManAction(){
        mGenre = "Hombre"
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mMaleButton, mView:  mArraysViews[1], mSelect: true, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @IBAction func mMarriedAction(_ sender: Any) {
        mMarried = "Casada"
        changeSelectedGenre(mButton: mMarriedButton, mView: mArraysViews[4], mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        
        changeSelectedGenre(mButton: mSingleButton, mView: mArraysViews[3], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @IBAction func mSingleAction(_ sender: Any) {
        mMarried = "Soltera"
        changeSelectedGenre(mButton: mMarriedButton, mView: mArraysViews[4], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        
        changeSelectedGenre(mButton: mSingleButton, mView: mArraysViews[3], mSelect: false, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
    }
    
    @objc func SelectWomanAction(){
        mGenre = "Mujer"
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mMaleButton, mView:  mArraysViews[1], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: true, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func addViews(){
        mArraysViews.append(mWomanView)
        mArraysViews.append(mMaleView)
        mArraysViews.append(mNoBinaryView)
        mArraysViews.append(mSingleView)
        mArraysViews.append(mMarriedView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setStyle(){
        mProfileImageView.roundedImage(mColor: UIColor(netHex: Colors.color_white))
        mContainerView.backgroundColor = UIColor(netHex: Colors.color_branding_blue)
        mContainerView.roundedView(mColor: UIColor.white)
        mEditImageView.setTintColor(mColor: UIColor.white)
    }
    
    @objc func capturar() {
        let alert = UIAlertController(
            title: "Aviso",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Tomar Foto"
        let action1 = UIAlertAction(title: titulo, style: .default) { [weak self] _ in
            self!.openCamera()
        }
        titulo = "Importar de Galeria"
        let action2 = UIAlertAction(title: titulo, style: .default) { [weak self] _ in
            self!.visualizar()
        }
      
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.systemBlue
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func guardarImagen(img: UIImage, nombre: String) {
        if let data = img.jpegData(compressionQuality: 0.6) {
            let filename = getDocumentsDirectory().appendingPathComponent(nombre)
            try! data.write(to: filename)
            print("guardada la imagen: \(filename)")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func leerImagen(nombre: String) -> UIImage? {
        let fileManager = FileManager.default
        let filename = getDocumentsDirectory().appendingPathComponent(nombre)
        if fileManager.fileExists(atPath: filename.path) {
            let image = UIImage(contentsOfFile: filename.path)
            mProfileImageView.image = image
           print("leida la imagen: \(nombre)")
            return image
        } else {
            return UIImage()
        }
    }
    
    func openCamera(){
        if imagePicker == nil {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker = UIImagePickerController()
                imagePicker!.delegate = self
                imagePicker!.sourceType = .camera
                imagePicker!.allowsEditing = false
                present(imagePicker!, animated: true, completion: nil)
            } else {
               print("Sin camara","No hay camara disponible")
            }
        }
    }
    
    func visualizar() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker = UIImagePickerController()
            imagePicker!.delegate = self
            imagePicker!.sourceType = .photoLibrary
            self.present(imagePicker!, animated: true, completion: nil)
        }else{
            print("Galeria", "No disponible")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker!.dismiss(animated: true, completion: nil)
        if let imagen = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            guardarImagen(img: imagen, nombre: nombreImagen)
            mProfileImageView.image = leerImagen(nombre: nombreImagen)
        }
        imagePicker = nil
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if(validatorManager.validate()){
            mAboutMeDelegate.saveAbout(mGenre: mGenre ,mLocation:mCityTextField.text! ,mState: mMarried,mNationality: mNationalityTextField.text!,mName: mNameTextField.text!, mLastName: mMiddleLastName.text!)
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func backAction(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeSelectedGenre(mButton: UIButton, mView : UIView, mSelect: Bool, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mView.backgroundColor = mColorBackground
        if(!mSelect){
            mView.layer.borderWidth = 1
            mView.layer.borderColor = mColorText.cgColor
        }else{
            mView.layer.borderWidth = 0
        }
    }
}
