//
//  MyCvViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit

protocol ProfileProtocol: NSObjectProtocol{
    func setData(mName : String)
}


class MyCvViewController: BaseViewController{
    
    @IBOutlet weak var mCertifiedBannerImageView: UIImageView!
    @IBOutlet weak var mGenreLabel: UILabel!
    @IBOutlet weak var mStateCivilLabel: UILabel!
    @IBOutlet weak var mLocationLabel: UILabel!
    @IBOutlet weak var mNationalityLabel: UILabel!
    @IBOutlet weak var mProfileLabel: UILabel!
    @IBOutlet weak var mSalaryLabel: UILabel!
    
    @IBOutlet weak var mCVButton: UIButton!
    @IBOutlet weak var mFileButton: UIButton!
    
    @IBOutlet weak var mListPresenceOnlineCarouselView: iCarousel!
    
    @IBOutlet weak var mLanguageCollectionView: UICollectionView!
    @IBOutlet weak var mExperienceTableView: UITableView!
    @IBOutlet weak var mTechnicalSkillCollectionView: UICollectionView!
    @IBOutlet weak var mSocialSkillCollectionView: UICollectionView!
    @IBOutlet weak var mExperienceAreaTableView: UITableView!
    @IBOutlet weak var mEducationTableView: UITableView!
    @IBOutlet weak var mCourseTableView: UITableView!
    @IBOutlet weak var mCertificateTableView: UITableView!
    
    @IBOutlet weak var mBannerCertifiedAccountView: UIView!
    @IBOutlet weak var mAboutView: UIView!
    @IBOutlet weak var mInformationPersonalView: UIView!
    @IBOutlet weak var mPresenceOnlineView: UIView!
    @IBOutlet weak var mExperienceView: UIView!
    @IBOutlet weak var mTechnicalSkillView: UIView!
    @IBOutlet weak var mSocialSkillView: UIView!
    @IBOutlet weak var mExperienceAreaView: UIView!
    @IBOutlet weak var mEducationView: UIView!
    @IBOutlet weak var mCourseView: UIView!
    @IBOutlet weak var mCertificationView: UIView!
    @IBOutlet weak var mLanguageView: UIView!

    @IBOutlet weak var mAboutEmptyView: UIView!
    @IBOutlet weak var mProfessionalInformationEmptyView: UIView!
    @IBOutlet weak var mPresenceOnlineEmptyView: UIView!
    @IBOutlet weak var mmExperienceEmptyView: UIView!
    @IBOutlet weak var mmTechnicalSkillEmptyView: UIView!
    @IBOutlet weak var mSocialSkillEmptyView: UIView!
    @IBOutlet weak var mExperienceAreasEmptyView: UIView!
    @IBOutlet weak var mEducationEmptyView: UIView!
    @IBOutlet weak var mCourseEmptyView: UIView!
    @IBOutlet weak var mCertificateEmptyView: UIView!
    @IBOutlet weak var mLanguageEmptyView: UIView!

    @IBOutlet weak var mEditAboutButton: UIButton!
    @IBOutlet weak var mComPleteAboutButton: UIButton!

    @IBOutlet weak var mEditProfessionalInformationButton: UIButton!
    @IBOutlet weak var mCompleteProfessionalInformationButton: UIButton!

    @IBOutlet weak var mEditPresenceOnlineButton: UIButton!
    @IBOutlet weak var mCompletePresenceOnlineButton: UIButton!
    
    @IBOutlet weak var mEditExperienceButton: UIButton!
    @IBOutlet weak var mCompleteExperienceButton: UIButton!
    
    @IBOutlet weak var mEditTechnicalSkillButton: UIButton!
    @IBOutlet weak var mCompleteTechnicalSkillButton: UIButton!
    
    @IBOutlet weak var mEditSocialSkillButton: UIButton!
    @IBOutlet weak var mCompleteSocialSkillButton: UIButton!
    
    @IBOutlet weak var mEditSpecialAreasButton: UIButton!
    @IBOutlet weak var mCompleteSpecialAreasButton: UIButton!
    
    @IBOutlet weak var mEditEducationButton: UIButton!
    @IBOutlet weak var mCompleteEducationButton: UIButton!
    
    @IBOutlet weak var mEditCourseButton: UIButton!
    @IBOutlet weak var mCompleteCourseButton: UIButton!
    
    @IBOutlet weak var mEditCertificatesButton: UIButton!
    @IBOutlet weak var mCompleteCertificatesButton: UIButton!
    
    @IBOutlet weak var mEditLanguageButton: UIButton!
    @IBOutlet weak var mCompleteLanguageButton: UIButton!
    
    
    var mExperienceDataSource : GenericDataSource?
    var mEducationDataSource : GenericDataSource?
    var mCourseDataSource : GenericDataSource?
    var mCertificatesDataSource : GenericDataSource?
    var mPresenceOnlineDataSource : PresenceOnlineDataSource?
    var mTechnicalKnoledgeDataSource : TechnicalKnowledgeDataSource?
    var mSkillDataSource : TechnicalKnowledgeDataSource?
    var mLanguageDataSource : TechnicalKnowledgeDataSource?
    var mProfileProtocol : ProfileProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        setStyleViews()
        addTargets()
    }
    
    func createTableView(mTableView : UITableView, mListCollection : [String], mDataSource: GenericDataSource){
        mTableView.dataSource = mDataSource
        mTableView.delegate = mDataSource
        mTableView.reloadData()
        mDataSource.update(items: mListCollection)
        mTableView.reloadData()
    }
    
    func createCarousel(){
        mPresenceOnlineDataSource = PresenceOnlineDataSource(carrusel: mListPresenceOnlineCarouselView)
        mPresenceOnlineDataSource?.update(items: ["","","",""])
        mPresenceOnlineDataSource?.settings()
    }
    
    func setStyleViews(){
        mCertifiedBannerImageView.roundCorners([.topLeft, .topRight], radius: 8.0)
    }
    
    func hideShowView(mVisility : ViewVisibilityEnum, mView: UIView){
        ViewUtils.setVisibility(view: mView, visibility: mVisility)
    }
    
    func addTargets(){
        mComPleteAboutButton.addTarget(self, action: #selector(aboutMeAction), for: .touchUpInside)
        mCompleteProfessionalInformationButton.addTarget(self, action: #selector(professionalInformationAction), for: .touchUpInside)
        mCompletePresenceOnlineButton.addTarget(self, action: #selector(presenceOnlineAction), for: .touchUpInside)
        mCompleteExperienceButton.addTarget(self, action: #selector(experienceAction), for: .touchUpInside)
        mCompleteEducationButton.addTarget(self, action: #selector(educationAction), for: .touchUpInside)
        mCompleteCourseButton.addTarget(self, action: #selector(courseAction), for: .touchUpInside)
        mCompleteCertificatesButton.addTarget(self, action: #selector(certificateAction), for: .touchUpInside)
        mCompleteLanguageButton.addTarget(self, action: #selector(languageAction), for: .touchUpInside)
        mCompleteTechnicalSkillButton.addTarget(self, action: #selector(technicalSkilllAction), for: .touchUpInside)
        mCompleteSocialSkillButton.addTarget(self, action: #selector(socialSkillAction), for: .touchUpInside)
        mCompleteSpecialAreasButton.addTarget(self, action: #selector(specialAreasAction), for: .touchUpInside)
        
        
        mEditAboutButton.addTarget(self, action: #selector(aboutMeAction), for: .touchUpInside)
        mEditProfessionalInformationButton.addTarget(self, action: #selector(professionalInformationAction), for: .touchUpInside)
        mEditPresenceOnlineButton.addTarget(self, action: #selector(presenceOnlineAction), for: .touchUpInside)
        mEditExperienceButton.addTarget(self, action: #selector(experienceAction), for: .touchUpInside)
        mEditEducationButton.addTarget(self, action: #selector(educationAction), for: .touchUpInside)
        mEditCourseButton.addTarget(self, action: #selector(courseAction), for: .touchUpInside)
        mEditCertificatesButton.addTarget(self, action: #selector(certificateAction), for: .touchUpInside)
        mEditLanguageButton.addTarget(self, action: #selector(languageAction), for: .touchUpInside)
        mEditTechnicalSkillButton.addTarget(self, action: #selector(technicalSkilllAction), for: .touchUpInside)
        mEditSocialSkillButton.addTarget(self, action: #selector(socialSkillAction), for: .touchUpInside)
        mEditSpecialAreasButton.addTarget(self, action: #selector(specialAreasAction), for: .touchUpInside)
    }
    
    @IBAction func mVerifyAccountAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "VerifyAccount", mViewController: VerifyAccountViewController.self, mNameViewController: "VerifyAccountViewController",mDelegate: self)
    }
    
    //MARK:- Pending Hide View Verify Account
    @IBAction func mHideAction(_ sender: Any) {
    
    }

    @objc func aboutMeAction(){
        pushViewController(mNameStoryBoard: "AboutMe", mViewController: AboutMeViewController.self, mNameViewController: "AboutMeViewController", mDelegate: self)
    }
   
    @objc func professionalInformationAction(){
        pushViewController(mNameStoryBoard: "PersonalInformation", mViewController: PersonalInformationViewController.self, mNameViewController: "PersonalInformationViewController",mDelegate: self)
    }
    
    @objc func presenceOnlineAction(){
        pushViewController(mNameStoryBoard: "PresenceOnline", mViewController: PresenceOnlineViewController.self, mNameViewController: "PresenceOnlineViewController",mDelegate: self)
    }
    
    @objc func experienceAction(){
        pushViewController(mNameStoryBoard: "ExperienceWork", mViewController: ExperienceWorkViewController.self, mNameViewController: "ExperienceWorkViewController",mDelegate: self)
    }
    
    @objc func technicalSkilllAction(){
        pushViewController(mNameStoryBoard: "TechnicalKwnoledge", mViewController: TechnicalKwnoledgeViewController.self, mNameViewController: "TechnicalKwnoledgeViewController",mDelegate: self)
    }
    
    @objc func socialSkillAction(){
        pushViewController(mNameStoryBoard: "SkillSocial", mViewController:SkillSocialViewController.self, mNameViewController: "SkillSocialViewController",mDelegate: self)
    }
    
    @objc func specialAreasAction(){
        pushViewController(mNameStoryBoard: "ExperienceArea", mViewController:ExperienceAreaViewController.self, mNameViewController: "ExperienceAreaViewController",mDelegate: self)
    }
    
    @objc func educationAction(){
        pushViewController(mNameStoryBoard: "Education", mViewController: EducationViewController.self, mNameViewController: "EducationViewController",mDelegate: self)
    }
    
    @objc func courseAction(){
        pushViewController(mNameStoryBoard: "Course", mViewController: CourseViewController.self, mNameViewController: "CourseViewController",mDelegate: self)
    }
    
    @objc func certificateAction(){
        pushViewController(mNameStoryBoard: "Certificate", mViewController: CertificateViewController.self, mNameViewController: "CertificateViewController",mDelegate: self)
    }
    
    @objc func languageAction(){
        pushViewController(mNameStoryBoard: "Language", mViewController: LanguageViewController.self, mNameViewController: "LanguageViewController",mDelegate: self)
    }
}

// MARK:-Add Protocols After edit or Add Elements to VC
extension MyCvViewController: AboutMeDelegate, ExperienceWorkDelegate, PresenceOnlineDelegate, EducationDelegate, CertificateDelegate, PersonalInformationDelegate, LanguageDelegate, CourseDelegate, TechnicalKwnoledgeVCDelegate, TechnicalKnowledgeDelegate,SkillSocialVCDelegate {
   
    func saveAbout(mGenre : String , mLocation : String, mState : String, mNationality: String, mName : String, mLastName : String) {
        hideShowView(mVisility: .INVISIBLE, mView: mAboutEmptyView)
        mGenreLabel.text = mGenre
        mLocationLabel.text = mLocation
        mStateCivilLabel.text = mState
        mNationalityLabel.text = mNationality
        mProfileProtocol.setData(mName: mName + " " + mLastName)
    }
    
    func saveExperience() {
        mExperienceDataSource = GenericDataSource(tableView: mExperienceTableView, mViewController: self)
        createTableView(mTableView: mExperienceTableView, mListCollection: ["",""], mDataSource: mExperienceDataSource!)
        hideShowView(mVisility: .INVISIBLE, mView: mmExperienceEmptyView)
    }
    
    func savePresence() {
        createCarousel()
        hideShowView(mVisility: .INVISIBLE, mView: mPresenceOnlineEmptyView)
    }
    
    func saveEducation() {
        mEducationDataSource = GenericDataSource(tableView: mEducationTableView, mViewController: self)
        createTableView(mTableView: mEducationTableView, mListCollection: [""], mDataSource: mEducationDataSource!)
        hideShowView(mVisility: .INVISIBLE, mView: mEducationEmptyView)
    }
    
    func saveCertificate() {
        mCertificatesDataSource = GenericDataSource(tableView: mCertificateTableView, mViewController: self)
        createTableView(mTableView: mCertificateTableView, mListCollection: [""], mDataSource: mCertificatesDataSource!)
        hideShowView(mVisility: .INVISIBLE, mView: mCertificateEmptyView)
    }
    
    func savePersonal(mProfile : String , mSalary : String) {
        mProfileLabel.text = mProfile
        mSalaryLabel.text = "$" + mSalary
        hideShowView(mVisility: .INVISIBLE, mView: mProfessionalInformationEmptyView)
    }
    
    func saveLanguage(mListTechnical : [TechnicalModel]) {
        hideShowView(mVisility: .INVISIBLE, mView: mLanguageEmptyView)
        mLanguageDataSource = TechnicalKnowledgeDataSource(mListTechnical: mListTechnical, mListPhotosCollectionView: mLanguageCollectionView, mDelegate: self)
    }
    
    func saveCourse() {
        mCourseDataSource = GenericDataSource(tableView: mCourseTableView, mViewController: self)
        createTableView(mTableView: mCourseTableView, mListCollection: [""], mDataSource: mCourseDataSource!)
        hideShowView(mVisility: .INVISIBLE, mView: mCourseEmptyView)
    }
    
    func saveTechnicalKnowlegde(mListTechnical : [TechnicalModel]) {
        hideShowView(mVisility: .INVISIBLE, mView: mmTechnicalSkillEmptyView)
        mTechnicalKnoledgeDataSource = TechnicalKnowledgeDataSource(mListTechnical: mListTechnical, mListPhotosCollectionView: mTechnicalSkillCollectionView, mDelegate: self)
    }
    
    func saveSkillSocial(mListTechnical: [TechnicalModel]) {
        hideShowView(mVisility: .INVISIBLE, mView: mSocialSkillEmptyView)
        mSkillDataSource = TechnicalKnowledgeDataSource(mListTechnical: mListTechnical, mListPhotosCollectionView: mSocialSkillCollectionView, mDelegate: self)
    }
    
    func removeItem() {
        
    }

}
