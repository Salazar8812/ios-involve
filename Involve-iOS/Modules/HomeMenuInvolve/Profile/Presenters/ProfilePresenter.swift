//
//  ProfilePresenter.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 23/06/21.
//

import UIKit

class ProfilePresenter: BaseInvolvePresenter {
    
    func getDataCandidate(){
        AlertDialog.showOverlay()
        print(ApiDefinitions.WS_INFO_CANDIDATE)
        BaseRetrofitManager<LoginInvolveResponse>.init(requestUrl: ApiDefinitions.WS_INFO_CANDIDATE, delegate: self).requestGetWithTokenHeader()
    }

    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        switch requestUrl {
        case ApiDefinitions.WS_INFO_CANDIDATE:
            self.onSuccessGetInfoCandidate(mResponse: response as! LoginInvolveResponse)
            break
        default:
            break
        }
    }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
    
    func onSuccessGetInfoCandidate(mResponse : LoginInvolveResponse){
        AlertDialog.hideOverlay()
        print("Response Candidate: \(mResponse.toJSONString(prettyPrint: true) ?? "")")
    }
    
}
