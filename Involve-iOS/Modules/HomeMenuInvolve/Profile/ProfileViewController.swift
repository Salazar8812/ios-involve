//
//  ProfileViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit
import SlidingContainerViewController

class ProfileViewController: BaseViewController, SlidingContainerSliderViewDelegate, SlidingContainerViewControllerDelegate, ProfileProtocol  {

    @IBOutlet weak var mMainContainerView: UIView!
    @IBOutlet weak var mProfileImageView: UIImageView!
    @IBOutlet weak var mProgressView: UIView!
    @IBOutlet weak var mProgressLabel: UILabel!
    @IBOutlet weak var mNameLabel: UILabel!
    @IBOutlet weak var mVerifyImageView: UIImageView!
    
    var mProfilePresenter : ProfilePresenter!
    var mArrayControllers : [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        populateControllers()
        setDataSourceTabs()
        setRoundedImage()
        mNameLabel.text = "Carlos Salazar"
    }
    
    override func getPresenter() -> BasePresenter? {
        mProfilePresenter = ProfilePresenter()
        //mProfilePresenter.getDataCandidate()
        return mProfilePresenter
    }

    func setRoundedImage(){
        mProfileImageView.image = UIImage(named: "ic_profile_gray")
        mProfileImageView.roundedImage(mColor: UIColor(netHex: Colors.color_yellow))
        mProgressView.backgroundColor = UIColor(netHex: Colors.color_gray_less_profile)
        mVerifyImageView.setTintColor(mColor: UIColor(netHex: Colors.color_gray_less_profile))
        mProgressLabel.text = "50 %"
    }
    
    func setDataSourceTabs(){
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: ["Mi CV","Configuración"],mHeight: 44)
        slidingContainerViewController.delegate = self
        
        //slidingContainerViewController.setCurrentViewControllerAtIndex(1)
        mMainContainerView.addSubview(slidingContainerViewController.view)

    }
    
    func populateControllers(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)
        let mSettings = (storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController)
        
        let storyboardVC : UIStoryboard = UIStoryboard(name: "MyCv", bundle: nil)
        let mCv = (storyboardVC.instantiateViewController(withIdentifier: "MyCvViewController") as! MyCvViewController)
        mCv.mProfileProtocol = self
        mArrayControllers.append(mCv)
        mArrayControllers.append(mSettings)
        

    }
    func slidingContainerSliderViewDidPressed(_ slidingtContainerSliderView: SlidingContainerSliderView, atIndex: Int) {
        
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func setData(mName: String) {
        mNameLabel.text = mName
    }
   
}
