//
//  HomeViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit
import SlidingContainerViewController

class HomeViewController: BaseViewController,SlidingContainerViewControllerDelegate   {
    @IBOutlet weak var mMainContainerView: UIView!
    @IBOutlet weak var mTabSelectorProfile: UIView!
    @IBOutlet weak var mTabSelectorNotificationView: UIView!
    @IBOutlet weak var mTabSelectorStateView: UIView!
    @IBOutlet weak var mTabSelectorChatView: UIView!
    @IBOutlet weak var mTabSelectorBusquedaView: UIView!
    @IBOutlet weak var mIconProfileImageView: UIImageView!
    @IBOutlet weak var mNotificationImageView: UIImageView!
    @IBOutlet weak var mIconStateImageVIew: UIImageView!
    @IBOutlet weak var mIconChatImageView: UIImageView!
    @IBOutlet weak var mIconSearchImageView: UIImageView!
    
    var mArrayIconsTabs : [UIImageView] = []
    var mArrayTabsView : [UIView] = []
    var mArrayControllers : [UIViewController] = []
    var slidingContainerViewController : SlidingContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateViews()
        populateControllers()
        setDataSourceTabs()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func populateViews(){
        mArrayIconsTabs.append(mIconSearchImageView)
        mArrayIconsTabs.append(mIconChatImageView)
        mArrayIconsTabs.append(mIconStateImageVIew)
        mArrayIconsTabs.append(mNotificationImageView)
        mArrayIconsTabs.append(mIconProfileImageView)
        
        mArrayTabsView.append(mTabSelectorBusquedaView)
        mArrayTabsView.append(mTabSelectorChatView)
        mArrayTabsView.append(mTabSelectorStateView)
        mArrayTabsView.append(mTabSelectorNotificationView)
        mArrayTabsView.append(mTabSelectorProfile)
    }
    
    func setDataSourceTabs(){
        slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: [],mHeight: 0)
        slidingContainerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        slidingContainerViewController.delegate = self
        mMainContainerView.addSubview(slidingContainerViewController.view)

        for view in slidingContainerViewController.sliderView.subviews{
            view.isUserInteractionEnabled = false
        }
    
        selectTab(mPositionSelect: 0)
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
    }
    
    func populateControllers(){
        let storyboardVacancy : UIStoryboard = UIStoryboard(name: "Vacancy", bundle: nil)
        let mVacancy = (storyboardVacancy.instantiateViewController(withIdentifier: "VancancyViewController") as! VancancyViewController)
        
        let storyboardMessage : UIStoryboard = UIStoryboard(name: "Message", bundle: nil)
        let mMessage = (storyboardMessage.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController)
        
        let storyboardState : UIStoryboard = UIStoryboard(name: "StateAdversiting", bundle: nil)
        let mState = (storyboardState.instantiateViewController(withIdentifier: "StateViewController") as! StateViewController)
        
        let storyboardNotifications : UIStoryboard = UIStoryboard(name: "Notifications", bundle: nil)
        let mNotifications = (storyboardNotifications.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController)
       
        let storyboardProfile : UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        let mProfile = (storyboardProfile.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController)
        
        mArrayControllers.append(mVacancy)
        mArrayControllers.append(mMessage)
        mArrayControllers.append(mState)
        mArrayControllers.append(mNotifications)
        mArrayControllers.append(mProfile)

    }
    
    @IBAction func mActionTabProfile(_ sender: Any) {
        selectTab(mPositionSelect: 4)
        slidingContainerViewController.setCurrentViewControllerAtIndex(4)
    }
    
    @IBAction func mActionTabNotification(_ sender: Any) {
        selectTab(mPositionSelect: 3)
        slidingContainerViewController.setCurrentViewControllerAtIndex(3)
    }
    
    @IBAction func mActionTabState(_ sender: Any) {
        selectTab(mPositionSelect: 2)
        slidingContainerViewController.setCurrentViewControllerAtIndex(2)
    }
    
    @IBAction func mActionTabChat(_ sender: Any) {
        selectTab(mPositionSelect: 1)
        slidingContainerViewController.setCurrentViewControllerAtIndex(1)
    }
    
    @IBAction func mActionTabSearch(_ sender: Any) {
        selectTab(mPositionSelect: 0)
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
    }
    
    func selectTab(mPositionSelect : Int){
        for mIndex in 0...mArrayIconsTabs.count - 1 {
            if(mPositionSelect == mIndex){
                mArrayIconsTabs[mIndex].setTintColor(mColor: UIColor(netHex: Colors.color_grey_select))
                mArrayTabsView[mIndex].backgroundColor = UIColor(netHex: Colors.color_branding_blue)
            }else{
                mArrayIconsTabs[mIndex].setTintColor(mColor: UIColor(netHex: Colors.color_gray_less_profile))
                mArrayTabsView[mIndex].backgroundColor = UIColor(netHex: Colors.color_white)
            }
        }
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        selectTab(mPositionSelect: atIndex)
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
}
