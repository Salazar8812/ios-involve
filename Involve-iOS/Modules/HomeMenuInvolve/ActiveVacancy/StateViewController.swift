//
//  StateViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/05/21.
//

import UIKit
import SlidingContainerViewController


class StateViewController: BaseViewController, SlidingContainerViewControllerDelegate {
    @IBOutlet weak var mActiveLabel: UILabel!
    @IBOutlet weak var mSelectorActiveView: UIView!
    
    @IBOutlet weak var mMainContainerView: UIView!
    @IBOutlet weak var mSelectorExpiredActive: UIView!
    @IBOutlet weak var mExpiredLabel: UILabel!
    @IBOutlet weak var mActiveButton: UIButton!
    @IBOutlet weak var mExpiredButton: UIButton!
    var mArrayControllers : [UIViewController] = []
    var mArrayLabels : [UILabel] = []
    var mArrayTabSelector : [UIView] = []
    var slidingContainerViewController : SlidingContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTargets()
        populateControllers()
        populateViews()
        setDataSourceTabs()
    }
    
    func populateViews(){
        mArrayLabels.append(mActiveLabel)
        mArrayLabels.append(mExpiredLabel)
        
        mArrayTabSelector.append(mSelectorActiveView)
        mArrayTabSelector.append(mSelectorExpiredActive)
    }
    
    func addTargets(){
        mActiveButton.addTarget(self, action: #selector(loadActiveTab), for: .touchUpInside)
        mExpiredButton.addTarget(self, action: #selector(loadExpiredTab), for: .touchUpInside)
    }
    
    @objc func loadActiveTab(){
        selectTab(mPosition: 0)
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
    }
    
    @objc func loadExpiredTab(){
        selectTab(mPosition: 1)
        slidingContainerViewController.setCurrentViewControllerAtIndex(1)
    }
    
    func populateControllers(){
        let storyboardVacancy : UIStoryboard = UIStoryboard(name: "Active", bundle: nil)
        let mVacancy = (storyboardVacancy.instantiateViewController(withIdentifier: "ActiveViewController") as! ActiveViewController)
        
        let storyboardFavs : UIStoryboard = UIStoryboard(name: "Expired", bundle: nil)
        let mFavs = (storyboardFavs.instantiateViewController(withIdentifier: "ExpiredViewController") as! ExpiredViewController)
        
        mArrayControllers.append(mVacancy)
        mArrayControllers.append(mFavs)
    }
    
    func setDataSourceTabs(){
        slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: [],mHeight: 0)
        slidingContainerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        slidingContainerViewController.delegate = self
        mMainContainerView.addSubview(slidingContainerViewController.view)
        selectTab(mPosition: 0)
    }

    func selectTab(mPosition : Int){
        for mIndex in 0...mArrayLabels.count - 1 {
            if(mIndex == mPosition){
                mArrayLabels[mIndex].textColor = UIColor(netHex: Colors.color_grey_select)
                mArrayTabSelector[mIndex].backgroundColor = UIColor(netHex: Colors.color_final_strong_green)
            }else{
                mArrayLabels[mIndex].textColor = UIColor(netHex: Colors.color_gray_less_profile)
                mArrayTabSelector[mIndex].backgroundColor = UIColor(netHex: Colors.color_white)
            }
        }
        
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        selectTab(mPosition: atIndex)
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
}
