//
//  ItemExpiredVacancyTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit

class ItemExpiredVacancyTableViewCell: UITableViewCell {
    @IBOutlet weak var mVacancyExpired: UILabel!
    @IBOutlet weak var mDateExpiredVacancy: UILabel!
    @IBOutlet weak var mTitleExpiredVacancy: UILabel!
    
    @IBOutlet weak var mOptionButton: UIButton!
    @IBOutlet weak var mChatButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
