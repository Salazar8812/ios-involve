//
//  ExpiredVacancyDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit
protocol ExpiredVacancyDelegate : NSObjectProtocol {
    func getVacancySelected()
}

class ExpiredVacancyDataSource:  NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [VacancyActiveModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemExpiredVacancyTableViewCell"
    var mViewController:  UIViewController?
    var mExpiredVacancyDelegate : ExpiredVacancyDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mExpiredVacancyDelegate : ExpiredVacancyDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.separatorStyle = .none
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mExpiredVacancyDelegate = mExpiredVacancyDelegate
    }
    
    func update(items: [VacancyActiveModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5//self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemExpiredVacancyTableViewCell
        
        return cell
    }
    
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // self.mSettingsDelegate.selectOption(mOption: "")
    }
    

}
