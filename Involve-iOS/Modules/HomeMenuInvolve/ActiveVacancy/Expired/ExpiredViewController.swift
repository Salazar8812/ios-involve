//
//  ExpiredViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit

class ExpiredViewController: BaseViewController, ExpiredVacancyDelegate {
    
    @IBOutlet weak var mExpiredVacancyTableView: UITableView!
    var mExpiredVacancyDataSource : ExpiredVacancyDataSource!
    var mListActiveVacancy : [VacancyActiveModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createTableView()
    }
    
    func createTableView(){
        mExpiredVacancyDataSource = ExpiredVacancyDataSource(tableView: mExpiredVacancyTableView, mViewController: self, mExpiredVacancyDelegate: self)
        mExpiredVacancyTableView.dataSource = mExpiredVacancyDataSource
        mExpiredVacancyTableView.delegate = mExpiredVacancyDataSource
        mExpiredVacancyTableView.reloadData()
        mExpiredVacancyDataSource?.update(items: mListActiveVacancy)
    }

    func getVacancySelected() {
        
    }
    
}
