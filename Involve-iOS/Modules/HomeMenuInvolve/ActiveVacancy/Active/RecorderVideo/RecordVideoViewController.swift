//
//  RecordVideoViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/06/21.
//

import UIKit

class RecordVideoViewController: BaseViewController, QuestionListDelegate {
    @IBOutlet weak var mBannerView: UIView!
    @IBOutlet weak var mRecButton: UIButton!
    @IBOutlet weak var mBackButton: UIButton!
    @IBOutlet weak var mFadeView: UIView!
    @IBOutlet weak var mCounterLabel: UILabel!
    @IBOutlet weak var mHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mContainerQuestions: UIView!
    @IBOutlet weak var mQuestionCarouselView: iCarousel!
    @IBOutlet weak var mNextQuestionButton: UIButton!
    @IBOutlet weak var mResetRecorderButton: UIButton!
    @IBOutlet weak var mTemporizerRecorderLabel: UILabel!
    @IBOutlet weak var mTotalQuestionsLabel: UILabel!
    var mTimer = Timer()
    var mCounter = 0
    
    var mTimerRecorder = Timer()
    var mCounterRecorder = 0
    
    var mQuestionListDataSource : QuestionsListCarouselDataSource!
    var mListQuestions : [String] = []
    var mCounterQuestions : Int = 0
    var mMaxTimeCounter : Int = 61
    
    @IBOutlet weak var mPlayImageView: UIImageView!
    
    @IBOutlet weak var mEnviarButton: UIButton!
    @IBOutlet weak var mRecordAgainButton: UIButton!
    @IBOutlet weak var mContainerSendVideoView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mCounterLabel.text = "3"
        mBannerView.roundCorners([.topLeft, .topRight], radius: 25)
        mContainerQuestions.roundCorners([.topLeft, .topRight], radius: 25)
        mContainerQuestions.isHidden = true
        mContainerSendVideoView.isHidden = true
        mPlayImageView.isHidden = true
        mHeightConstraint.constant = 100
        mBannerView.setNeedsLayout()
        addTarget()
        populateQuestions()
        createCarousel()
    }
    
    func createCarousel(){
        mQuestionListDataSource = QuestionsListCarouselDataSource(carrusel: mQuestionCarouselView, mQuestionListDelegate: self)
        mQuestionListDataSource?.update(items: mListQuestions)
        mQuestionListDataSource?.settings()
        mTotalQuestionsLabel.text = "1 de " + String(mListQuestions.count)
    }
    
    func populateQuestions(){
        mListQuestions = ["¿Por qué estás buscando un cambio de trabajo ahora?","¿Por qué te consideras un buen candidato para nuestro equipo?","¿Qué sabes de nuestra empresa?","¿Qué te gusta hacer en tu tiempo libre?"]
    }
    
    func addTarget(){
        mRecButton.addTarget(self, action: #selector(recordAction), for: .touchUpInside)
        mBackButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        mNextQuestionButton.addTarget(self, action: #selector(nextQuestionAction), for: .touchUpInside)
        mResetRecorderButton.addTarget(self, action: #selector(restartRecorderAction), for: .touchUpInside)
        mEnviarButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        mRecordAgainButton.addTarget(self, action: #selector(recordAgain), for: .touchUpInside)
    }
    
    func delayLaunchIntroductionView(){
        mCounter = 0
        mCounterLabel.text = "3"
        mTimer.invalidate()
        mTimer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(actionDelay), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelay() {
        mCounter += 1
        let mRegresive = 3 - mCounter
        if(mCounter == 5){
            mFadeView.isHidden = true
            mTimer.invalidate()
        }else{
            if(mCounter <= 3){
                mCounterLabel.text = String(mRegresive)
            }else{
                mCounterLabel.sizeToFit()
                mHeightConstraint.constant = 160
                mBannerView.setNeedsLayout()
                loadQuestions()
            }
        }
    }
    
    func beginCounterRecorder(){
        mCounterRecorder = 0
        mTimerRecorder.invalidate()
        mTimerRecorder = Timer.scheduledTimer(timeInterval: 1.2, target: self, selector: #selector(actionRecorder), userInfo: nil, repeats: true)

    }
    
    @objc func actionRecorder(){
        mCounterRecorder += 1
        mMaxTimeCounter -= 1
        if(mCounterRecorder <= 61){
            print(mMaxTimeCounter)
            mTemporizerRecorderLabel.text = "00:" + String(mMaxTimeCounter)
        }else{
            mTimerRecorder.invalidate()
        }
    }
 
    @objc func nextQuestionAction(){
        mCounterQuestions += 1
        if( mCounterQuestions <= mListQuestions.count - 1){
            mQuestionCarouselView.scrollToItem(at: mCounterQuestions , animated: true)
        }
    }
    
    @objc func restartRecorderAction(){
        mTimerRecorder.invalidate()
        mContainerQuestions.isHidden = true
        mMaxTimeCounter = 61
        mCounterRecorder = 0
    }
    
    @objc func finishRecord(){
        mTimerRecorder.invalidate()
        mResetRecorderButton.isHidden = true
        mContainerQuestions.isHidden = true
        mFadeView.isHidden = false
        mCounterLabel.isHidden = true
        mPlayImageView.isHidden = false
        mHeightConstraint.constant = 100
        mContainerSendVideoView.isHidden = false
        mBannerView.setNeedsLayout()
    }
    
    @objc func recordAction(){
        mFadeView.isHidden = false
        mCounterQuestions = 0
        mQuestionCarouselView.scrollToItem(at: mCounterQuestions , animated: true)
        delayLaunchIntroductionView()
        beginCounterRecorder()
    }
    
    @objc func backAction(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func sendAction(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func recordAgain(){
        mTimerRecorder.invalidate()
        mContainerQuestions.isHidden = true
        mPlayImageView.isHidden = true
        mCounterLabel.isHidden = false
        mCounterLabel.text = "3"
        mContainerSendVideoView.isHidden = true
        mMaxTimeCounter = 61
        mCounterRecorder = 0
    }
    
    func loadQuestions(){
        mContainerQuestions.isHidden = false
    }
    
    func getPosition(mPos: Int) {
        mCounterQuestions = mPos
        mTotalQuestionsLabel.text = String(mCounterQuestions + 1) + " de " + String(mListQuestions.count)
        if(mCounterQuestions == mListQuestions.count - 1){
            mNextQuestionButton.setTitle("Finzalizar Grabación", for: .normal)
            mNextQuestionButton.removeTarget(self, action: #selector(nextQuestionAction), for: .touchUpInside)
            mNextQuestionButton.addTarget(self, action: #selector(finishRecord), for: .touchUpInside)
        }
    }
}
