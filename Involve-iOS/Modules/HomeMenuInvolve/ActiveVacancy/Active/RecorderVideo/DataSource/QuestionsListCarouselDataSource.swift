//
//  QuestionsListCarouselDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/06/21.
//

import UIKit

protocol QuestionListDelegate : NSObjectProtocol {
    func getPosition(mPos : Int)
}

class QuestionsListCarouselDataSource: NSObject, iCarouselDataSource, iCarouselDelegate {
    var mICarousel: iCarousel
    var mItems : [String] = []
    var mQuestionListDelegate : QuestionListDelegate!
    
    init(carrusel : iCarousel, mQuestionListDelegate : QuestionListDelegate){
        mICarousel = carrusel
        self.mQuestionListDelegate = mQuestionListDelegate
    }
    
    func settings(){
        mICarousel.type = iCarouselType.linear
        mICarousel.delegate = self
        mICarousel.dataSource = self
    }
    
    func update(items : [String]){
        mItems = items
        mICarousel.reloadData()
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return mItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view = Bundle.main.loadNibNamed("ItemQuestionCollectionReusableView", owner: self, options: nil)![0] as! ItemQuestionCollectionReusableView
        view.mQuestionLabel.text = mItems[index]
        return view
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
       print(Int(carousel.currentItemIndex))
        mQuestionListDelegate?.getPosition(mPos: Int(carousel.currentItemIndex))
    }
    
}
