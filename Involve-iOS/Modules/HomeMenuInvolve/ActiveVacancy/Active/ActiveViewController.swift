//
//  ActiveViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit

class ActiveViewController: BaseViewController, ActiveVacancyDelegate {
    @IBOutlet weak var mActiveVacancyTableView: UITableView!
    var mActiveVacancyDataSource : ActiveVacancyDataSource!
    var mListActiveVacancy : [VacancyActiveModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateActiveVacancy()
        createTableView()
    }
    
    func createTableView(){
        mActiveVacancyDataSource = ActiveVacancyDataSource(tableView: mActiveVacancyTableView, mViewController: self, mActiveVacancyDelegate: self)
        mActiveVacancyTableView.dataSource = mActiveVacancyDataSource
        mActiveVacancyTableView.delegate = mActiveVacancyDataSource
        mActiveVacancyTableView.reloadData()
        mActiveVacancyDataSource?.update(items: mListActiveVacancy)
    }
    
    func populateActiveVacancy(){
        mListActiveVacancy.append(VacancyActiveModel(mTitleVacancy: "",mDatePostulate: "",mStepsPass: [true, false, false]))
        mListActiveVacancy.append(VacancyActiveModel(mTitleVacancy: "",mDatePostulate: "",mStepsPass: [false, false, false]))
        mListActiveVacancy.append(VacancyActiveModel(mTitleVacancy: "",mDatePostulate: "",mStepsPass: [true, true, false]))
        mListActiveVacancy.append(VacancyActiveModel(mTitleVacancy: "",mDatePostulate: "",mStepsPass: [true, true, true]))
    }
    
    func getVacancySelected() {
        
    }
    
    func loadOptions(mPassSteps : UIButton) {
        switch mPassSteps.titleLabel?.text {
        case "Ir al video entrevista":
            pushViewControllerExtra(mNameStoryBoard: "MainStepsIntroductionView", mViewController: MainStepsIntroductionViewViewController.self, mNameViewController: "MainStepsIntroductionViewViewController",mDelegate: self, vc: MainStepsIntroductionViewViewController(), mExtras: ["VideoInterview"])
            break
            
        case "Ir al video presentación":
            pushViewControllerExtra(mNameStoryBoard: "MainStepsIntroductionView", mViewController: MainStepsIntroductionViewViewController.self, mNameViewController: "MainStepsIntroductionViewViewController",mDelegate: self, vc: MainStepsIntroductionViewViewController(), mExtras: ["VideoPresentacion"])
            break
            
        case "Ir al cuestionario":
            pushViewControllerExtra(mNameStoryBoard: "Questions", mViewController: QuestionsViewController.self, mNameViewController: "QuestionsViewController",mDelegate: self, vc: QuestionsViewController(), mExtras:[""])
            break
        default:
            break
        }
    }

    func watchVacancy() {
        pushViewController(mNameStoryBoard: "VacancyDetail", mViewController: VacancyDetailViewController.self, mNameViewController: "VacancyDetailViewController",mDelegate: self)
    }
}
