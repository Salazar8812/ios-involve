//
//  QuestionSelectDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/06/21.
//

import UIKit

protocol QuestionSelectDelegate : NSObjectProtocol {
    func getQuestionSelect()
}

class QuestionSelectDataSource:NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [String] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemQuestionSelectTableViewCell"
    var mViewController:  UIViewController?
    var mQuestionSelectDelegate : QuestionSelectDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mQuestionSelectDelegate : QuestionSelectDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.separatorStyle = .none
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mQuestionSelectDelegate = mQuestionSelectDelegate
    }
    
    func update(items: [String]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemQuestionSelectTableViewCell
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // self.mSettingsDelegate.selectOption(mOption: "")
    }

}
