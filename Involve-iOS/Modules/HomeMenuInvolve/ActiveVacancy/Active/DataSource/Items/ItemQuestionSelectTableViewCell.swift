//
//  ItemQuestionSelectTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/06/21.
//

import UIKit

class ItemQuestionSelectTableViewCell: UITableViewCell {

    @IBOutlet weak var mQuestionLabel: UILabel!
    @IBOutlet weak var mCircleImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)

         if selected {
            contentView.backgroundColor = UIColor(netHex: Colors.color_final_strong_green)
            mQuestionLabel.textColor = UIColor.white
            mCircleImageView.setTintColor(mColor: UIColor(netHex:Colors.color_white))
         } else {
             contentView.backgroundColor = UIColor(netHex: Colors.color_white)
            mQuestionLabel.textColor = UIColor(netHex: Colors.color_grey_select)
            mCircleImageView.setTintColor(mColor: UIColor(netHex:Colors.color_gray_less_profile))

         }
     }
}
