//
//  ItemActiveVacancyTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit

class ItemActiveVacancyTableViewCell: UITableViewCell {

    @IBOutlet weak var mChatButton: UIButton!
    @IBOutlet weak var mOptionButton: UIButton!
    @IBOutlet weak var mGoQuestionsButton: UIButton!
    @IBOutlet weak var mWatchVacancyButton: UIButton!
    @IBOutlet weak var mVideoInterviewImageView: UIImageView!
    @IBOutlet weak var mViewPresentationImageView: UIImageView!
    @IBOutlet weak var mQuestionsImageView: UIImageView!
    @IBOutlet weak var mTitleVacancyLabel: UILabel!
    @IBOutlet weak var mTopHeaderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
