//
//  ActiveVacancyDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit
protocol ActiveVacancyDelegate: NSObjectProtocol {
    func getVacancySelected ()
    func loadOptions(mPassSteps : UIButton)
    func watchVacancy()
}

class ActiveVacancyDataSource:  NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [VacancyActiveModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemActiveVacancyTableViewCell"
    var mViewController:  UIViewController?
    var mActiveVacancyDelegate : ActiveVacancyDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mActiveVacancyDelegate : ActiveVacancyDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.separatorStyle = .none
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mActiveVacancyDelegate = mActiveVacancyDelegate
    }
    
    func update(items: [VacancyActiveModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 212
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemActiveVacancyTableViewCell
        cell.mTopHeaderView.roundCorners([.topLeft, .topRight], radius: 8)
        cell.mGoQuestionsButton.tag = indexPath.row
        cell.mWatchVacancyButton.tag = indexPath.row
        cell.mGoQuestionsButton.setTitle("Ir al cuestionario", for: .normal)
        cell.mGoQuestionsButton.addTarget(self, action: #selector(optionSelect), for: .touchUpInside)
        cell.mWatchVacancyButton.addTarget(self, action: #selector(watchVacancy), for: .touchUpInside)

        for mIndex in 0...item.mStepsPass!.count - 1{
           
            switch mIndex {
            case 0:
                if(item.mStepsPass![mIndex]){
                    cell.mQuestionsImageView.setTintColor(mColor: UIColor(netHex: Colors.color_final_strong_green))
                    cell.mGoQuestionsButton.setTitle("Ir al video presentación", for: .normal)
                }else{
                    cell.mQuestionsImageView.setTintColor(mColor: UIColor(netHex: Colors.color_gray_less_profile))
                }
                break
            case 1:
                if(item.mStepsPass![mIndex]){
                    cell.mViewPresentationImageView.setTintColor(mColor: UIColor(netHex: Colors.color_final_strong_green))
                    cell.mGoQuestionsButton.setTitle("Ir al video entrevista", for: .normal)
                }else{
                    cell.mViewPresentationImageView.setTintColor(mColor: UIColor(netHex: Colors.color_gray_less_profile))
                }
                break
                
            case 2:
                if(item.mStepsPass![mIndex]){
                    cell.mVideoInterviewImageView.setTintColor(mColor: UIColor(netHex: Colors.color_final_strong_green))
                    cell.mGoQuestionsButton.setTitle("", for: .normal)
                }else{
                    cell.mVideoInterviewImageView.setTintColor(mColor: UIColor(netHex: Colors.color_gray_less_profile))
                }
                break
                
            default:
                break
            }
        }
        return cell
    }
    
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
        self.mActiveVacancyDelegate.loadOptions(mPassSteps: sender)
    }
    
    @objc func watchVacancy(){
        self.mActiveVacancyDelegate.watchVacancy()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // self.mSettingsDelegate.selectOption(mOption: "")
    }
    

}
