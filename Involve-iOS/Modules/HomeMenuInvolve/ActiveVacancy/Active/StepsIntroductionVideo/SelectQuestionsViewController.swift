//
//  SelectQuestionsViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 08/06/21.
//

import UIKit

class SelectQuestionsViewController: BaseViewController, QuestionSelectDelegate {
    
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mQuestionsTableView: UITableView!
    var mQuestionSelectDataSource : QuestionSelectDataSource!
    var mListQuestions : [String] = []
 
    override func viewDidLoad() {
        super.viewDidLoad()
        mTitleLabel.text = "Prepara tu\npresentación"
        createTableView()
    }
    
    func createTableView(){
        mQuestionSelectDataSource = QuestionSelectDataSource(tableView: mQuestionsTableView, mViewController: self, mQuestionSelectDelegate: self)
        mQuestionsTableView.dataSource = mQuestionSelectDataSource
        mQuestionsTableView.delegate = mQuestionSelectDataSource
        mQuestionsTableView.reloadData()
        mQuestionSelectDataSource?.update(items: mListQuestions)
    }
    
    func getQuestionSelect() {
        
    }

}
