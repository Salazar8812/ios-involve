//
//  MainStepsIntroductionViewViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 04/06/21.
//

import UIKit
import SlidingContainerViewController


class MainStepsIntroductionViewViewController: BaseViewController,SlidingContainerSliderViewDelegate, SlidingContainerViewControllerDelegate {
       
    @IBOutlet weak var mMainContainerView: UIView!
    var mArrayControllers : [UIViewController] = []
    @IBOutlet weak var mPageControl: UIPageControl!
    var slidingContainerViewController : SlidingContainerViewController!
    var mCont : Int = 0
    
    @IBOutlet weak var mNextButton: UIButton!
    var mCounter = 0
    var mTimer = Timer()
    var mIsInterview : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        detectTypeVideo()
        setDataSourceTabs()
        delayLaunchIntroductionView()
        mMainContainerView.isUserInteractionEnabled = false
    }
    
    func detectTypeVideo(){
        if(mExtras[0] == "VideoPresentacion"){
            populateControllersPresentation()
            mIsInterview = false
        }else{
            mIsInterview = true
            populateControllersInterview()
        }
    }
    
    func setDataSourceTabs(){
        slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: [],mHeight: 0)
        slidingContainerViewController.delegate = self
        mMainContainerView.addSubview(slidingContainerViewController.view)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if(mCont < mArrayControllers.count - 1){
            if(mCont == mArrayControllers.count - 2){
                if(mIsInterview){
                    mMainContainerView.isUserInteractionEnabled = true
                    mNextButton.setTitle("Grabar video", for: .normal)
                }else{
                    mMainContainerView.isUserInteractionEnabled = false
                    mNextButton.setTitle("Iniciar", for: .normal)
                }
        }
            mCont += 1
            slidingContainerViewController.setCurrentViewControllerAtIndex(mCont)
        }else{
            presentViewController(mNameStoryBoard: "RecordVideo", mViewController: RecordVideoViewController.self, mNameViewController: "RecordVideoViewController")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func delayLaunchIntroductionView(){
        mTimer.invalidate()
        mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelay), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelay() {
        mCounter += 1
        if(mCounter == 1){
            mTimer.invalidate()
            slidingContainerViewController.setCurrentViewControllerAtIndex(mArrayControllers.count - 1)
            slidingContainerViewController.setCurrentViewControllerAtIndex(0)
        }
    }
    
    func populateControllersPresentation(){
        let storyboard : UIStoryboard = UIStoryboard(name: "IntroductionVideoSteps", bundle: nil)
        
        let step1 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step1.mComponents = ["Paso 1","Busca un sitio tranquilo y callado, además asegúrate de tener una red estable","ic_introduction_video"]
        
        let step2 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step2.mComponents = ["Paso 2","Arréglate de forma adecuada para tu entrevista, no olvides que es tu primera impresión","ic_interview_banner"]

        
        let step3 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step3.mComponents = ["Paso 3","Mantén una distancia entre 30 y 40 cm con la cámara y mira hacia ella siempre","ic_distance_video"]
        
        mArrayControllers.append(step1)
        mArrayControllers.append(step2)
        mArrayControllers.append(step3)
        
        mPageControl.numberOfPages = mArrayControllers.count
    }
    
    func populateControllersInterview(){
        let storyboard : UIStoryboard = UIStoryboard(name: "IntroductionVideoSteps", bundle: nil)
        let storyboardSelectQuestion : UIStoryboard = UIStoryboard(name: "SelectQuestions", bundle: nil)

        
        let step1 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step1.mComponents = ["Paso 1","Busca un sitio tranquilo y callado, además asegúrate de tener una red estable","ic_introduction_video"]
        
        let step2 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step2.mComponents = ["Paso 2","Arréglate de forma adecuada para tu entrevista, no olvides que es tu primera impresión","ic_interview_banner"]

        
        let step3 = (storyboard.instantiateViewController(withIdentifier: "IntroductionVideoStepsViewController") as! IntroductionVideoStepsViewController)
        step3.mComponents = ["Paso 3","Mantén una distancia entre 30 y 40 cm con la cámara y mira hacia ella siempre","ic_distance_video"]
        
        
        let step4 = (storyboardSelectQuestion.instantiateViewController(withIdentifier: "SelectQuestionsViewController") as! SelectQuestionsViewController)
        
        mArrayControllers.append(step1)
        mArrayControllers.append(step2)
        mArrayControllers.append(step3)
        mArrayControllers.append(step4)
        
        mPageControl.numberOfPages = mArrayControllers.count
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func slidingContainerSliderViewDidPressed(_ slidingtContainerSliderView: SlidingContainerSliderView, atIndex: Int) {
        mPageControl.currentPage = atIndex
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        mPageControl.currentPage = atIndex
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }

}
