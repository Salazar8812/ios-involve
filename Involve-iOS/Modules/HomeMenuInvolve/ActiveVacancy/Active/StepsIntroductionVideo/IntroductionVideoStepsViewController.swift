//
//  IntroductionVideoStepsViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 04/06/21.
//

import UIKit

class IntroductionVideoStepsViewController: BaseViewController {
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mStepTitle: UILabel!
    @IBOutlet weak var mBannerImageView: UIImageView!
    var mComponents : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateInfo()
    }
    
    func populateInfo(){
        mStepTitle.text = mComponents[0]
        mDescriptionLabel.text = mComponents[1]
        mBannerImageView.image = UIImage(named: mComponents[2])
    }

}
