//
//  TermsViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 10/04/21.
//

import UIKit
import BonMot

class TermsViewController: BaseViewController, RegistryBasicDelegate {
    @IBOutlet weak var mCrearAccountButton: UIButton!
    @IBOutlet weak var mGetNewsSwitch: UISwitch!
    @IBOutlet weak var mPrivacSwitch: UISwitch!
    @IBOutlet weak var mTermsSwitch: UISwitch!
    
    @IBOutlet weak var mTermsConditionsButton: UIButton!
    @IBOutlet weak var mPrivacyButton: UIButton!
    
    @IBOutlet weak var mPrivacyLabel: UILabel!
    @IBOutlet weak var mTermsLabel: UILabel!
    var mCounter = 0
    var mTimer = Timer()
    
    var mResgistryBasicPresenter : ResgistryBasicPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarWhite()
        setColorGreen()
    }
    
    func setColorGreen(){
        let mTerminos = "He leído y <green>acepto los términos y condiciones</green>"
        let mPrivacidad = "He leído y <green>acepto el aviso de privacidad</green>"
        let redStyle = StringStyle()
        let blueStyle = StringStyle(.color(BONColor.init(cgColor: UIColor(netHex: Colors.color_branding_blue).cgColor)))

        let fishStyle = StringStyle(
            .lineHeightMultiple(1.8),
            .xmlRules([
                .style("green", blueStyle),
                .style("blue", blueStyle),
                ])
        )
        let mAttributedTerms = mTerminos.styled(with: fishStyle)
        
        let mAttributedPrivacidad = mPrivacidad.styled(with: fishStyle)
      
        mPrivacyLabel.attributedText = mAttributedPrivacidad
        mTermsLabel.attributedText = mAttributedTerms
    }
    
    override func getPresenter() -> BasePresenter? {
        mResgistryBasicPresenter = ResgistryBasicPresenter(mViewController: self, mDelegate: self)
        return mResgistryBasicPresenter
    }
    
    @IBAction func privacyAction(_ sender: Any) {
        if(mPrivacSwitch.isOn){
            if(mTermsSwitch.isOn){
                mCrearAccountButton.isUserInteractionEnabled = true
                changeColorButton(mButton: mCrearAccountButton, mColorText: UIColor.white, mColorBackground: UIColor(netHex: Colors.color_branding_gray))
            }
        }else{
            mCrearAccountButton.isUserInteractionEnabled = false
            changeColorButton(mButton: mCrearAccountButton, mColorText: UIColor(netHex: Colors.color_branding_gray), mColorBackground: UIColor(netHex: Colors.color_branding_unselect_gray))
        }
    }
    @IBAction func termsAction(_ sender: Any) {
        if(mTermsSwitch.isOn){
            if(mPrivacSwitch.isOn){
                mCrearAccountButton.isUserInteractionEnabled = true
                changeColorButton(mButton: mCrearAccountButton, mColorText: UIColor.white, mColorBackground: UIColor(netHex: Colors.color_branding_gray))
            }
        }else{
            mCrearAccountButton.isUserInteractionEnabled = false
            changeColorButton(mButton: mCrearAccountButton, mColorText: UIColor(netHex: Colors.color_branding_gray), mColorBackground: UIColor(netHex: Colors.color_branding_unselect_gray))
        }
    }
    
    @IBAction func getNewAction(_ sender: Any) {

    }
    
    @IBAction func mCreateAccountAction(_ sender: Any) {
        mResgistryBasicPresenter.registry(mData: mExtras, mTerms : mTermsSwitch.isOn, mPrivacy : mPrivacSwitch.isOn, mNews : mGetNewsSwitch.isOn)
    }
    
    func changeColorButton(mButton: UIButton, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mButton.backgroundColor = mColorBackground
    }
    
    @IBAction func showTermsActions(_ sender: Any) {
        launchBlurController(mNameStoryBoard: "TermsAndConditions", mViewController: TermsAndConditionsViewController.self,mNameViewController: "TermsAndConditionsViewController")
    }
    
    @IBAction func showPrivacyAction(_ sender: Any) {
        launchBlurController(mNameStoryBoard: "TermsAndConditions", mViewController: TermsAndConditionsViewController.self,mNameViewController: "TermsAndConditionsViewController")
    }
    
    func successRegistry() {
        presentWithNavigationBar(mNameStoryBoard: "Register",mViewController: RegisterViewController.self, mNameViewController: "RegisterViewController")
    }

}
