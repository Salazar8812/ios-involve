//
//  CreateAccountViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

class CreateAccountViewController: BaseViewController {
    @IBOutlet weak var mEmailTextField: TextFieldUtils!
    let validatorManager = ValidatorText()
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarBrown()
        addValidator()
    }

    
    func addValidator(){
        mEmailTextField.delegate = validatorManager
        
        validatorManager.addFieldValidate(
            mFields: [ValidatorField.textField(
                mEmailTextField, //textfield
                .required, //validacion del texto mientras.
                .email,
                "Al parecer el formato del correo electrónico no es el correcto, verifiquelo por favor.",
                self
            )])
        
    }

    @IBAction func mNextAction(_ sender: Any) {
        if(validatorManager.validate()){
            pushViewControllerExtra(mNameStoryBoard: "CreatePassword", mViewController: CreatePasswordViewController.self, mNameViewController: "CreatePasswordViewController", mDelegate: nil, vc: CreatePasswordViewController(), mExtras : [String(mEmailTextField.text!)])
        }
    }

}
