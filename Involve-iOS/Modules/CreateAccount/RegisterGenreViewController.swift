//
//  RegisterGenreViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 11/04/21.
//

import UIKit
import GradientProgress

class RegisterGenreViewController: BaseViewController {
    @IBOutlet weak var mProgessView: GradientProgressBar!
    @IBOutlet weak var mNoBinaryView: UIView!
    @IBOutlet weak var mManView: UIView!
    @IBOutlet weak var mWomanView: UIView!
    
    @IBOutlet weak var mNoBinaryButton: UIButton!
    @IBOutlet weak var mManButton: UIButton!
    @IBOutlet weak var mWomanButton: UIButton!
    
    @IBOutlet weak var mBackButton: UIButton!
    
    var mArraysViews : [UIView] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        setNavigationBarWhite()
        addTargets()
        addViews()
        hideBackButton()
    }
    
    func hideBackButton(){
        if mHiddenBack {
            mBackButton.isHidden = true
        }else{
            mBackButton.isHidden = false
        }
    }
    
    func addViews(){
        mArraysViews.append(mWomanView)
        mArraysViews.append(mManView)
        mArraysViews.append(mNoBinaryView)
    }
    
    func addTargets(){
        mNoBinaryButton.addTarget(self, action: #selector(SelectnoBinaryAction), for: .touchUpInside)
        mManButton.addTarget(self, action: #selector(SelectManAction), for: .touchUpInside)
        mWomanButton.addTarget(self, action: #selector(SelectWomanAction), for: .touchUpInside)
    }
    
    @objc func SelectnoBinaryAction(){
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: true, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        changeSelectedGenre(mButton: mManButton, mView:  mArraysViews[1], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @objc func SelectManAction(){
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mManButton, mView:  mArraysViews[1], mSelect: true, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_branding_blue))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
    }
    
    @objc func SelectWomanAction(){
        changeSelectedGenre(mButton: mNoBinaryButton, mView: mArraysViews[2], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text), mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mManButton, mView:  mArraysViews[1], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
        changeSelectedGenre(mButton: mWomanButton, mView:  mArraysViews[0], mSelect: true, mColorText: UIColor(netHex: Colors.color_white) , mColorBackground: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func selectView(mPos : Int, mButton : UIButton){
        for mIndex in 0...mArraysViews.count - 2{
            if(mIndex == mPos){
                changeSelectedGenre(mButton: mButton, mView: mArraysViews[mIndex], mSelect: true, mColorText: UIColor(netHex: Colors.color_white), mColorBackground: UIColor(netHex: Colors.color_final_strong_green))
            }else{
                changeSelectedGenre(mButton: mButton, mView:  mArraysViews[mIndex], mSelect: false, mColorText: UIColor(netHex: Colors.color_brown_text) , mColorBackground: UIColor(netHex: Colors.color_white))
            }
        }
    }
    
    func changeSelectedGenre(mButton: UIButton, mView : UIView, mSelect: Bool, mColorText : UIColor, mColorBackground : UIColor){
        mButton.setTitleColor(mColorText, for: .normal)
        mView.backgroundColor = mColorBackground
        if(!mSelect){
            mView.layer.borderWidth = 1
            mView.layer.borderColor = mColorText.cgColor
        }else{
            mView.layer.borderWidth = 0
        }
    }
       
    func setProgressBar(){
        mProgessView.gradientColors = [UIColor(netHex: Colors.color_branding_light_blue).cgColor, UIColor(netHex: Colors.color_branding_light_blue).cgColor]
        mProgessView.cornerRadius = 8
        mProgessView.progress = 0.40
    }

    
    @IBAction func mNextAction(_ sender: Any) {
        pushViewController(mNameStoryBoard: "RegisterProfile", mViewController: RegisterProfileViewController.self, mNameViewController: "RegisterProfileViewController", mDelegate: nil)
    }
    
    @IBAction func mBackAction(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
}
