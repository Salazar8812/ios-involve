//
//  RegisterProfileViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 11/04/21.
//

import UIKit
import GradientProgress

class RegisterProfileViewController: BaseViewController {

    @IBOutlet weak var mProgressView: GradientProgressBar!
    
    @IBOutlet weak var mProfileDescriptionTextField: TextFieldUtils!
    let validatorManager = ValidatorText()

    @IBOutlet weak var mBackButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        setNavigationBarWhite()
        addValidator()
        hideBackButton()
    }
    
    @IBAction func mNextAction(_ sender: Any) {
        if(validatorManager.validate()){
            pushViewController(mNameStoryBoard: "RegisterPosition", mViewController: RegisterPositionViewController.self, mNameViewController: "RegisterPositionViewController", mDelegate: nil)
        }
    }
    
    func hideBackButton(){
        if mHiddenBack {
            mBackButton.isHidden = true
        }else{
            mBackButton.isHidden = false
        }
    }
    
    func addValidator(){
        mProfileDescriptionTextField.delegate = validatorManager

        validatorManager.addFieldValidate(mFields:
            [ValidatorField.textField(
                mProfileDescriptionTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self
            )])
    }
    
    @IBAction func mBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setProgressBar(){
        mProgressView.gradientColors = [UIColor(netHex: Colors.color_branding_light_blue).cgColor, UIColor(netHex: Colors.color_branding_light_blue).cgColor]
        mProgressView.cornerRadius = 8
        mProgressView.progress = 0.60
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
}
