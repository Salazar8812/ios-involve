//
//  RegistryBasicPresenter.swift
//  Involve-iOS
//
//  Created by BE-003 on 25/05/21.
//


import UIKit
protocol RegistryBasicDelegate: NSObjectProtocol {
    func successRegistry()
}

class ResgistryBasicPresenter: BaseInvolvePresenter{
    var mViewController : UIViewController!
    var mDelegate : RegistryBasicDelegate!
    var mEmail : String?
    var mPass : String?
    
    init(mViewController : UIViewController, mDelegate : RegistryBasicDelegate) {
        self.mViewController = mViewController
        self.mDelegate = mDelegate
    }
    
    func registry(mData : [String], mTerms : Bool, mPrivacy : Bool, mNews: Bool){
        AlertDialog.showOverlay()
        mEmail = mData[0]
        mPass = mData[1]
        let mRegistryRequest = RegistryRequest(mAcceptNewsLetter: mNews,
                                               mAcceptPrivacy: mPrivacy,
                                               mAcceptTerms: mTerms,
                                               mAttemps: 0,
                                               mEmail: mEmail,
                                               mCreateDate: getCurrentDate(),
                                               mGender: "MAN",
                                               mLastName: nil,
                                               mName: nil,
                                               mPassword: mPass,
                                               mPhone: nil,
                                               mPhoto: nil,
                                               mSecondLastName: nil,
                                               mUserID: nil,
                                               mUserRol: "CANDIDATE")
        BaseRetrofitManager<RegistryResponse>.init(requestUrl: ApiDefinitions.WS_REGISTRY, delegate: self).requestPost(requestModel: mRegistryRequest)
    }
    
    func loginGetToken(){
        let mLoginRequest = LoginInvolveRequest(mEmail: mEmail!, mPassword: mPass!, mEmailSuccesss: true)
        BaseRetrofitManager<LoginInvolveResponse>.init(requestUrl: ApiDefinitions.WS_LOGIN, delegate: self).requestLoginGetToken(requestModel: mLoginRequest)
    }
    
   
    
    func getCurrentDate()-> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let now = Date()
        let dateString = formatter.string(from:now)
        return dateString
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        
        switch requestUrl {
        case ApiDefinitions.WS_LOGIN:
            onSuccessValidateLogin(mResponse: response as! LoginInvolveResponse)
            break
            
        case ApiDefinitions.WS_REGISTRY:
            OnSuccessRegistry(mResponse: response as! RegistryResponse)
            break
        default:
            break
        }
    }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    func onSuccessValidateLogin(mResponse : LoginInvolveResponse){
        AlertDialog.hideOverlay()
        print("Response: \(mResponse.toJSONString(prettyPrint: true) ?? "")")
        mDelegate.successRegistry()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
    
    func OnSuccessRegistry(mResponse : RegistryResponse){
        AlertDialog.hideOverlay()
        print("Response: \(mResponse.toJSONString(prettyPrint: true) ?? "")")
        switch mResponse.mStatus {
        case 412:
            AlertDialog.show(title: "Aviso", body: "Ya existe un usuario registrado con esa dirección", view: mViewController)
            break
        case 201:
            loginGetToken()
            break
        default:
            if(mResponse.mUserModel != nil){
                loginGetToken()
            }
            break
        }
    }
    
}
