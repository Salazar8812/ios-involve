//
//  RegisterStepPresenter.swift
//  Involve-iOS
//
//  Created by Branchbit on 29/06/21.
//

import UIKit

protocol RegisterStepDelegate : NSObjectProtocol {
    func successStepRegister(mResponse : LoginInvolveResponse)
}

class RegisterStepPresenter: BaseInvolvePresenter {
    var mViewController : UIViewController?
    var mRgisterStepDelegate : RegisterStepDelegate?
    
    init(mViewController : UIViewController, mRgisterStepDelegate : RegisterStepDelegate ) {
        self.mViewController = mViewController
        self.mRgisterStepDelegate = mRgisterStepDelegate
    }
    
    func patchUpdateRegistryCandidate(mListValues : [CandidateOperationPatch]){
        AlertDialog.showOverlay()
        let mRequestModel = PatchCandidateRequest(mListDataOperation: mListValues)
        BaseRetrofitManager<LoginInvolveResponse>.init(requestUrl: ApiDefinitions.WS_INFO_CANDIDATE, delegate: self).requestPatchWithToken(requestModel: mRequestModel)
    }
    
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        OnSuccessPatchRegister(mResponse: response as! LoginInvolveResponse)
    }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
    
    func OnSuccessPatchRegister(mResponse : LoginInvolveResponse){
        AlertDialog.hideOverlay()
        print("Response: \(mResponse.toJSONString(prettyPrint: true) ?? "")")
        mRgisterStepDelegate?.successStepRegister(mResponse: mResponse)
    }
}
