//
//  RegisterPositionViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 11/04/21.
//

import UIKit
import GradientProgress

class RegisterPositionViewController: BaseViewController,UITextFieldDelegate, UIGestureRecognizerDelegate {
    @IBOutlet weak var mProgressView: GradientProgressBar!
    @IBOutlet weak var mCountryTextField: TextFieldUtils!
    @IBOutlet weak var mCityTextField: TextFieldUtils!
    let dropManager = DropDownUtil()

    @IBOutlet weak var mPositionTextField: TextFieldUtils!
    
    let validatorManager = ValidatorText()

    @IBOutlet weak var mBackButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        setNavigationBarWhite()
        addValidator()
        addTargets()
        setIcons()
        hideBackButton()
    }
    
    func hideBackButton(){
        if mHiddenBack {
            mBackButton.isHidden = true
        }else{
            mBackButton.isHidden = false
        }
    }
    
    func setIcons(){
        mCountryTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
        mCityTextField.addIconRight(mNameImage: "ic_down_arrow", mColor: UIColor(netHex: Colors.color_branding_blue))
    }
    
    func addValidator(){
        mCountryTextField.delegate = validatorManager
        mCityTextField.delegate = validatorManager
        mPositionTextField.delegate = validatorManager

        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mPositionTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mCountryTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mCityTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func addTargets(){
        let tapCountry = UITapGestureRecognizer(target: self, action: #selector(loadListCountry))
        tapCountry.delegate = self
        mCountryTextField.addGestureRecognizer(tapCountry)
        
        let tapCity = UITapGestureRecognizer(target: self, action: #selector(loadCity))
        tapCity.delegate = self
        mCityTextField.addGestureRecognizer(tapCity)
    }
    
    @objc func loadListCountry(textField: UITextField) {
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "México"),
                                    OptionSettingsModel(mIcon: "", mOption: "Argentina"),
                                    OptionSettingsModel(mIcon: "", mOption: "Colombia"),
                                    OptionSettingsModel(mIcon: "", mOption: "Brazil"),
                                    OptionSettingsModel(mIcon: "", mOption: "Uruguay")]
        mGenericList?.mTextField = mCountryTextField
        present(mGenericList!, animated: true, completion: nil)

    }
    
    @objc func loadCity(textField: UITextField){
        let mGenericList = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController
        mGenericList!.mListOption = [OptionSettingsModel(mIcon: "", mOption: "Veracruz"),
                                    OptionSettingsModel(mIcon: "", mOption: "Monterrey"),
                                    OptionSettingsModel(mIcon: "", mOption: "Sinaloa"),
                                    OptionSettingsModel(mIcon: "", mOption: "Chihuahua"),
                                    OptionSettingsModel(mIcon: "", mOption: "Tijuana")]
        mGenericList?.mTextField = mCityTextField
        present(mGenericList!, animated: true, completion: nil)
    }
    
    @IBAction func mNextAction(_ sender: Any) {
        if(validatorManager.validate()){
            pushViewController(mNameStoryBoard: "RegisterSalary", mViewController: RegisterSalaryViewController.self, mNameViewController: "RegisterSalaryViewController", mDelegate: nil)
        }
    }
    
    @IBAction func mBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setProgressBar(){
        mProgressView.gradientColors = [UIColor(netHex: Colors.color_branding_light_blue).cgColor, UIColor(netHex: Colors.color_branding_light_blue).cgColor]
        mProgressView.cornerRadius = 8
        mProgressView.progress = 0.80
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
