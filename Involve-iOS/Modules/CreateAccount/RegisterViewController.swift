//
//  RegisterViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 10/04/21.
//

import UIKit
import GradientProgress

class RegisterViewController: BaseViewController, RegisterStepDelegate {
    
    @IBOutlet weak var mProgressView: GradientProgressBar!
    @IBOutlet weak var mLastNameTextField: TextFieldUtils!
    @IBOutlet weak var mMiddleNameTextField: TextFieldUtils!
    @IBOutlet weak var mNameTextField: TextFieldUtils!
    let validatorManager = ValidatorText()
    var mRegisterStepPresenter : RegisterStepPresenter!
    var mListValues : [CandidateOperationPatch] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        setNavigationBarWhite()
        addValidator()
    }
    
    override func getPresenter() -> BasePresenter? {
        mRegisterStepPresenter = RegisterStepPresenter(mViewController: self, mRgisterStepDelegate: self)
        return mRegisterStepPresenter
    }
    
    func addValidator(){
        mLastNameTextField.delegate = validatorManager
        mMiddleNameTextField.delegate = validatorManager
        mNameTextField.delegate = validatorManager

        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mNameTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mMiddleNameTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mLastNameTextField, //textfield
                .required, //validacion del texto mientras.
                .name,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }

    @IBAction func mNextAction(_ sender: Any) {
        if(validatorManager.validate()){
            mListValues.append(CandidateOperationPatch(mOperation: "replace",mPath: "/user/name",mValue: mNameTextField.text!))
            mListValues.append(CandidateOperationPatch(mOperation: "replace",mPath: "/user/lastName",mValue: mMiddleNameTextField.text!))
            mListValues.append(CandidateOperationPatch(mOperation: "replace",mPath: "/user/secondLastName",mValue: mLastNameTextField.text!))
            
            mRegisterStepPresenter.patchUpdateRegistryCandidate(mListValues: mListValues)
        }
    }
    
    func setProgressBar(){
        mProgressView.gradientColors = [UIColor(netHex: Colors.color_branding_light_blue).cgColor, UIColor(netHex: Colors.color_branding_light_blue).cgColor]
        mProgressView.cornerRadius = 8
        mProgressView.progress = 0.20
    }
    
    func successStepRegister(mResponse: LoginInvolveResponse) {
        pushViewController(mNameStoryBoard: "RegisterGenre", mViewController: RegisterGenreViewController.self, mNameViewController: "RegisterGenreViewController", mDelegate: nil)
    }
    
}
