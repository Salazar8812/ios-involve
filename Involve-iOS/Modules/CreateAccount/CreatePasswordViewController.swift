//
//  CreatePasswordViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

class CreatePasswordViewController: BaseViewController{
    
    @IBOutlet weak var mContainerIndicatorsView: UIView!
    @IBOutlet weak var mContainerSuccessView: UIView!
    @IBOutlet weak var mFirstPasswordTextField: TextFieldUtils!
    @IBOutlet weak var mConfirmPassWordTextField: TextFieldUtils!
    
    @IBOutlet weak var mFirstIndicatorView: UIView!
    @IBOutlet weak var mSecondIndicatorView: UIView!
    @IBOutlet weak var mThirdIndicatorView: UIView!
    @IBOutlet weak var mFourIndicatorView: UIView!
    let validatorManager = ValidatorText()

    @IBOutlet weak var mContinueButton: UIButton!
    var mIndicators : [UIView] = []
    var mShowPass : Bool = false
    var mShowPassConfirm : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        setNavigationBarWhite()
        mFirstPasswordTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingChanged)
        
        mConfirmPassWordTextField.addTarget(self, action: #selector(textFieldEditingDidChangeConfirm(_:)), for: UIControl.Event.editingChanged)
        setHide(mView: mContainerSuccessView, misHide: true)
        //addValidator()
    }
    
    func addValidator(){
        mFirstPasswordTextField.delegate = validatorManager
        mConfirmPassWordTextField.delegate = validatorManager

        validatorManager.addFieldValidate(mFields: [
            ValidatorField.textField(
                mFirstPasswordTextField, //textfield
                .required, //validacion del texto mientras.
                .all,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            ),
            ValidatorField.textField(
                mConfirmPassWordTextField, //textfield
                .required, //validacion del texto mientras.
                .all,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    func setViews(){
        mIndicators.append(mFirstIndicatorView)
        mIndicators.append(mSecondIndicatorView)
        mIndicators.append(mThirdIndicatorView)
        mIndicators.append(mFourIndicatorView)
    }
    
    @objc func textFieldEditingDidChange(_ mPass: UITextField) {
        let mFlags = checkPassword(text: mPass.text!)

        for mPosition in 0 ... mFlags.count-1{
            if(mFlags[mPosition]){
                changeColor(mIndicatorView: mIndicators[mPosition], mColor: UIColor(netHex: Colors.color_branding_blue))
            }else{
                changeColor(mIndicatorView: mIndicators[mPosition], mColor: UIColor(netHex: Colors.color_branding_unselect_gray))

            }
        }
        
        if(mFlags[0] && mFlags[1] && mFlags[2] && mFlags[3]){
            setHide(mView: mContainerIndicatorsView, misHide: true)
            setHide(mView: mContainerSuccessView, misHide: false)
        }else{
            setHide(mView: mContainerIndicatorsView, misHide: false)
            setHide(mView: mContainerSuccessView, misHide: true)
        }
        
    }
    
    @objc func textFieldEditingDidChangeConfirm(_ mPass : UITextField){
        if(mFirstPasswordTextField.text == mPass.text){
            mContinueButton.isHidden = false
        }else{
            mContinueButton.isHidden = true
        }
    }
    
    func setHide(mView : UIView, misHide : Bool){
        mView.isHidden = misHide
    }
    
    func changeColor(mIndicatorView : UIView, mColor: UIColor){
        mIndicatorView.backgroundColor = mColor
    }
    
    @IBAction func mContinueAction(_ sender: Any) {
        mExtras.append(mFirstPasswordTextField.text!)
        pushViewControllerExtra(mNameStoryBoard: "Terms", mViewController: TermsViewController.self, mNameViewController: "TermsViewController", mDelegate: nil, vc: TermsViewController(),mExtras: mExtras)
    }
    
    @IBAction func mShowPassConfirmAction(_ sender: Any) {
        mShowPass = showHidePass(mInputField: mConfirmPassWordTextField, mShowPass: mShowPass)
    }
    
    @IBAction func mShowPassAction(_ sender: Any) {
        mShowPassConfirm = showHidePass(mInputField: mFirstPasswordTextField, mShowPass: mShowPassConfirm)
    }
    
    func showHidePass(mInputField : UITextField, mShowPass: Bool)->Bool{
        if(!mShowPass){
            mInputField.isSecureTextEntry = false
            return true
        }else{
            mInputField.isSecureTextEntry = true
            return false
        }
    }
}
