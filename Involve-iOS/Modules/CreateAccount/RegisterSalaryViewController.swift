//
//  RegisterSalaryViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 11/04/21.
//

import UIKit
import GradientProgress

class RegisterSalaryViewController: BaseViewController {
    @IBOutlet weak var mProgressView: GradientProgressBar!
    @IBOutlet weak var mSalaryTextField: TextFieldUtils!
    let validatorManager = ValidatorText()

    var mCounter = 0
    @IBOutlet weak var mBackButton: UIButton!
    var mTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setProgressBar()
        setNavigationBarWhite()
        addValidator()
        mSalaryTextField.addTextLeft(mChar: "$", mColor: UIColor(netHex: Colors.color_grey_select))
        hideBackButton()
    }
    
    func hideBackButton(){
        if mHiddenBack {
            mBackButton.isHidden = true
        }else{
            mBackButton.isHidden = false
        }
    }

    @IBAction func mFinisgAction(_ sender: Any) {
        if(validatorManager.validate()){
            delayActionValidaEmail()
        }
    }
    
    func addValidator(){
        mSalaryTextField.delegate = validatorManager

        validatorManager.addFieldValidate(mFields:[
            ValidatorField.textField(
                mSalaryTextField, //textfield
                .required, //validacion del texto mientras.
                .phone,
                "Faltan campos por llenar",
                self// tipo de caracteres validos
            )])
    }
    
    @IBAction func mBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setProgressBar(){
        mProgressView.gradientColors = [UIColor(netHex: Colors.color_branding_light_blue).cgColor, UIColor(netHex: Colors.color_branding_light_blue).cgColor]
        mProgressView.cornerRadius = 8
        mProgressView.progress = 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func delayActionValidaEmail(){
        AlertDialog.showOverlay()
        mTimer.invalidate()
        mCounter = 0
        mTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(actionDelayValidEmail), userInfo: nil, repeats: true)
    }
    
    @objc func actionDelayValidEmail(){
        mCounter += 1
        if(mCounter == 20){
            mTimer.invalidate()
            AlertDialog.hideOverlay()
            send()
        }
    }
    
    func send(){
        presentWithNavigationBar(mNameStoryBoard: "Home", mViewController: HomeViewController.self, mNameViewController: "HomeViewController")
    }
    
}
