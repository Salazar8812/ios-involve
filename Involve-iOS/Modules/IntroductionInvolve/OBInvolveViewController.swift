//
//  OBInvolveViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit
import SlidingContainerViewController

class OBInvolveViewController: BaseViewController, SlidingContainerSliderViewDelegate, SlidingContainerViewControllerDelegate {
    @IBOutlet weak var mSkipButton: UIButton!
    @IBOutlet weak var mMainContainerView: UIView!
    @IBOutlet weak var mPageControl: UIPageControl!
    var mArrayControllers : [UIViewController] = []
    var mPersistentData : PersistenceData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mPageControl.numberOfPages = 3
        addTargetAction()
        populateSteps()
        setDataSourceTabs()
        initializePersisitantData()
    }
    
    func initializePersisitantData(){
        mPersistentData = PersistenceData()
    }
    
    func addTargetAction(){
        mSkipButton.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
    }
    
    @objc func skipAction(){
        presentWithNavigationBar(mNameStoryBoard: "LoginInvolve",mViewController: LoginInvolveViewController.self, mNameViewController: "LoginInvolveViewController")
        mPersistentData.saveData(mData: "on", meKey: "Skip")
    }
    
    func setDataSourceTabs(){
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: mArrayControllers,
            titles: [],mHeight: 0)
        slidingContainerViewController.delegate = self
        
        mMainContainerView.addSubview(slidingContainerViewController.view)
    }
    
    func populateSteps(){
        let storyboard : UIStoryboard = UIStoryboard(name: "PageStepItem", bundle: nil)
        
        let step1 = (storyboard.instantiateViewController(withIdentifier: "PageStepItemViewController") as! PageStepItemViewController)
        step1.mCompoments = ["ic_slider_one","Talent Network","Vive la experiencia de pertenecer a nuestra red de talento"]
        let step2 = (storyboard.instantiateViewController(withIdentifier: "PageStepItemViewController") as! PageStepItemViewController)
        step2.mCompoments = ["ic_slider_two","Vacantes","Excelentes oportunidades de empleo al alcance de tu mano"]
        let step3 = (storyboard.instantiateViewController(withIdentifier: "PageStepItemViewController") as! PageStepItemViewController)
        step3.mCompoments = ["ic_slider_three","Postúlate","Solo crea tu cuenta, sube tu curriculum y comienza a postularte"]

        mArrayControllers.append(step1)
        mArrayControllers.append(step2)
        mArrayControllers.append(step3)
    }
    
    func slidingContainerSliderViewDidPressed(_ slidingtContainerSliderView: SlidingContainerSliderView, atIndex: Int) {
        mPageControl.currentPage = atIndex
    }
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        mPageControl.currentPage = atIndex
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    
}
