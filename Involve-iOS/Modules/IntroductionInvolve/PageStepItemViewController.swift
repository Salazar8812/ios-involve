//
//  PageStepItemViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

class PageStepItemViewController: UIViewController {
    
    @IBOutlet weak var mBannerImageView: UIImageView!
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mDescriptionLabel: UILabel!
    var mCompoments : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateInfo()
    }
    
    func populateInfo(){
        mBannerImageView.image = UIImage(named: mCompoments[0])
        mTitleLabel.text = mCompoments[1]
        mDescriptionLabel.text = mCompoments[2]
    }

}
