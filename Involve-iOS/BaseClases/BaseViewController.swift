//
//  BaseViewController.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 11/04/21.
//

import UIKit

class BaseViewController: UIViewController {
    var mPresenter : BasePresenter?
    var mPresenters : [BasePresenter]! = []
    var mDelegate : NSObjectProtocol?
    var mVC : UIViewController?
    var mExtras : [String] = []
    var mHiddenBack : Bool = false
    var mTextfield : TextFieldUtils?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mPresenter = getPresenter()
        mPresenters = getPresenters()
        if mPresenter != nil {
            mPresenter?.viewDidLoad()
        }
        for presenter in mPresenters! {
            presenter.viewDidLoad()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if mPresenter != nil {
            mPresenter?.viewDidAppear()
        }
        for presenter in mPresenters! {
            presenter.viewDidAppear()
        }
    }
    
    func getPresenter() -> BasePresenter? {
        return nil
    }
    
    func getPresenters() -> [BasePresenter]? {
        return []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillAppear()
        }
        for presenter in mPresenters! {
            presenter.viewWillAppear()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillDisappear()
        }
        for presenter in mPresenters! {
            presenter.viewWillDisappear()
        }
    }
    
    func launchBlurController(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String) {
        let storyboard = UIStoryboard(name: mNameStoryBoard, bundle: Bundle(for: BaseViewController.self))
        let v1 = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! BaseViewController
        MIBlurPopup.show(v1, on: self)
    }
    
    func launchBlurControllerCalendar(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String, mDelegate : NSObjectProtocol, mField : TextFieldUtils) {
        let storyboard = UIStoryboard(name: mNameStoryBoard, bundle: Bundle(for: BaseViewController.self))
        let v1 = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! BaseViewController
        v1.mTextfield = mField
        v1.mDelegate = mDelegate
        MIBlurPopup.show(v1, on: self)
    }
    
    func presentViewController(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String){
        let storyboard : UIStoryboard = UIStoryboard(name: mNameStoryBoard, bundle: nil)
        let vc : BaseViewController = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! BaseViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func pushViewController(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String, mDelegate : NSObjectProtocol?){
        let vc = UIStoryboard.init(name: mNameStoryBoard, bundle: Bundle.main).instantiateViewController(withIdentifier: mNameViewController) as? BaseViewController
        vc?.mDelegate = mDelegate
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func pushViewControllerExtra(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String, mDelegate : NSObjectProtocol?, vc: BaseViewController, mExtras: [String]){
        let vc = UIStoryboard.init(name: mNameStoryBoard, bundle: Bundle.main).instantiateViewController(withIdentifier: mNameViewController) as? BaseViewController
        vc?.mDelegate = mDelegate
        vc?.mVC = vc
        vc?.mExtras = mExtras
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func presentWithNavigationBar(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String){
        let storyboard : UIStoryboard = UIStoryboard(name: mNameStoryBoard, bundle: nil)
        
        let vc : BaseViewController = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! BaseViewController
        
        vc.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func presentWithNavigationBarHideBack(mNameStoryBoard : String, mViewController: UIViewController.Type, mNameViewController: String){
        let storyboard : UIStoryboard = UIStoryboard(name: mNameStoryBoard, bundle: nil)
        
        let vc : BaseViewController = storyboard.instantiateViewController(withIdentifier: mNameViewController) as! BaseViewController
        
        vc.modalPresentationStyle = .fullScreen
        vc.mHiddenBack = true
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func showBottomSheet(mListItems: [NSObject.Type]){
        guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "GenericMenu") as? GenericMenuViewController else { return }
        present(popupVC, animated: true, completion: nil)
    }
    
    func setNavigationBarWhite(){
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor(netHex: Colors.color_white)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(netHex: Colors.color_white)]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIColor.white.as1ptImage()
        UINavigationBar.appearance().setBackgroundImage(UIColor.white.as1ptImage(), for: .default)
    }

    func setNavigationBarBrown(){
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor(netHex: Colors.color_brown_background)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(netHex: Colors.color_final_strong_green)]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIColor.white.as1ptImage()
        UINavigationBar.appearance().setBackgroundImage(UIColor.white.as1ptImage(), for: .default)
    }
    
    func checkPassword( text : String) -> [Bool]{
        var mArrayValidate : [Bool] = []
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        mArrayValidate.append(capitalresult)

        
        let minLetterRegEx  = ".*[a-z]+.*"
        let texttest0 = NSPredicate(format:"SELF MATCHES %@", minLetterRegEx)
        let minusResult = texttest0.evaluate(with: text)
        mArrayValidate.append(minusResult)


        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        mArrayValidate.append(numberresult)

        mArrayValidate.append(text.count >= 8 ? true : false)
       
        return mArrayValidate
    }
}


extension UIColor {
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}
