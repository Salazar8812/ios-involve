//
//  ItemOptionMultipleTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 05/05/21.
//

import UIKit

class ItemOptionMultipleTableViewCell: UITableViewCell {
    @IBOutlet weak var mOptionSelectBotton: UIButton!
    @IBOutlet weak var mOptionLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
