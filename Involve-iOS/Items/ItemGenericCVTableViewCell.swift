//
//  ItemGenericCVTableViewCell.swift
//  Involve-iOS
//
//  Created by BE-003 on 28/04/21.
//

import UIKit

class ItemGenericCVTableViewCell: UITableViewCell {

    @IBOutlet weak var mOptionButton: UIButton!
    @IBOutlet weak var mYearLabel: UILabel!
    @IBOutlet weak var mDescriptionLabel: UILabel!
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
