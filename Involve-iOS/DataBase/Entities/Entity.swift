//
//  Entity.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import Foundation
import RealmSwift

class Entity: Object, DBManagerDelegate {
    @objc dynamic var id: Int = 0
    
    override static func primaryKey() -> String {
        return "id"
    }
    
    func setId<T: Entity>(entity: T.Type) {
        let id = (DBManager.shared.nextId(from: entity))
        self.id = id
    }
    
    func setID(mAuxID : Int){
        self.id = mAuxID
    }
    
    func getByID<T: Entity>(entity: T.Type, id: String, field: String) -> T? {
        let element = realmInstance().objects(entity).filter("\(field) == '\(id)'").first
        return element
    }
}
