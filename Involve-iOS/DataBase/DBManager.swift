//
//  DBManager.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//
import Foundation
import RealmSwift

enum FetchResult {
    case all([Entity])
    case single(Entity)
}

class DBManager {
    var realm: Realm
    public static var shared = DBManager()
    private var CRUD: DBCrud?
    
    var delegate: DBManagerDelegate?

    init() {
        let config = Realm.Configuration(
            schemaVersion: 4,
            migrationBlock: { _, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    // Apply any necessary migration logic here.
                }
        }, deleteRealmIfMigrationNeeded: true
        )
        Realm.Configuration.defaultConfiguration = config
        realm = try! Realm()
        print("Realm file: \(String(describing: realm.configuration.fileURL))")
    }
    
    func start() {
        self.CRUD = DBCrud(realm: realm)
    }
    
    
    func update(updatingCode: @escaping () -> Void) {
        try! self.realm.write {
            updatingCode()
        }
    }
    
    func nextId<T: Entity>(from: T.Type) -> Int {
        let id = (realm.objects(from).max(ofProperty: "id") as Int? ?? 0) + 1
        return id
    }
    
    struct DBCrud {
        
        var realm: Realm
        
        init(realm: Realm) {
            self.realm = realm
        }
        
        func add<T: Entity>(entity: T.Type, elements: [T]) {
            try! realm.write {
                elements.forEach({ element in
                    element.setId(entity: entity)
                    realm.add(element)
                })
            }
        }
        
        func addOnly<T: Entity>(entity: T.Type, element: T) {
            try! realm.write {
                //elements.forEach({ element in
                    element.setId(entity: entity)
                realm.add(element)
                //})
            }
        }
        
        func getAll<T: Entity>(type: T.Type) -> [T] {
            let realmResults = realm.objects(type)
            var results = [T]()
            realmResults.forEach { element in
                results.append(element)
            }
            return results
        }
        
        func getFrom<T: Entity>(type: T.Type, field: String, equalsTo: String) -> T? {
            return realm.objects(type).filter("\(field) == '\(equalsTo)'").first
        }
        
        func deleteCollection<T: Entity>(type: T.Type) {
            try! realm.write {
                realm.delete(realm.objects(type))
            }
        }
        
        func deleteFrom<T: Entity>(type: T.Type, field: String, equalsTo: String) {
            let element = getFrom(type: type, field: field, equalsTo: equalsTo)
            try! realm.write {
                guard let el = element else {
                    return
                }
                realm.delete(el)
            }
        }
        
        func deleteAll() {
            try! realm.write {
                realm.deleteAll()
            }
        }
        
        func nextId<T: Entity>(from: T.Type) -> Int {
            let id = (realm.objects(from).max(ofProperty: "id") as Int? ?? 0) + 1
            return id
        }
    }
}

