//
//  DBManagerDelegate.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import Foundation
import RealmSwift

protocol DBManagerDelegate {
    func writeOnDB(code: @escaping () -> Void)
    func realmInstance() -> Realm
}

extension DBManagerDelegate {
    func writeOnDB(code: @escaping () -> Void) {
        DBManager.shared.update {
            code()
        }
    }
    
    func realmInstance() -> Realm {
        return DBManager.shared.realm
    }
}

