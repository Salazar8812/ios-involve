//
//  Colors.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 09/04/21.
//

import UIKit

extension UIColor {
    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

public class Colors {
    
    public static let color_final_strong_green : Int = 0x29AC3B
    public static let color_medium_green : Int = 0x59BE29
    public static let color_begin_green : Int = 0x96D112
    public static let color_green_button : Int = 0x24AA3D
    public static let color_brown_text : Int = 0xCFCFCF// 0x565656
    public static let color_black : Int = 0x7A7A7A
    public static let color_brown_background = 0x3C3C3B
    public static let color_white = 0xFFFFFF
    public static let color_branding_blue = 0x0BB9B9
    public static let color_branding_light_blue = 0xA8EAEA
    public static let color_branding_gray = 0x565656
    public static let color_branding_light_gray = 0x979797
    public static let color_branding_unselect_gray = 0xF4F4F4
    
    public static let color_done_background = 0x3C3C3B

    public static let color_grey_unselect = 0x9B9B9B
    public static let color_grey_select = 0x3C3C3B
    
    public static let colot_tab_selector_green = 0x24AA3D
    
    public static let color_gray_unselect_button = 0xEEEEED
    
    public static let color_yellow = 0xE7D44B
    public static let color_gray_less_profile = 0xCFCFCF
}
