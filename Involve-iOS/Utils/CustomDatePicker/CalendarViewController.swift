//
//  CalendarViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 01/06/21.
//

import UIKit

protocol CalendarDelegate: NSObjectProtocol {
    func getSelectedDate(mDate : String, mField : TextFieldUtils)
}


class CalendarViewController: BaseViewController {

    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mCloseButton: UIButton!
    @IBOutlet weak var mMainContentView: UIView!
    @IBOutlet weak var mPViewPicker: PersianDatePickerView!
    
    var customBlurEffectStyle: UIBlurEffect.Style!
    var mCalendarDelegate : CalendarDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTitleLabel.roundCorners([.topLeft, .topRight], radius: 10)
    }
    
    @IBAction func mCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mOkAction(_ sender: Any) {
        mCalendarDelegate = (mDelegate as! CalendarDelegate)
        let mDate = mPViewPicker.getPersianDate() ?? ""
        let mTransformArray = mDate.split{$0 == "/"}.map(String.init)
        let mFormattedDate = mTransformArray[2] + "/" + mTransformArray[1] + "/" + mTransformArray[0]
        mCalendarDelegate.getSelectedDate(mDate: mFormattedDate, mField: mTextfield!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func mCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
}

extension CalendarViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mMainContentView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
