//
//  DropDownUtil.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit
import DropDown

class DropDownUtil: NSObject {
    private var lista: [UITextField: DropDown] = [:]
    private var listaToolTipsIsShow: [UITextField: Bool] = [:]
    
    private var listaB: [UIView: DropDown] = [:]
    private var listaToolTipsIsShowB: [UIView: Bool] = [:]
    
    private var view: UIView?
    private let tiempoToolTip = 2000
    let borderColorBien = UIColor.lightGray
    let borderColorMal = UIColor.red
    let mensajeError = "Seleccione una opción."
    var mEnableDesign : Bool = true

    
    override init() {
        DropDown.appearance().textColor = UIColor(netHex: Colors.color_grey_select)
        //DropDown.appearance().textFont = Fonts.Title.normal
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor(netHex: Colors.color_begin_green)
        DropDown.appearance().cornerRadius = 4
    }

    func enableDesign(mEnable : Bool){
        mEnableDesign = mEnable
    }
    
   
    private func existe(_ textfield: UITextField) -> Bool {
        return lista[textfield] != nil
    }

    private func existeB(_ textfield: UIView) -> Bool {
           return listaB[textfield] != nil
       }

    func add(textfield: UITextField, data array: [String], view: UIView? = nil, selectionAction closure: SelectionClosure?) {
        /*guard !existe(textfield) else {
            Logger.w("Ya existe un dropdown para ese textfield.")
            return
        }*/

        self.view = view

        let dropDown = DropDown()
        dropDown.anchorView = textfield // UIView or UIBarButtonItem
        dropDown.dataSource = array
        dropDown.direction = .bottom

        lista[textfield] = dropDown
        listaToolTipsIsShow[textfield] = false

        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { (index: Int, item: String) in
            textfield.text = item
            if let closure = closure {
                closure(index, item)
            }
        }
    }
    
    func addB(textfield: UIView, data array: [String], view: UIView? = nil, selectionAction closure: SelectionClosure?) {
           guard !existeB(textfield) else {
               return
           }

           self.view = view

           let dropDown = DropDown()
           dropDown.anchorView = textfield // UIView or UIBarButtonItem
           dropDown.dataSource = array
           dropDown.direction = .bottom

           listaB[textfield] = dropDown
           listaToolTipsIsShowB[textfield] = false

           dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
           dropDown.selectionAction = { (index: Int, item: String) in
               //textfield.text = item
               if let closure = closure {
                   closure(index, item)
               }
           }
       }

    func seleccionarDropDown(pos: Int, _ textField: UITextField) {
        if let dropDown = lista[textField] {
            dropDown.clearSelection()
            dropDown.selectRow(at: pos)
            textField.text = dropDown.selectedItem
            _ = validate()
        }
    }

    func updateDropdown(pos: Int, array: [String], textfield: UITextField) {
        if let dropDown = lista[textfield] {
            dropDown.dataSource = array
            dropDown.clearSelection()

            dropDown.selectRow(at: pos)
            guard pos >= 0 && pos < array.count else {
                textfield.text = ""
                return
            }
            textfield.text = dropDown.selectedItem
             _ = validate()
        }
    }

    func updateDropdown(value: String, array: [String], textfield: UITextField) {
        if let dropDown = lista[textfield] {
            dropDown.dataSource = array
            dropDown.clearSelection()
            var pos = -1
            for (i, v) in array.enumerated() {
                if v == value { pos = i }
            }
            guard pos != -1 else {
                return
            }
            dropDown.selectRow(at: pos)
            guard pos >= 0 && pos < array.count else {
                textfield.text = ""
                return
            }
            textfield.text = dropDown.selectedItem
            _ = validate()
        }
    }

    func clear() {
        for (textfield, dropDown) in lista {
            textfield.text = ""
            dropDown.clearSelection()
            bien(textfield)
        }
    }

    func validate() -> Bool {
        var res = true
        for (textfield, _) in lista {
            let texto = textfield.text ?? ""
        }
        return res
    }

    private func bien(_ textField: UITextField) {
        if(mEnableDesign){
            textField.layer.borderColor = borderColorBien.cgColor
            textField.layer.borderWidth = 0.25
        }
        
//        textField.addLineBotom(color: borderColorBien, width: 0.25)

    }

    private func mal(_ textField: UITextField) {
        if(mEnableDesign){
            textField.layer.borderColor = borderColorMal.cgColor
            textField.layer.borderWidth = 2
        }
//        textField.addLineBotom(color: borderColorMal, width: 2)
      
    }
}

extension DropDownUtil: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let dropDown = lista[textField] {
            if let view = view {
                view.endEditing(true)
            }
            dropDown.show()
            return false
        }
        return true
    }

}



struct Fonts {
    struct Title {
        static let normal: UIFont = UIFont(name: "redhat-medium", size: 16)!
    }
}
