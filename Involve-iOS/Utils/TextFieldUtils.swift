//
//  TextFieldUtils.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 13/04/21.
//

import UIKit

class TextFieldUtils: UITextField {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    required override init(frame: CGRect) {
        super.init(frame: frame)
    }

    func errorBorder(){
        let imageView = UIImageView(frame: CGRect(x: -4.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: "ic_error")
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.red
        
        imageView.contentMode = .scaleAspectFit
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.red.cgColor
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 40))
        view.addSubview(imageView)
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = view
    }
    
    func doneBorder(){
        let imageView = UIImageView(frame: CGRect(x: -4.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: "")
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.red
        
        imageView.contentMode = .scaleAspectFit
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(netHex: Colors.color_grey_unselect).cgColor
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 40))
        view.addSubview(imageView)
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = view
    }
    
}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}

extension UITextField{
    
    func addIconRight(mNameImage: String, mColor : UIColor){
        let imageView = UIImageView(frame: CGRect(x: -4.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: mNameImage)
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = mColor
        
        imageView.contentMode = .scaleAspectFit

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
        view.addSubview(imageView)
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = view
    }
    
    func addIconRightMin(mNameImage: String, mColor : UIColor){
        let imageView = UIImageView(frame: CGRect(x: -4.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: mNameImage)
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = mColor
        
        imageView.contentMode = .scaleAspectFit

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 22, height: 40))
        view.addSubview(imageView)
        self.rightViewMode = UITextField.ViewMode.always
        self.rightView = view
    }
    
    
    func addIconLeft(mNameImage: String, mColor : UIColor){
        let imageView = UIImageView(frame: CGRect(x: -4.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: mNameImage)
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = mColor
        
        imageView.contentMode = .scaleAspectFit

        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
        view.addSubview(imageView)
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = view
    }
    
    func addIconLeftCalendar(mNameImage: String, mColor : UIColor){
        let imageView = UIImageView(frame: CGRect(x:  12.0, y: 8.0, width: 21.0, height: 21.0))
        let image = UIImage(named: mNameImage)
        imageView.image = image
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = mColor
        
        imageView.contentMode = .scaleAspectFit

        let view = UIView(frame: CGRect(x: 15, y: 15, width: 35, height: 35))
        view.addSubview(imageView)
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = view
    }
    
    func addTextLeft(mChar: String, mColor : UIColor){
        let mText = UILabel(frame: CGRect(x: 8.0, y: 8.0, width: 21.0, height: 21.0))
        mText.text = mChar
        mText.font = UIFont(name: mChar, size: 16)
        mText.textColor = mColor
        
        let view = UIView(frame: CGRect(x: 8.0, y: 0, width: 14, height: 40))
        view.addSubview(mText)
        self.leftViewMode = UITextField.ViewMode.always
        self.leftView = view
    }
}
