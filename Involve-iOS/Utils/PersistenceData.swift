//
//  PersistenceData.swift
//  Involve-iOS
//
//  Created by BE-003 on 27/04/21.
//

import UIKit

class PersistenceData: NSObject {
    
    func saveData(mData : String, meKey : String){
        UserDefaults.standard.set(mData, forKey: meKey)
    }
    
    func retrieveData(mKey: String)->String{
        let mData = UserDefaults.standard.string(forKey: mKey) != nil ? UserDefaults.standard.string(forKey: mKey) : ""
        return mData!
    }
}
