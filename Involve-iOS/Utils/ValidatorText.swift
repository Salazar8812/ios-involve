//
//  ValidatorText.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 14/04/21.
//

import UIKit

enum ExpressionRegular : String{
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    case ip = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
    case ipSepateComa = "((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]),)*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))+$"
    case hostName = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$"
    case hostNameSeparateComa = "((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]),)*((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))+$"
    case phone = "^\\d{3}-\\d{3}-\\d{4}$"
    case rfc = "[A-Z,a-z,ñ,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,a-z,ñ,Ñ,0-9][A-Z,a-z,Ñ,ñ,0-9][0-9,A-Z,a-z,Ñ,ñ]"
    
    case name = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"
    case all = "^[A-Za-z0-9!\"#$%&'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~].{8,}$"

}
enum InputType{
    case correo
    case none
    case required
}

enum ValidatorField {
    case textField(TextFieldUtils, InputType, ExpressionRegular, String, UIViewController)
}

class ValidatorText: NSObject {
    var listaValidator = [ValidatorField]()
    var mErrorMessage : String?
    var mViewController : UIViewController?

    
    func validate()->Bool{
        var res = true
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, let mType ,  let regexType, let mMessage, let mViewController):
                self.mErrorMessage = mMessage
                self.mViewController = mViewController
                res = res && validateText(mTextField: textField, mExpressionRegular: regexType, mType : mType, mMessageError: mMessage, mViewController: mViewController)
            }
        }
        return res
    }
    
    func addFieldValidate(mFields : [ValidatorField]){
        for field in mFields {
            listaValidator.append(field)
            switch field {
            case .textField(let textField, _, _, let mMessage, let mViewController):
                self.mErrorMessage = mMessage
                self.mViewController = mViewController
                textField.addTarget(
                    self,
                    action: #selector(textFieldEditingDidChange(_:)),
                    for: UIControl.Event.editingChanged)

            }
        }
    }
    
    func validateText(mTextField : TextFieldUtils, mExpressionRegular : ExpressionRegular , mType : InputType, mMessageError : String, mViewController : UIViewController) -> Bool{
        self.mErrorMessage = mMessageError
        self.mViewController = mViewController
        switch (mType) {
        case .none:
            break
        case .required:
            return validateRequired(mTextField: mTextField, mInputRegex: mExpressionRegular)
        default: return false
        }
        return true
    }
    
    
    func validarRegex(mTextField: TextFieldUtils, mRegex : String) -> Bool {
        let predicate = NSPredicate(format: "SELF MATCHES %@", mRegex)
        let result = predicate.evaluate(with: mTextField.text?.trim())
        if result {
            mTextField.doneBorder()
        } else {
            AlertDialog.show(title: "Aviso", body: mErrorMessage!, view: mViewController!)
            mTextField.errorBorder()
        }
        return result
    }
    
    
    func validateRequired(mTextField: TextFieldUtils, mInputRegex : ExpressionRegular) -> Bool {
        if !mTextField.text!.trim().isEmpty {
            switch mInputRegex {
            case .email:
                return validarRegex(mTextField: mTextField, mRegex: mInputRegex.rawValue)
            default:
                mTextField.doneBorder()
                return true
            }
        } else {
            AlertDialog.show(title: "Aviso", body: "El campo no puede estar vacío", view: mViewController!)
            mTextField.errorBorder()
            return false
        }
    }
    
    
   @objc func textFieldEditingDidChange(_ sender: TextFieldUtils) {
        if(sender.text!.count > 0){
            sender.doneBorder()
        }else{
            sender.errorBorder()
        }
    }
    

}

extension ValidatorText: UITextFieldDelegate {

    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.count > 0 else { return true }
        return validateField(textField, range, string)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.uppercased()
        if let closure = listaActionEndEditing[textField] {
            if let closure = closure {
                closure(textField)
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let closure = listaActionBeginEditing[textField] {
            if let closure = closure {
                closure(textField)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let textFieldTo = listaChangeField[textField] {
            textFieldTo.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }*/
}
