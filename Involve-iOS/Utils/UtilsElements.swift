//
//  UtilsElements.swift
//  Involve-iOS
//
//  Created by BE-003 on 25/04/21.
//

import UIKit

extension UIImageView {
    func roundedImage(mColor : UIColor) {
        self.layer.cornerRadius = (self.frame.size.width) / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 4.0
        self.layer.borderColor = mColor.cgColor
    }
    
    func setTintColor(mColor : UIColor){
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = mColor
    }
}

extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
    
    
    func roundedView(mColor : UIColor) {
        self.layer.cornerRadius = (self.frame.size.width) / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 2.0
        self.layer.borderColor = mColor.cgColor
    }
    
    func addDashedBorder() {
        let color = UIColor(netHex: Colors.color_branding_blue).cgColor

      let shapeLayer:CAShapeLayer = CAShapeLayer()
      let frameSize = self.frame.size
      let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

      shapeLayer.bounds = shapeRect
      shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
      shapeLayer.fillColor = UIColor.clear.cgColor
      shapeLayer.strokeColor = color
      shapeLayer.lineWidth = 2
      shapeLayer.lineJoin = CAShapeLayerLineJoin.round
      shapeLayer.lineDashPattern = [6,3]
      shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

      self.layer.addSublayer(shapeLayer)
      }
}
