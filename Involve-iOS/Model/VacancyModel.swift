//
//  VacancyModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class VacancyModel: NSObject {
    var mTitleVacancy : String?
    var mFavs : Bool?
    var mDate : String?
    var mDescription : String?
    var mSalary : String?
    var mModality : String?
    var mContract : String?
    var mLocation : String?
    var mJourney : String?
    
    init(mTitleVacancy: String, mFavs : Bool, mDate : String,mDescription : String, mSalary: String, mModality: String,  mContract : String, mLocation: String, mJourney : String ) {
        self.mTitleVacancy = mTitleVacancy
        self.mFavs = mFavs
        self.mDate = mDate
        self.mDescription = mDescription
        self.mSalary = mSalary
        self.mModality = mModality
        self.mContract = mContract
        self.mLocation = mLocation
        self.mJourney = mJourney
    }
}
