//
//  VacancyActiveModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/06/21.
//

import UIKit

class VacancyActiveModel: NSObject {
    var mTitleVacancy : String?
    var mDatePostulate : String?
    var mStepsPass : [Bool]?
    
    init(mTitleVacancy : String, mDatePostulate: String) {
        self.mTitleVacancy = mTitleVacancy
        self.mDatePostulate = mDatePostulate
    }
    
    init(mTitleVacancy : String, mDatePostulate: String, mStepsPass : [Bool]) {
        self.mTitleVacancy = mTitleVacancy
        self.mDatePostulate = mDatePostulate
        self.mStepsPass = mStepsPass
    }
}
