//
//  SearchCoincidenceModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class SearchCoincidenceModel: NSObject {
    var mIdSearch : String?
    var mTitleCoincidence : String?
    
    init(mIdSearch : String, mTitleCoincidence : String) {
        self.mIdSearch = mIdSearch
        self.mTitleCoincidence = mTitleCoincidence
    }

}
