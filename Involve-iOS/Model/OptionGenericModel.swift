//
//  OptionGenericModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class OptionGenericModel: NSObject {
    var mTitleOption : String?
    var mIconButton : String?
    var mMultipleSelectionEnable : Bool?
    
    
    init(mTitleOption : String, mIconButton : String, mMultipleSelectionEnable: Bool) {
        self.mTitleOption = mTitleOption
        self.mIconButton = mIconButton
        self.mMultipleSelectionEnable = mMultipleSelectionEnable
    }

}
