//
//  OptionCVModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 21/04/21.
//

import UIKit

class OptionCVModel: NSObject {
    var mImageIcon : String?
    var mTitleOption : String?
    
    init(mImageIcon : String, mTitleOption : String) {
        self.mImageIcon = mImageIcon
        self.mTitleOption = mTitleOption
    }
}
