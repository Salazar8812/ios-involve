//
//  OptionFiltersModel.swift
//  Involve-iOS
//
//  Created by BE-003 on 09/05/21.
//

import UIKit

class OptionFiltersModel: NSObject {
    var mTitleSectionFilter:  String?
    var mListOptions : [OptionSettingsModel] = []
    
    init(mTitleSectionFilter : String , mListOptions : [OptionSettingsModel]) {
        self.mTitleSectionFilter = mTitleSectionFilter
        self.mListOptions = mListOptions
    }
    
}
