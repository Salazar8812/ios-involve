//
//  OptionSettingsModel.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 12/04/21.
//

import UIKit

class OptionSettingsModel: NSObject {
    var mIcon : String?
    var mOption : String?
    
    init(mIcon : String, mOption : String){
        self.mIcon = mIcon
        self.mOption = mOption
    }

}
