//
//  TermsAndConditionsViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/05/21.
//

import UIKit

class TermsAndConditionsViewController: BaseViewController {
    
    var customBlurEffectStyle: UIBlurEffect.Style!
    @IBOutlet weak var mContentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return customBlurEffectStyle == .dark ? .lightContent : .lightContent
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension TermsAndConditionsViewController: MIBlurPopupDelegate {
    
    var popupView: UIView {
        return mContentView ?? UIView()
    }
    
    var blurEffectStyle: UIBlurEffect.Style {
        return .dark
    }
    
    var initialScaleAmmount: CGFloat {
        return 0.51
    }
    
    var animationDuration: TimeInterval {
        return 0.5
    }
    
}
