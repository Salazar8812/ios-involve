//
//  GenericMenuViewController.swift
//  Involve-iOS
//
//  Created by BE-003 on 02/05/21.
//

import UIKit
import BottomPopup

class GenericMenuViewController: BottomPopupViewController, OptionMenuDelegate{
    var height: CGFloat?
    var shouldDismissInteractivelty: Bool?
    var mListOption : [OptionSettingsModel]  = []
    var mOptionMenuDataSource :  OptionMenuDataSource!
    var mTextField: UITextField!
    @IBOutlet weak var mListTableView: UITableView!
    @IBOutlet weak var mContainerView: UIView!
    
    override func viewDidLoad() {
        createTableView()
        mContainerView.roundCorners([.topLeft,.topRight], radius: 20)
    }
    
    func createTableView(){
        mOptionMenuDataSource = OptionMenuDataSource(tableView: mListTableView, mViewController: self, mOptionMenuDelegate: self)
        mListTableView.dataSource = mOptionMenuDataSource
        mListTableView.delegate = mOptionMenuDataSource
        mListTableView.reloadData()
        mOptionMenuDataSource?.update(items: mListOption)
    }
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
         
    override var popupTopCornerRadius: CGFloat { return 20.0}
        
    override var popupPresentDuration: Double { return 0.3 }
        
    override var popupDismissDuration: Double { return 0.3}
        
    override var popupShouldDismissInteractivelty: Bool { return true }
    
    func optionSelect(mItemSelect: String) {
        self.dismiss(animated: true, completion: nil)
        mTextField.text = mItemSelect
    }

}
