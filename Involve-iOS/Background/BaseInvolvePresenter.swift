//
//  BaseInvolvePresenter.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 13/04/21.
//

import UIKit

class BaseInvolvePresenter: BasePresenter, AlamofireResponseDelegate {
    
    
    override open func viewDidLoad() {
        //        mUser = mDataManager.queryWhere(object: User.self).findFirst()
        //        mFormalityEntity = mDataManager.queryWhere(object: FormalityEntity.self).findFirst()
    }
    
    open func onRequestWs(){
    }
    
    open func onSuccessLoadResponse(requestUrl : String, response : BaseResponse){
        
    }
    
    open func onErrorLoadResponse(requestUrl : String, messageError : String){
        if messageError == "" {
            //AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_intern, view: mViewController)
        } else {
            //AlertDialog.show(title: "Ha ocurrido un error", body: messageError, view: mViewController)
        }
    }
    
    open func onErrorConnection(){
        //AlertDialog.show( title: "Error", body: StringDialogs.dialog_error_connection, view: mViewController)
    }
    
}
