//
//  BasePresneter.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class BasePresenter: NSObject {
    
    public override init(){
       
    }
    
    open func viewDidLoad(){
    }
    
    open func viewWillAppear(){
    }
    
    open func viewDidAppear(){
    }
    
    open func viewWillDisappear(){
    }
    
    open func viewDidDisappear(){
    }
    
    open func viewDidUnload(){
    }
    
}

