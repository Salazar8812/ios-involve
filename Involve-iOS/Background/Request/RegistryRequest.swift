//
//  RegistryRequest.swift
//  Involve-iOS
//
//  Created by BE-003 on 25/05/21.
//

import UIKit
import ObjectMapper

class RegistryRequest: BaseRequest {
    var mAcceptNewsLetter : Bool?
    var mAcceptPrivacy : Bool?
    var mAcceptTerms : Bool?
    var mAttemps : Int?
    var mEmail : String?
    var mCreateDate : String?
    var mGender : String?
    var mLastName : String?
    var mName : String?
    var mPassword : String?
    var mPhone : String?
    var mPhoto : String?
    var mSecondLastName : String?
    var mUserID : String?
    var mUserRol : String?
    
    init(mAcceptNewsLetter : Bool?, mAcceptPrivacy : Bool?, mAcceptTerms: Bool?, mAttemps : Int?, mEmail : String?, mCreateDate : String?, mGender : String?, mLastName : String?, mName : String?, mPassword : String?, mPhone : String?, mPhoto : String?, mSecondLastName : String?, mUserID : String?, mUserRol : String? ) {
        super.init()
        self.mAcceptNewsLetter = mAcceptNewsLetter
        self.mAcceptPrivacy = mAcceptPrivacy
        self.mAcceptTerms = mAcceptTerms
        self.mAttemps = mAttemps
        self.mEmail = mEmail
        self.mCreateDate = mCreateDate
        self.mGender = mGender
        self.mLastName = mLastName
        self.mName = mName
        self.mPassword = mPassword
        self.mPhone = mPhone
        self.mPhoto = mPhoto
        self.mSecondLastName = mSecondLastName
        self.mUserID = mUserID
        self.mUserRol = mUserRol
    }
    
    override func mapping(map: Map) {
        if(mAcceptNewsLetter != nil){
            mAcceptNewsLetter <- map["acceptNewsletter"]
        }
        
        if(mAcceptPrivacy != nil){
            mAcceptPrivacy <- map["acceptPrivacy"]
        }
        
        if(mAcceptTerms != nil){
            mAcceptTerms <- map["acceptTerms"]
        }
        
        if(mAttemps != nil){
            mAttemps <- map["attempts"]
        }
        
        if(mEmail != nil){
            mEmail <- map["email"]
        }
        
        if(mCreateDate != nil){
            mCreateDate <- map["createDate"]
        }
        
        if(mGender != nil){
            mGender <- map["gender"]
        }
        
        if(mLastName != nil){
            mLastName <- map["lastName"]
        }
        
        if(mName != nil){
            mName <- map["name"]
        }
        
        if(mPassword != nil){
            mPassword <- map["password"]
        }
        
        if(mPhone != nil){
            mPhone <- map["phone"]
        }
        
        if(mPhoto != nil){
            mPhoto <- map["photo"]
        }
        
        if(mSecondLastName != nil) {
            mSecondLastName <- map["secondLastName"]
        }
        
        if(mUserID != nil){
            mUserID <- map["userId"]
        }
        
        if(mUserRol != nil){
            mUserRol <- map["userRol"]
        }
        
    }
    
    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
}
