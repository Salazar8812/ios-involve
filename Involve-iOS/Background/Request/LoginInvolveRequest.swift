//
//  LoginInvolveRequest.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 19/04/21.
//

import UIKit
import ObjectMapper

class LoginInvolveRequest: BaseRequest {
    var mEmail : String = ""
    var mGrantType : String = ""
    var mPassword : String = ""
   
    
    var mEmailSuccesss : Bool = false
   
    override init() {
        super.init()
    }
    
    public init(mEmail: String, mEmailSuccesss : Bool) {
        super.init()
        self.mEmailSuccesss = mEmailSuccesss
        self.mEmail = mEmail
        self.mGrantType = "CANDIDATE"
    }
    
    public init(mEmail : String, mPassword : String, mEmailSuccesss : Bool){
        super.init()
        self.mEmailSuccesss = mEmailSuccesss
        self.mEmail = mEmail
        self.mPassword = mPassword
        mGrantType = "CANDIDATE"
    }
    
    
    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        if(!mEmailSuccesss){
            mEmail <- map["email"]
            mGrantType <- map["userRol"]
        }else{
            mEmail <- map["email"]
            mPassword <- map["password"]
            mGrantType <- map["userRol"]
        }
    }
}
