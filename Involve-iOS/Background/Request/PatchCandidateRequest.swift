//
//  PatchCandidateRequest.swift
//  Involve-iOS
//
//  Created by Branchbit on 29/06/21.
//

import UIKit
import ObjectMapper

struct PatchCandidateRequest: Codable {
    var mListDataOperation :  [CandidateOperationPatch]?
    
    init(mListDataOperation : [CandidateOperationPatch]) {
        self.mListDataOperation = mListDataOperation
    }
    
}

struct CandidateOperationPatch: Codable{
    var op : String?
    var path : String?
    var value : String?
    
   
    init(mOperation : String, mPath : String, mValue : String) {
        self.op = mOperation
        self.path = mPath
        self.value = mValue
    }
   
    enum CodingKeys: String, CodingKey {
        case op = "op"
        case path = "path"
        case value = "value"
    }
    
    
    init(from decoder: Decoder) throws {
           let values = try decoder.container(keyedBy: CodingKeys.self)
        op = try values.decode(String.self, forKey: .op)
        path = try values.decode(String.self, forKey: .path)
        value = try values.decodeIfPresent(String.self, forKey: .value)
       }

       func encode(to encoder: Encoder) throws {
           var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(op, forKey: .op)
           try container.encode(path, forKey: .path)
        try container.encode(path, forKey: .path)
       }
}



extension PatchCandidateRequest {
  func asDictionary() throws -> [String: Any] {
    let data = try JSONEncoder().encode(self)
    guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
      throw NSError()
    }
    return dictionary
  }
}


extension PatchCandidateRequest {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}
