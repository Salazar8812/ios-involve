//
//  SearchVacantFilterRequest.swift
//  Involve-iOS
//
//  Created by BE-003 on 16/06/21.
//

import UIKit
import ObjectMapper

class SearchVacantFilterRequest: BaseRequest {
    var mAreas : [AreaModel]?
    var mCompanies : [String]?
    var mContracts : [ContractsModel]?
    var mMaximumSalary : Int?
    var mMinimumSalary : Int?
    var mModalities :  [String]?
    var mPublicationDate : String?
    var mStates : [String]?
    var mWorkDays : [String]?
    
    override init() {
        super.init()
    }
    
    init(mAreas:[AreaModel],mCompanies: [String], mContracts:[ContractsModel], mMaximumSalary : Int, mMinimumSalary: Int, mModalities : [String], mPublicationDate : String,mStates : [String], mWorkDays : [String] ) {
        super.init()
        self.mAreas = mAreas
        self.mCompanies = mCompanies
        self.mContracts = mContracts
        self.mMaximumSalary = mMaximumSalary
        self.mMinimumSalary = mMinimumSalary
        self.mModalities = mModalities
        self.mPublicationDate = mPublicationDate
        self.mStates = mStates
        self.mWorkDays = mWorkDays
    }
    
    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        mAreas <- map["areas"]
        mCompanies <- map["companies"]
        mContracts <- map["contracts"]
        mMaximumSalary <- map["maximumSalary"]
        mMinimumSalary <- map["minimumSalary"]
        mModalities <- map["modalities"]
        mPublicationDate <- map["publicationDate"]
        mStates <- map["states"]
        mWorkDays <- map["workDays"]
    }
}


class AreaModel : NSObject, Mappable{
    var mAreaId : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mAreaId <- map["areaId"]
    }
}

class ContractsModel : NSObject, Mappable{
    var mContractId : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mContractId <- map["contractId"]
    }
}

