//
//  ApiDefinitions.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class ApiDefinitions: NSObject {
    
    static var mTokenBeaerer : String = ""
    
    public static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Accept" : "application/json"
    ]
    
    public static let CUSTOM_HEADERS_BEARER = [
        "Content-Type": "application/json",
        "Accept" : "application/json",
        "Authorization": ApiDefinitions.mTokenBeaerer
    ]
    
    static let BASE_URL = "https://je2amljwxe.execute-api.us-east-1.amazonaws.com/dev/involve"
    
    static let WS_LOGIN =  BASE_URL + "/login"
    static let WS_EMAIL = BASE_URL + " "
    static let WS_CUENTA = BASE_URL + " "
    static let WS_REGISTRY = BASE_URL + "/registry"
    static let WS_VERYPHONE = BASE_URL + "/message/veryfy-phone"
    static let WS_CHECK_CODE = BASE_URL + "/message/check-code"
    static let WS_SEARCH_VACANCY = BASE_URL + "/search-vacant"
    static let WS_SEARCH_VACANCY_FILTERS = BASE_URL + "/search-vacant"
    static let WS_INFO_CANDIDATE = BASE_URL + "/candidate"
    static let WS_ABOUT_ME = BASE_URL + "/about-me"
    static let WS_PROFESSIONAL_DATA = BASE_URL + "/professional-data"
    static let WS_PRESENCE_ONLINE = BASE_URL + "/online-presence"
    static let WS_WORK_EXPERIENCE = BASE_URL + "/work-experience"
    static let WS_HARD_SKILL = BASE_URL + "/hard-skill"
    static let WS_SOFT_SKILL = BASE_URL + "/soft-skill"
    static let WS_SPECIAL_AREA = BASE_URL + "/area"
    static let WS_EDUCATION = BASE_URL + "/education"
    static let WS_COURSE = BASE_URL + "/course"
    static let WS_CERTIFICATION = BASE_URL + "/certification"
    static let WS_LANGUAGE = BASE_URL + "/language"
}
