//
//  SearchVacantResponse.swift
//  Involve-iOS
//
//  Created by BE-003 on 16/06/21.
//

import UIKit
import ObjectMapper

class SearchVacantResponse: BaseResponse {
    var mListVacant : [VacantModel]?
    
    override func mapping(map: Map) {
        
    }
}

class VacantModel : NSObject, Mappable{
    var mVacantId : String?
    var mIdShow : String?
    var mName : String?
    var mTitlePosition : String?
    var mNumberVacants : String?
    var mConfidential : Bool?
    var mDraftName : String?
    var mDescription : String?
    var mPeopleCharge : Int?
    var mSalaryMinimum : Double?
    var mSalaryMaximum : Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mVacantId <- map["vacantId"]
        mIdShow <- map["idShow"]
        mName <- map["name"]
        mTitlePosition <- map["titlePosition"]
        mNumberVacants <- map["numbersVacants"]
        mConfidential <- map["confidential"]
        mDraftName <- map["draftName"]
        mDescription <- map["description"]
        mPeopleCharge <- map["peopleCharge"]
        mSalaryMinimum <- map["salaryMinimum"]
        mSalaryMaximum <- map["salaryMaximum"]
    }
}
