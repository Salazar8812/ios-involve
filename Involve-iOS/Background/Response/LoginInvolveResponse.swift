//
//  LoginInvolveResponse.swift
//  Involve-iOS
//
//  Created by Charls Salazar on 19/04/21.
//

import UIKit
import ObjectMapper

class LoginInvolveResponse: BaseResponse{
    var mMessage : String?
    var mDebugInfo : String?
    var mCode : String?
    var mIdCustomCode : String?
    var mData: String?
    var mTimestamp : String?
    var mStatus : Int?
    var mError : String?
    var mPath : String?
    
    var mCandidateId : String?
    var mCountryPosition : String?
    var mStatePosition : String?
    var mTownsHipPosition : String?
    var mPostaCodePosition : String?
    var mMaritalStatsu : String?
    var mNacionality : String?
    var mResidenceCountry : String?
    var mStateResidence : String?
    var mTownsHipResidence : String?
    var mDesireSalary : String?
    var mDateBirth : String?
    var mChangeResidence : String?
    var mStatusEmail : String?
    var mStatusPhone : String?
    var mEmail : String?
    var mAge : String?
    var mUpdateCV : String?
    var mPorcentageProfilel : Int?
    var mNotWorkExperience : String?
    var mUser : UserModel?
    var mPositionRequired : String?
    var mPositionProfile : PositionProfile?
    var mUrlFiles : [String]?
    var mAcademicHistory : [String]?
    var mLanguages : [String]?
    var mCourses : [String]?
    var mHardSkilss : [String]?
    var mSoftSkills : [String]?
    var mAreas : [String]?
    var mSpecialties : [String]?
    var mWorkExperience : [String]?
    var mBodyModel : BodyModel?
    
    var mAccessToken : String?
    
    override func mapping(map: Map) {
        mMessage <- map["message"]
        mDebugInfo <- map["debugInfo"]
        mCode <- map["code"]
        mIdCustomCode <- map["isCustomCode"]
        mData <- map["data"]
        mTimestamp <- map["timestamp"]
        mStatus <- map["status"]
        mError <- map["error"]
        mPath <- map["path"]
        
        mCandidateId <- map["candidateId"]
        mCountryPosition <- map["countryPosition"]
        mStatePosition <- map["statePosition"]
        mTownsHipPosition <- map["townshipPosition"]
        mPostaCodePosition <- map["postalCodePosition"]
        mMaritalStatsu <- map["maritalStatus"]
        mNacionality <- map["nacionality"]
        mResidenceCountry <- map["residenceCountry"]
        mStateResidence <- map["stateResidence"]
        mTownsHipResidence <- map["townshipResidence"]
        mDesireSalary <- map["desiredSalary"]
        mDateBirth <- map["dateBirth"]
        mChangeResidence <- map["changeResidence"]
        mStatusEmail <- map["statusEmail"]
        mStatusPhone <- map["statusPhone"]
        mAge <- map["age"]
        mEmail <- map["email"]
        mUpdateCV <- map["updateCV"]
        mPorcentageProfilel <- map["porcentageProfilel"]
        mNotWorkExperience <- map["notWorkExperience"]
        mUser <- map["user"]
        mPositionRequired <- map["positionRequired"]
        mPositionProfile <- map["positionProfile"]
        mUrlFiles <- map["urlFiles"]
        mAcademicHistory <- map["academicHistory"]
        mLanguages <- map["languages"]
        mCourses <- map["courses"]
        mHardSkilss <- map["hardSkills"]
        mSoftSkills <- map["softSkills"]
        mAreas <- map["areas"]
        mSpecialties <- map["specialties"]
        mWorkExperience <- map["workExperiencie"]
        mBodyModel <- map["body"]
        mAccessToken <- map["token"]
    }
    
}

class BodyModel : NSObject, Mappable{
    var mMessage : String?
    var mCode : Int?
    var mIsCustomCode : Bool?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mMessage <- map["message"]
        mCode <- map["code"]
        mIsCustomCode <- map["isCustomCode"]
    }
}

class PositionProfile:  NSObject, Mappable{
    var mPositionId : String?
    var mPosition : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mPosition <- map["position"]
        mPositionId <- map["positionId"]
    }
}

class UserModel : NSObject, Mappable{
    var mUserId: String?
    var mName : String?
    var mLasName : String?
    var mSeconLastName : String?
    var mEmail : String?
    var mPassword : String?
    var mGender : String?
    var mPhoto : String?
    var mPhone : String?
    var mAttemps : String?
    var mUserRol : String?
    var mAcceptPrivacy : Bool?
    var mAcceptTerms : Bool?
    var mAcceptNewsLetter : Bool?
    var mCreateDate : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mUserId <- map["userId"]
        mName <- map["name"]
        mLasName <- map["lastName"]
        mSeconLastName <- map["secondLastName"]
        mEmail <- map["email"]
        mPassword <- map["password"]
        mGender <- map["gender"]
        mPhoto <- map["photo"]
        mPhone <- map["phone"]
        mAttemps <- map["attempts"]
        mUserRol <- map["userRol"]
        mAcceptPrivacy <- map["acceptPrivacy"]
        mAcceptTerms <- map["acceptTerms"]
        mAcceptNewsLetter <- map["acceptNewsletter"]
        mCreateDate <- map["createDate"]
    }
}
