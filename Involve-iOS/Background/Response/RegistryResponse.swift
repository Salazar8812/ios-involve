//
//  RegistryResponse.swift
//  Involve-iOS
//
//  Created by BE-003 on 25/05/21.
//

import UIKit
import ObjectMapper

class RegistryResponse: BaseResponse {
    var mStatus : Int?
    var mError : String?
    
    var mCandidateId: String?
    var mCountryPosition : String?
    var mStatePosition : String?
    var mTownShipPosition : String?
    var mPostalCodePosition : String?
    var mMaritalStatus : String?
    var mNaciontality : String?
    var mResidenceCountry : String?
    var mStateResidence : String?
    var mTownShipResidence : String?
    var mDesiredSalary : String?
    var mDateBirth : String?
    var mChangeResidence : String?
    var mStatusMail : Bool?
    var mStatusPhone : Bool?
    var mAge : String?
    var mUpadteCV: String?
    var mPorcentageProfilel : Int?
    var mNotWorkExperience : String?
    var mUserModel : UserRegistryModel?
    var mPositionRequired : String?
    var mPositionProfile : String?
    var mAboutMeComplete : String?
    var mProfessionalDataComplete : String?
    var mLaboralExperienceComplete : String?
    var mSkillComplete : String?
    var mEducationComplete : String?
    var mUrlFiles : String?
    var mAcademicHistory : String?
    var mLanguages : String?
    var mCourses : String?
    var mCertifications : String?
    var mHardSkills : String?
    var mSoftSkills : String?
    var mAreas : String?
    var mSpecialities : String?
    var mWorlkExperience : String?
    
    override func mapping(map: Map) {
        mStatus <- map["status"]
        mError <- map["error"]
        
        mCandidateId <- map["candidateId"]
        mCountryPosition <- map["countryPosition"]
        mStatePosition <- map["statePosition"]
        mTownShipPosition <- map["townshipPosition"]
        mPostalCodePosition <- map["postalCodePosition"]
        mMaritalStatus <- map["maritalStatus"]
        mNaciontality <- map["nacionality"]
        mResidenceCountry <- map["residenceCountry"]
        mStateResidence <- map["stateResidence"]
        mTownShipResidence <- map["townshipResidence"]
        mDesiredSalary <- map["desiredSalary"]
        mDateBirth <- map["dateBirth"]
        mChangeResidence <- map["changeResidence"]
        mStatusMail <- map["statusEmail"]
        mStatusPhone <- map["statusPhone"]
        mAge <- map["age"]
        mUpadteCV <- map["updateCV"]
        mPorcentageProfilel <- map["porcentageProfilel"]
        mNotWorkExperience <- map["notWorkExperience"]
        mUserModel <- map["user"]
        mPositionRequired <- map["positionRequired"]
        mPositionProfile <- map["positionProfile"]
        mAboutMeComplete <- map["aboutMeComplete"]
        mProfessionalDataComplete <- map["professionalDataComplete"]
        mLaboralExperienceComplete <- map["laboralExperienceComplete"]
        mSkillComplete <- map["skillsComplete"]
        mEducationComplete <- map["educationComplete"]
        mUrlFiles <- map["urlFiles"]
        mAcademicHistory <- map["academicHistory"]
        mLanguages <- map["languages"]
        mCourses <- map["courses"]
        mCertifications <- map["certifications"]
        mHardSkills <- map["hardSkills"]
        mSoftSkills <- map["softSkills"]
        mAreas <- map["areas"]
        mSpecialities <- map["specialties"]
        mWorlkExperience <- map["workExperiencie"]
    }
    
}

class UserRegistryModel : NSObject , Mappable{
    var mAcceptNewsLetter : Bool?
    var mAcceptPrivacy : Bool?
    var mAcceptTerms : Bool?
    var mAttemps : Int?
    var mEmail : String?
    var mCreateDate : String?
    var mGender : String?
    var mLastName : String?
    var mName : String?
    var mPassword : String?
    var mPhone : String?
    var mPhoto : String?
    var mSecondLastName : String?
    var mUserID : String?
    var mUserRol : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        mAcceptNewsLetter <- map["acceptNewsletter"]
        mAcceptPrivacy <- map["acceptPrivacy"]
        mAcceptTerms <- map["acceptTerms"]
        mAttemps <- map["attempts"]
        mEmail <- map["email"]
        mCreateDate <- map["createDate"]
        mGender <- map["gender"]
        mLastName <- map["lastName"]
        mName <- map["name"]
        mPassword <- map["password"]
        mPhone <- map["phone"]
        mPhoto <- map["photo"]
        mSecondLastName <- map["secondLastName"]
        mUserID <- map["userId"]
        mUserRol <- map["userRol"]
    }
}
