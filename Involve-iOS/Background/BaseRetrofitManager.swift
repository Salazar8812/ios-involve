//
//  BaseRetrofitManager.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Foundation

open  class BaseRetrofitManager<Res : BaseResponse>: NSObject {
    
    open var requestUrl : String
    open var delegate : AlamofireResponseDelegate
    open var request : Alamofire.Request?
    
    public init(requestUrl: String, delegate : AlamofireResponseDelegate){
        self.delegate = delegate
        self.requestUrl = requestUrl
    }
    
    open var Manager: Alamofire.SessionManager = {
        // Create the server trust policies
        
        let certificates = ServerTrustPolicy.certificates()
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "http://3.91.30.220:8080": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    
    open func requestLoginGetToken(requestModel : BaseRequest) {
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            let params  = Mapper().toJSON(requestModel)
            print("Request: \(requestModel.toJSONString(prettyPrint: true) ?? "")")
            print("URL: ", self.requestUrl)
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    if let headers = response.response?.allHeaderFields as? [String: String]{
                        let header = headers["token"]
                        ApiDefinitions.mTokenBeaerer = header != nil ? header! : ""
                        if(ApiDefinitions.mTokenBeaerer != ""){
                            self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                        }else{
                            self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Token no obtenido, intente nuevamente")
                        }
                        print(ApiDefinitions.mTokenBeaerer)
                    }
                   
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    

    open func requestPost(requestModel : BaseRequest) {
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            let params  = Mapper().toJSON(requestModel)
            print("Request: \(requestModel.toJSONString(prettyPrint: true) ?? "")")
            print("URL: ", self.requestUrl)
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    
    open func requestPatchWithToken(requestModel: Codable) {
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            print("URL: ", self.requestUrl)

            let mParams = requestModel as! PatchCandidateRequest
            let mParameter = mParams.dictionary
            let mPar = mParameter?.values
        
            request = Manager.request(self.requestUrl, method: HTTPMethod.patch, parameters: mParameter, encoding: JSONEncoding.default, headers:ApiDefinitions.CUSTOM_HEADERS_BEARER).responseObject{
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    
   
    
    open func requestPostWithTokenHeader(requestModel : BaseRequest) {
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            let params  = Mapper().toJSON(requestModel)
            print("Request: \(requestModel.toJSONString(prettyPrint: true) ?? "")")
            print("URL: ", self.requestUrl)
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS_BEARER).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    
   
    
    open func requestGet() {
        
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    open func requestGetWithTokenHeader() {
        
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS_BEARER).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
   
}


public protocol AlamofireResponseDelegate {
    
    func onRequestWs()
    
    func onSuccessLoadResponse(requestUrl : String, response : BaseResponse)
    
    func onErrorLoadResponse(requestUrl : String, messageError : String)
        
    func onErrorConnection()
    
}



