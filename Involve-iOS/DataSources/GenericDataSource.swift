//
//  GenericDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 28/04/21.
//

import UIKit

protocol GenericDelegate : NSObjectProtocol{
    func selectOption(mOption: String)
}

class GenericDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [String] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemGenericCVTableViewCell"
    var mViewController:  UIViewController?
    var mSettingsDelegate : GenericDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.separatorStyle = .none
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
    }
    
    func update(items: [String]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 88
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemGenericCVTableViewCell
        
        return cell
    }
    
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.mSettingsDelegate.selectOption(mOption: mSectionsArray[indexPath.row].mOption!)
    }
    

}
