//
//  OptionMenuDataSource.swift
//  Involve-iOS
//
//  Created by BE-003 on 05/05/21.
//

import UIKit

public protocol OptionMenuDelegate : NSObjectProtocol {
    func optionSelect(mItemSelect: String)
}

class OptionMenuDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var mSectionsArray : [OptionSettingsModel] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemOptionMultipleTableViewCell"
    var mViewController:  UIViewController?
    var mOptionMenuDelegate : OptionMenuDelegate!
    var mPositionTag : Int?
    var mButton : UIView?
    
    init(tableView: UITableView, mViewController:  UIViewController, mOptionMenuDelegate : OptionMenuDelegate) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
        self.mOptionMenuDelegate = mOptionMenuDelegate
    }
    
    func update(items: [OptionSettingsModel]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem, for: indexPath) as! ItemOptionMultipleTableViewCell

        cell.mOptionLabel.text = item.mOption
        cell.mOptionSelectBotton.addTarget(self, action: #selector(selectOption), for: .touchUpInside)
        
        return cell
    }
    
    @objc func selectOption(){
        
    }
    
    @objc func optionSelect(_ sender: UIButton){
        mPositionTag = sender.tag
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mOptionMenuDelegate.optionSelect(mItemSelect: mSectionsArray[indexPath.row].mOption!)
    }
    

}
